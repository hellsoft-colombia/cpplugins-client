import {BASE_URL} from '../../constants/api';

const imagesUrl = `${BASE_URL}image`;

export const urlGetUserImages = `${imagesUrl}`;

export const urlGetSpecificImage = `${imagesUrl}/`;

export const urlDeleteImage = `${imagesUrl}/delete/`;

export const urlUploadImage = `${imagesUrl}/new`;
