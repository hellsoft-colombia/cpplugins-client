import {BASE_URL} from '../../constants/api';

const pluginsUrl = `${BASE_URL}plugins/all`;
const customPluginUrl = `${BASE_URL}custom-plugin`;

export const urlFunctionList = `${pluginsUrl}`;

export const urlGetUserCustomPlugins = `${customPluginUrl}`;

export const urlGetSpecificCustomPlugin = `${customPluginUrl}/`;

export const urlCreateCustomPlugin = `${customPluginUrl}/new`;

export const urlUpdateCustomPlugin = `${customPluginUrl}/update`;

export const urlDeleteCustomPlugin = `${customPluginUrl}/delete/`;
