import {BASE_URL} from '../../constants/api';

const urlPipelines = `${BASE_URL}pipeline`;

export const urlGetUserPipelines = `${urlPipelines}`;

export const urlGetUserPipelinesByState = `${urlPipelines}/state/`;

export const urlGetSpecificPipeline = `${urlPipelines}/`;

export const urlNewPipeline = `${urlPipelines}/new`;

export const urlDeletePipeline = `${urlPipelines}/delete/`;

export const urlUpdatePipeline = `${urlPipelines}/update`;

export const urlExecutePipeline = `${urlPipelines}/execute`;

export const urlStopPipeline = `${urlPipelines}/stop/`;

export const urlCheckPipeline = `${urlPipelines}/check/`;

export const urlCancelPipeline = `${urlPipelines}/cancel/`;

export const urlResumePipeline = `${urlPipelines}/resume/`;