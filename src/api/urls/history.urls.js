import {BASE_URL} from '../../constants/api';

const urlHistory = `${BASE_URL}history`;

export const urlGetSpecificHistoryEntry = `${urlHistory}/`;

export const urlGetUserHistory = `${urlHistory}/`;

export const urlGetUserHistoryCount = `${urlHistory}count`;

export const urlGetUserHistoryByState = `${urlHistory}/`;

export const urlGetUserHistoryByStateCount = `${urlHistory}count/`;
