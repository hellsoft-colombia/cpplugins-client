import {BASE_URL} from '../../constants/api';

const userUrl = `${BASE_URL}user/`;

export const urlUserAuthentication = `${BASE_URL}auth`;

export const urlUserRegister = `${userUrl}signup`;

export const urlUserInformation = `${userUrl}info`;

export const urlUserLogout = `${BASE_URL}logout`;