/**
 * File description: This file contains the functions of the request that are used in the application.
 * Author: Hellsoft
 */
import {handleUnauthorizedRequest} from './handleErrorCodes';

const headers = {
    'Content-type': 'application/json'
}

const handleResponse = (response) => (response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
        if (response.status === 401) {
            handleUnauthorizedRequest();
        }
        return Promise.reject(data);
    }
    return data.payload;
}));

export const apiGet = (url, params = '') => () => fetch(`${url}${params}`, {
    method: 'GET',
    headers: new Headers(headers),
    credentials: 'include'
}).then(v => handleResponse(v))

export const apiPut = (url, obj, id='') => () => fetch(`${url}${id}`, {
    method: 'PUT',
    body: JSON.stringify(obj),
    headers: new Headers(headers),
    credentials: 'include'
}).then(v => handleResponse(v))

export const apiPost = (url, obj) => () => fetch(`${url}`, {
    method: 'POST',
    body: JSON.stringify(obj),
    headers: new Headers(headers),
    credentials: 'include'
}).then(v => handleResponse(v))

export const apiFormDataPost = (url, obj) => () => fetch(`${url}`, {
    method: 'POST',
    body: obj,
    credentials: 'include'
}).then(v => handleResponse(v))

export const apiDelete = (url, id) => () => fetch(`${url}${id}`, {
    method: 'DELETE',
    headers: new Headers(headers),
    credentials: 'include'
}).then(v => handleResponse(v))