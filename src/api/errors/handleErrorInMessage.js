/**
 * File description: This file contains the messages when an error has ocurred.
 * Author: Hellsoft
 */
import {_USER_FUNCTIONS, _PIPELINE_FUNCTIONS, _FILE_FUNCTIONS, _PLUGIN_FUNCTIONS, _EXECUTION_HISTORY} from '../../constants/componentFunctions';

export const handleErrorInMessage = (response, actionType) => {
    let message = "";
    switch (response.status) {
        case 'BAD_FORMATTED_DATA':
            message = "Your request is not well builded !"
            break;
        case 'DUPLICATE_ENTITY':
            switch (actionType) {
                case _USER_FUNCTIONS:
                    message = "User or Email is duplicated. please fill the form again"
                    break;
                case _PIPELINE_FUNCTIONS:
                    message = "Pipeline name/is duplicated"
                    break;
                case _FILE_FUNCTIONS:
                    message = "File name is duplicated"
                    break;
                case _PLUGIN_FUNCTIONS:
                    message = "CustomPlugin"
                    break;
                case _EXECUTION_HISTORY:
                    message = "Execution entry is duplicated"
                    break;
                default:
                    message = "Duplicated entity !";
                    break;
            }
            break;
        case 'INTERNAL_SERVER_ERROR':
            message = "Something is wrong in our server. Please try later...."
            break;
        case 'NOT_FOUND':
            switch (actionType) {
                case _USER_FUNCTIONS:
                    message = "Searched user is not present";
                    break;
                case _PIPELINE_FUNCTIONS:
                    message = "Not found desired pipeline";
                    break;
                case _FILE_FUNCTIONS:
                    message = "Not found desired file"
                    break;
                case _PLUGIN_FUNCTIONS:
                    message = "Not found plugins"
                    break;
                case _EXECUTION_HISTORY:
                    message = "Not entry found"
                    break;
                default:
                    message = "404 NOT FOUND !";
                    break;

            }
            break;
        default:
            message = "Wrong request arrived to the server";
            break;
    }

    return message;
}