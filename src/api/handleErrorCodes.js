/**
 * File description: This file contains the handlers for code error. Ex: Error 400.
 * Author: Hellsoft
 */
import {history} from '../helpers/history';
import {LOGIN_ROUTE} from '../constants/routes';

export const handleUnauthorizedRequest = _ => (
    history.push(LOGIN_ROUTE)
)