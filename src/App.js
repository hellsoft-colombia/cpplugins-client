import React, {Component} from 'react';
import {Router, Route, Switch} from "react-router-dom";
import HomeContainer from './containers/HomeContainer';
import LoginContainer from './containers/LoginContainer';
import RegisterContainer from './containers/RegisterContainer';
import FileAreaContainer from './containers/FileAreaContainer';
import WorkSpaceContainer from './containers/WorkSpaceContainer';
import VisualizerContainer from './containers/VisualizerContainer';
import ExecutionHistoryContainer from './containers/ExecutionHistoryContainer';
import {
    HOME_ROUTE,
    LOGIN_ROUTE,
    REGISTER_ROUTE,
    FILE_AREA_ROUTE,
    DESIGN_AREA_ROUTE,
    EXECUTION_HISTORY
} from './constants/routes';
import {AuthorizationComponent, NoAuthorizationComponent} from './helpers/AuthorizationComponent';
import {history} from './helpers/history';
import NotFoundComponent from './components/NotFoundComponent';

function AppRouter() {

    const renderHomeContainer = () => <HomeContainer/>;

    const renderLoginContainer = () => <LoginContainer/>;

    const renderRegisterContainer = () => <RegisterContainer/>;

    const renderFileAreaContainer = () => <FileAreaContainer/>;

    const renderWorkSpaceContainer = () => <WorkSpaceContainer/>;

    const renderVisualizerContainer = () => <VisualizerContainer/>;

    const renderExecutionHistoryContainer = () => <ExecutionHistoryContainer/>;

    const renderNotFoundComponent = () => <NotFoundComponent/>;


    return (
            <Router history={history}>
                <Switch>
                    <Route
                        exact
                        path={HOME_ROUTE}
                        component={NoAuthorizationComponent(renderHomeContainer)}/>
                    <Route
                        exact
                        path={LOGIN_ROUTE}
                        component={NoAuthorizationComponent(renderLoginContainer)}/>
                    <Route
                        exact
                        path={REGISTER_ROUTE}
                        component={NoAuthorizationComponent(renderRegisterContainer)}/>
                    <Route
                        exact
                        path={FILE_AREA_ROUTE}
                        component={AuthorizationComponent(renderFileAreaContainer)}/>
                    <Route
                        exact
                        path={EXECUTION_HISTORY}
                        component={AuthorizationComponent(renderExecutionHistoryContainer)}/>
                    <Route
                        exact
                        path={DESIGN_AREA_ROUTE}
                        component={AuthorizationComponent(renderWorkSpaceContainer)}/>
                    <Route
                        exact
                        path="/visualizer"
                        component={AuthorizationComponent(renderVisualizerContainer)}/>
                    <Route component={renderNotFoundComponent} />
                </Switch>
            </Router>
    );
}

class App extends Component {
    render() {
        return (<AppRouter/>)
    }
}

export default App;
