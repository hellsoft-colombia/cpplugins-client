/**
 * File description: This component shows an explorer that contains the plugins of cpPlugins3.0.
 * Author: Hellsoft
*/
import React from 'react';
import PropTypes from 'prop-types';
import SortableTree from 'react-sortable-tree';
import FileExplorerTheme from 'react-sortable-tree-theme-file-explorer';
import Tippy from '@tippy.js/react';
import { Transition } from 'react-transition-group'
import FolderIcon from '@material-ui/icons/Folder';
import InfoIcon from '@material-ui/icons/Info';
import AddIcon from '@material-ui/icons/AddCircleRounded';
import FileIcon from '@material-ui/icons/InsertDriveFileRounded';
import './style.css';

const duration = 300
const sidebarStyle = {
  transition: `width ${duration}ms`
}
const sidebarTransitionStyles = {
  entering: { width: 0 },
  entered: { width: '30vw' },
  exiting: { width: '30vw' },
  exited: { width: 0 }
}


const renderIcons = (rowInfo) => (
  rowInfo.node.isDirectory ? [
      <FolderIcon fontSize="large"></FolderIcon>
    , ] : [
      <FileIcon fontSize="large"></FileIcon>
    , ]
)

const renderCustomDirectory = (rowInfo, modalNodeInfo, nodeInformation) => (
  <div> 
    <Tippy 
      content = {<span>{`${nodeInformation}<br>`}</span>}
      arrow = {true} 
      maxWidth = {200}
      placement = {'rigth'}
      trigger = {'click'}>
      <button className="tree-item-info"
        data-toggle="tooltip" 
        data-placement="top" 
        title={rowInfo.node.subtitle}
        onClick = {() => modalNodeInfo(rowInfo)}
      >
      <InfoIcon fontSize="large"></InfoIcon>
      </button>
    </Tippy>
  </div>
)

const renderStaticDirectory = (rowInfo, modalNodeInfo, nodeInformation) => (
  <Tippy
    content = {<span>{`${nodeInformation}`}</span>}
    arrow = {true} 
    maxWidth = {200}
    placement = {'rigth'}
    trigger = {'click'}>
    <button className="tree-item-info"              
      data-toggle="tooltip" data-placement="top" title={rowInfo.node.subtitle}
      onClick = {() => modalNodeInfo(rowInfo)}
    > <InfoIcon fontSize="large"></InfoIcon> </button>
  </Tippy>
)

const renderNodeDirectory = (rowInfo, modalNodeInfo, nodeInformation) => (
  rowInfo.node.isCustom ? 
  renderCustomDirectory(rowInfo, modalNodeInfo, nodeInformation) :
  renderStaticDirectory(rowInfo, modalNodeInfo, nodeInformation)
)

const renderCustomNode = (rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) => (
  [
  <Tippy 
    content={<span>{`${nodeInformation}`}</span>}
    arrow={true} 
    maxWidth = {200}
    placement = {'rigth'} 
    trigger = {'click'}>
    <button className="tree-item-info"               
      data-toggle="tooltip" 
      data-placement="top" 
      title={rowInfo.node.subtitle}
      onClick = {() => modalNodeInfo(rowInfo)}
    > 
      <InfoIcon fontSize="large"></InfoIcon> 
    </button>
    </Tippy>,
  <button className="tree-item-info"               
    data-toggle="tooltip" 
    data-placement="top" 
    title={rowInfo.node.subtitle}
    onClick={() =>  addFilterToCanvas(rowInfo.node)}
  > <AddIcon fontSize="large"></AddIcon> </button>
  ]
)

const renderStaticNode = (rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) => (
  [
    <Tippy 
      content={<span>{`${nodeInformation}`}</span>}
      arrow = {true} 
      maxWidth = {200}
      placement = {'rigth'} 
      trigger = {'click'}>
      <button className="tree-item-info"
        data-toggle="tooltip" 
        data-placement="top" 
        title={rowInfo.node.subtitle}
        onClick = {() => modalNodeInfo(rowInfo)}
      >
        <InfoIcon fontSize="large"></InfoIcon>
      </button>
    </Tippy>,
    <button 
      className="tree-item-info"
      data-toggle="tooltip" 
      data-placement="top" 
      title={rowInfo.node.subtitle}
      onClick={() =>  addFilterToCanvas(rowInfo.node)}
    >
      <AddIcon fontSize="large"></AddIcon>
    </button>
  ]
)

const renderButtons = (rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) => (
  rowInfo.node.isDirectory ? [
    renderNodeDirectory(rowInfo, modalNodeInfo, nodeInformation)
  ]:
  rowInfo.node.isCustom ?
    renderCustomNode(rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) : 
    renderStaticNode(rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation)
)

const renderGenerateNodeProps = (rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) => {
  return {
    icons: renderIcons(rowInfo),
    buttons: renderButtons(rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation)
  }
}

const FunctionListComponent = ({ searchFocusIndex, searchString, handleSearchOnChange, 
                                toggleNodeExpansion, functionList, setFunctionList, 
                                addFilterToCanvas, modalNodeInfo, 
                                nodeInformation, isOpen}) => {
    return (
    <Transition in={isOpen} timeout={duration}>
      {(state) => (
      <div className="my-tree-wrapper" style={{
        ...sidebarStyle,
        ...sidebarTransitionStyles[state]
      }}>
        <div className="left-bar-actions">
          <label>Search: </label>
          <input type="text" onChange={handleSearchOnChange} />
          <div>
            <button onClick={toggleNodeExpansion.bind(this, true)}>
                Expand all
            </button>
            <button onClick={toggleNodeExpansion.bind(this, false)}>
              Collapse all
            </button>
          </div>          
        </div>
        
        <SortableTree className = "tree"
          treeData={functionList}
          onChange={treeData => setFunctionList(treeData)}
          theme={FileExplorerTheme}
          canDrag={false}
          canDrop={() => false}
          searchQuery={searchString}
          searchFocusOffset={searchFocusIndex}
          generateNodeProps={ (rowInfo) => 
            renderGenerateNodeProps(rowInfo, modalNodeInfo, addFilterToCanvas, nodeInformation) }
        />
      </div>
      )}
      </Transition>
    );
};

FunctionListComponent.propTypes = {
    searchFocusIndex : PropTypes.number.isRequired,
    searchString : PropTypes.string.isRequired,
    handleSearchOnChange : PropTypes.func.isRequired,
    toggleNodeExpansion : PropTypes.func.isRequired,
    functionList : PropTypes.array.isRequired,
    setFunctionList : PropTypes.func.isRequired,
    addFilterToCanvas: PropTypes.func.isRequired,
    hideInfoNodeInfo: PropTypes.func.isRequired,
    modalNodeInfo: PropTypes.func.isRequired,
    nodeInformation: PropTypes.string.isRequired,
};

export default FunctionListComponent;