import React from 'react';

const NotFoundComponent = () => {
    return (
        <div style={{
            display: 'flex',
            alignContent: 'center',
            alignItems: 'center',
            backgroundImage: 'url(https://kicksdigitalmarketing.com/u/2019/09/iStock-1142986365.jpg)',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat',
            backgroundAttachment: 'fixed',
            backgroundSize: 'cover',
            backgroundColor: 'black',
            height: '100vh'
        }}/>
    );
}

export default NotFoundComponent;