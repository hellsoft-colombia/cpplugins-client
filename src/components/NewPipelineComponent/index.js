/**
 * File description: Displays a modal to create a new pipeline.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-awesome-modal';

const NewPipelineComponent = ({
    newPipelineVisible,
    openModalNewPipeline,
    closeModalNewPipeline,
    newPipelineName,
    newPipelineDescription,
    onChange,
    onSubmit,
    changeCheckedState,
    title
}) => {
    const newPipelineNameName = "newPipelineName";
    const newPipelineDescriptionName = "newPipelineDescription";
    return (
        <div>
            { 
                title !== 'Files'
                ? <input type="button" value="New +" className="modal-button" onClick={openModalNewPipeline}/>
                : <p></p>
            }
            <Modal visible={ newPipelineVisible } width="400" height="300" effect="fadeInUp" onClickAway={ closeModalNewPipeline }>
            <div className="pipelines-modal">
                <div id="newPipeline">
                    <h3 style={{ textAlign: "center", marginLeft: "0px"}}>New pipeline</h3>
                    <p>Name:
                    </p>
                    <input
                        name={newPipelineNameName}
                        value={newPipelineName.value}
                        placeholder={newPipelineName.placeholder}
                        onChange={onChange}
                        type="text"/> {newPipelineName.error && <strong style={{
                        color: 'red'
                    }}>{newPipelineName.error}</strong>}
                    <p>Description:</p>
                    <input
                        name={newPipelineDescriptionName}
                        value={newPipelineDescription.value}
                        placeholder={newPipelineDescription.placeholder}
                        onChange={onChange}
                        type="text"/> {newPipelineDescription.error && <strong style={{
                        color: 'red'
                    }}>{newPipelineDescription.error}</strong>}

                    <br/>
                    <label>
                        <input
                            id="funccb"
                            type="checkbox"
                            onClick={changeCheckedState}
                            value="isFunction"/>
                        Is custom node?
                    </label>
                    <br/>
                    <button className="modal-start-button" onClick={onSubmit}>Start to design</button>
                </div>
            </div>
            </Modal>
        </div>
    );
}

NewPipelineComponent.propTypes = {
    newPipelineVisible: PropTypes.bool.isRequired,
    newPipelineName: PropTypes.object.isRequired,
    newPipelineDescription: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    changeCheckedState: PropTypes.func.isRequired
};

export default NewPipelineComponent;