/**
 * File description: Displays the information for the home component.
 * Author: Hellsoft
 */
import React from 'react';
import HomeInformationComponent from './HomeInformationComponent';
import UserRegisterInfo from '../UserRegisterComponent/UserRegisterInfo';
import InformativeVideoComponent from '../InformativeVideoComponent';
import TeamInformationComponent from '../TeamInformationComponent';
import './style.css'

const HomeComponent = ({
    onChange,
    onSubmit,
    userName,
    password,
    userEmail,
    firstName,
    lastName,
    error
}) => {
    return (
        <div className="home-component">
            <HomeInformationComponent
                title = "cpPlugins 3.0"
                description = "Modular tooltip for medical images processing and visualization"
            />
            <h2 className = "home-title">Meet cpPlugins 3.0!</h2>
            <InformativeVideoComponent/> 
            <TeamInformationComponent/> 
            <HomeInformationComponent
                title = "Register"
                description = "Start usign cppPlugins 3.0"
            />
            <UserRegisterInfo
                onChange={onChange}
                onSubmit={onSubmit}
                userName={userName}
                password={password}
                userEmail={userEmail}
                firstName={firstName}
                lastName={lastName}
                error={error}/>
        </div>
    );
};

export default HomeComponent;