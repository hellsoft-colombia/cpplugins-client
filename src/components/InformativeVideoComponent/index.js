/**
 * File description: Displays the video component shown in home component.
 * Author: Hellsoft
 */
import React from 'react';
import { VIDEO, VIDEO_THUMBNAIL} from '../../constants/assets';
import './style.css'

const InformativeVideoComponent = () => {
    return (
        <div className="informative-video-component">
            <video width="605" controls poster={VIDEO}>
                <source type="video/mp4" src = {VIDEO_THUMBNAIL} />
            </video>
        </div>
    );
};


export default InformativeVideoComponent;