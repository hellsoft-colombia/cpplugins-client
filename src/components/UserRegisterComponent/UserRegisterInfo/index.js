/**
 * File description: Displays the fields for registration.
 * Author: Hellsoft
 */
import React from 'react';
import {UsernameData, NameData, PasswordData, EmailData} from '../../shared/forms/inputs';


const UserRegisterInfo = (props) => {
    const {
        onChange,
        onSubmit,
        userName,
        password,
        userEmail,
        firstName,
        lastName,
        error
    } = props;

    return (
        <div className="user-register-info-component">
            <UsernameData onChange={onChange} userName={userName}/>
            <NameData onChange={onChange} firstName={firstName} lastName={lastName}/>
            <EmailData onChange={onChange} userEmail={userEmail}/>
            <PasswordData onChange={onChange} password={password}/> 
            <br/>
            {error && <strong>{error}</strong>}
            <br/>
            <button onClick={onSubmit}>Create account</button>
        </div>
    )
}

export default UserRegisterInfo;
