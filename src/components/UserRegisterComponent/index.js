/**
 * File description: Displays the component that shows the fields of registration.
 * Author: Hellsoft
 */
import React from 'react';
import UserRegisterInfo from './UserRegisterInfo';
import './style.css';

const UserRegisterComponent = (props) => {
    const {
        onChange,
        onSubmit,
        userName,
        password,
        userEmail,
        firstName,
        lastName,
        error
    } = props;
    return (
        <div>
            <h1>Welcome to cpPlugins 3.0</h1>
            <UserRegisterInfo
                onChange={onChange}
                onSubmit={onSubmit}
                userEmail={userEmail}
                userName={userName}
                password={password}
                firstName={firstName}
                lastName={lastName}
                error={error}/>
            <img src="" alt=""/><br/>
            You're almost done to enjoy cpPlugins experience.
        </div>
    );
};

export default UserRegisterComponent;
