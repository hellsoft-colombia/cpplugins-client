import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-awesome-modal';
import NewPipelineComponent from '../NewPipelineComponent';
import CloseIcon from '@material-ui/icons/Close';

const LoadFileComponent = ({
    isPipelinesAdministration,
    loadFileVisible,
    openModalLoadFile,
    closeModalLoadFile,
    newPipelineVisible, 
    newPipelineName,
    newPipelineDescription,
    onChange,
    onSubmit,
    changeCheckedState
}) => {

    return (
        <div>
            <input type="button" value="New +" className="modal-button" onClick={openModalLoadFile}/>
            <Modal visible={loadFileVisible} width="410" height="350" effect="fadeInUp" 
                onClickAway={closeModalLoadFile}>
                <div className="pipelines-modal">
                    {
                        isPipelinesAdministration() &&
                        <div>
                            <NewPipelineComponent
                                newPipelineVisible = { newPipelineVisible }
                                newPipelineName={newPipelineName}
                                newPipelineDescription={newPipelineDescription}
                                onChange={onChange}
                                onSubmit={onSubmit}
                                changeCheckedState={changeCheckedState}
                            />
                        </div>
                    }
                    <button className="close-button" onClick={closeModalLoadFile}>
                        <CloseIcon fontSize="large"></CloseIcon>
                    </button>
                </div>
            </Modal>
        </div>
    );
}

LoadFileComponent.propTypes = {
    isPipelinesAdministration: PropTypes.func.isRequired,
    loadFileVisible: PropTypes.bool.isRequired,
    openModalLoadFile: PropTypes.func.isRequired,
    closeModalLoadFile: PropTypes.func.isRequired,
    newPipelineVisible: PropTypes.bool.isRequired, 
    onChangeFile: PropTypes.func.isRequired,
    onFileFormSubmit: PropTypes.func.isRequired,
    newPipelineName: PropTypes.object.isRequired,
    newPipelineDescription: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    changeCheckedState: PropTypes.func.isRequired,
};

export default LoadFileComponent;