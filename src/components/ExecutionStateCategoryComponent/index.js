/**
 * File description: The component has the buttons to filter the pipelines according to its state.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';

const ExecutionStateCategoryComponent = ({swtichToAllEntries, switchToActivePipelines, switchToFinishedPipelines, switchToFinishedPipelinesEntries}) => (
    <div>
        <button onClick={swtichToAllEntries}>All entries</button>
        <button onClick={switchToActivePipelines}>Active pipelines</button>
        <button onClick={switchToFinishedPipelinesEntries}>Finished pipelines</button>
        <button onClick={switchToFinishedPipelines}>Pipelines Results</button>
    </div>
)

ExecutionStateCategoryComponent.propTypes = {
    swtichToAllEntries: PropTypes.func.isRequired,
    switchToActivePipelines: PropTypes.func.isRequired,
    switchToFinishedPipelines: PropTypes.func.isRequired,
    switchToFinishedPipelinesEntries: PropTypes.func.isRequired
}

export default ExecutionStateCategoryComponent;