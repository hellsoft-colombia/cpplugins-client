/**
 * File description: Displays the option to create a new account.
 * Author: Hellsoft
 */
import React from 'react'

const NewUserOption = () => (
  <div>
    Es usted un usuario nuevo en cpPlugins 3.0?<br />
    <a href="register">Crear una cuenta</a>
  </div>
)

export default NewUserOption