/**
 * File description: Displays the component that shows the fields of login.
 * Author: Hellsoft
 */
import React from 'react';
import NewUserOption from './NewUserOption';
import UserInfoLogin from './UserInfoLogin';

const UserLoginComponent = (props) => {

    const {onSubmit, error, userName, password, onChange} = props;
    return (
        <div>
            <h1>Login to cpPlugins 3.0</h1>
            <div className="user-register-info-component">
                <UserInfoLogin
                    error={error}
                    onSubmit={onSubmit}
                    userName={userName}
                    password={password}
                    onChange={onChange}/>
                <br/>
                <NewUserOption/>
            </div>
        </div>

    );
};

export default UserLoginComponent;