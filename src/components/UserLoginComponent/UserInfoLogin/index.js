/**
 * File description: Displays the fields for login.
 * Author: Hellsoft
 */
import React from 'react';
import {UsernameData, PasswordData} from '../../shared/forms/inputs';

const UserInfoLogin = (props) => {
    const {error, onSubmit, userName, password, onChange} = props;
    return (
        <div>
            <UsernameData userName={userName} onChange={onChange}/>
            <PasswordData password={password} onChange={onChange}/>
            <div className="center-text">
                <a href="forgetLink">Olvido la contrasena?</a><br/>
            </div>
            <br/> {error && <strong>{error}</strong>}
            <div>
                <button onClick={onSubmit}>Log In</button>
            </div>
        </div>
    )

}

export default UserInfoLogin;
