/**
 * File description: Displays the information of the development team.
 * Author: Hellsoft
 */
import React from 'react';
import MemberInfo from './MemberInfo'
import './style.css';
import pablo from './../../members/pabli.jpeg';
import carlos from './../../members/carlos.jpeg';
import chaustre from './../../members/chaustre.jpeg';
import cocu from './../../members/cucu.jpeg';
import ailin from './../../members/ailin.jpg';
import leo from './../../members/leo.png';

const TeamInformationComponent = () => {
    return (
        <div id="TeamInformationComponent" className="team-container team-information-component">
            <h2 className="team-title">Meet the development team</h2>
            <div className="team-member-container">
                <MemberInfo
                    photo={pablo}
                    name="Pablo Andres Ariza Luna"
                    email="pablo_ariza@javeriana.edu.co" />
                <MemberInfo
                    photo={carlos}
                    name="Carlos Orlando Barón León"
                    email="baron_carlos@javeriana.edu.co" />
                <MemberInfo
                    photo={chaustre}
                    name="Santiago Chaustre Perdomo"
                    email="santiago-chaustre@javeriana.edu.co" />
                <MemberInfo
                    photo={cocu}
                    name="Andres Felipe Cocunubo Quintero"
                    email="andres-coconubo@javeriana.edu.co" />
                <MemberInfo
                    photo={ailin}
                    name="Ailin Ana Maria Rojas Bohorquez"
                    email="ailin.rojas@javeriana.edu.co" />
                <MemberInfo
                    photo={leo}
                    name="Leonardo Florez Valencia"
                    email="florez-l@javeriana.edu.co" />
            </div>
        </div>
    );
};

export default TeamInformationComponent;
