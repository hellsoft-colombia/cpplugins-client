/**
 * File description: Displays the information for each member of the development team.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types'

const MemberInfo = ( { photo, name, email } ) => (
    <div className="team-member">
      <img src={photo} alt= " "/>
      {name}<br />
      <a href={`mailto:${email}`}>{email}</a>
    </div>
)

MemberInfo.propTypes = {
  photo: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
}

export default MemberInfo