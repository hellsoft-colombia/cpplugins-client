/**
 * File description: Displays the result of an execution.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const VisualizerControlComponent = ( { barValue, handleBarChange} ) => {

        return (
            <div className="controlsContainer">
                VisualizerControlComponent
                <p>Size: </p>
                <input
                    id="typeinp"
                    type="range"
                    min="0" max="5"
                    value = { barValue }
                    onChange = { handleBarChange }
                    step="1" />
                <hr/>
                <p>Rotation: </p>
                <input
                    id="typeinp"
                    type="range"
                    min="0" max="5"
                    value = { barValue }
                    onChange = { handleBarChange }
                    step="1" />
            </div>
        );

}

VisualizerControlComponent.propTypes = {
    barValue: PropTypes.string.isRequired,
    handleBarChange: PropTypes.func.isRequired,
};

export default VisualizerControlComponent;