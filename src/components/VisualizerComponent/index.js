/**
 * File description: Displays the result that produced and execution.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import VisualizerControlComponent from './VisualizerControlComponent';
import UserActionsComponent from '../shared/UserActionsComponent';

const VisualizerComponent = ( { barValue, handleBarChange} ) => {
    return (
        <div>
            <VisualizerControlComponent
                barValue = { barValue }
                handleBarChange = { handleBarChange }
            ></VisualizerControlComponent>
            <UserActionsComponent>
                <button>Guardar resultado</button>
                <button>Volver al diseño</button>
            </UserActionsComponent>
        </div>
    );
};

VisualizerComponent.propTypes = {
    barValue: PropTypes.string.isRequired,
    handleBarChange: PropTypes.func.isRequired,
};

export default VisualizerComponent;