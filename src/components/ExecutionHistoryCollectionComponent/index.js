/**
 * File description: The component shows the information of history pipelines.
 * Author: Hellsoft
 */
import React from 'react';
import Proptypes from 'prop-types';
import HistoryEntryComponent from '../HistoryEntryComponent';
import InfiniteScroll from 'react-infinite-scroll-component';
import LoadingOverlay from 'react-loading-overlay';
import PacmanLoader from 'react-spinners/PacmanLoader';
import './style.css';

const renderExecutionHistoryItems = (executionHistory, resumePipelineExecution, stopPipelineExecution, cancelPipelineExecution, markEntryAsReaded, isHistory, reloadData) => (executionHistory.map((entry) => (
    <div key={entry.id
        ? 'Entry-' + entry.id + '/Result-' + entry.fileId
        : 'Result-' + entry.fileId}><HistoryEntryComponent
        key={entry.id
        ? 'Entry-' + entry.id
        : 'Result-' + entry.fileId}
        entry={entry}
        resumePipelineExecution={resumePipelineExecution}
        stopPipelineExecution={stopPipelineExecution}
        cancelPipelineExecution={cancelPipelineExecution}
        markEntryAsReaded={markEntryAsReaded}
        isHistory={isHistory}
        reloadData={reloadData}/></div>
)));

const isValidList = (executionHistory) => (executionHistory.length > 0);

const ExecutionHistoryCollectionComponent = ({
    executionHistory,
    fetchUserExecutionHistory,
    hasMore,
    resumePipelineExecution,
    stopPipelineExecution,
    cancelPipelineExecution,
    markEntryAsReaded,
    pageNumber,
    isLoading,
    isHistory,
    reloadData
}) => (
    <LoadingOverlay active={isLoading} spinner={< PacmanLoader />}>
        <div
            id="scrollableDiv"
            style={{
            overflowY: "auto",
            overflowX: "unset",
            height: "80vh"
        }}>

            <InfiniteScroll
                dataLength={executionHistory.length}
                hasMore={hasMore}
                next={_ => fetchUserExecutionHistory(pageNumber)}
                scrollableTarget="scrollableDiv">
                {isValidList(executionHistory)
                    ? renderExecutionHistoryItems(executionHistory, resumePipelineExecution, stopPipelineExecution, cancelPipelineExecution, markEntryAsReaded, isHistory, reloadData)
                    : <div className={"showError"}>
                        <p className={"errorMesssage"}>There is nothing here :)</p>
                    </div>}
            </InfiniteScroll>

        </div>
    </LoadingOverlay>
);

ExecutionHistoryCollectionComponent.propTypes = {
    executionHistory: Proptypes.array.isRequired,
    fetchUserExecutionHistory: Proptypes.func.isRequired,
    hasMore: Proptypes.bool.isRequired,
    resumePipelineExecution: Proptypes.func.isRequired,
    stopPipelineExecution: Proptypes.func.isRequired,
    cancelPipelineExecution: Proptypes.func.isRequired,
    markEntryAsReaded: Proptypes.func.isRequired,
    pageNumber: Proptypes.number.isRequired,
    isHistory: Proptypes.bool.isRequired,
    reloadData: Proptypes.func.isRequired
}

export default ExecutionHistoryCollectionComponent;