/**
 * File description: This component shows the buttons for the actions that will be performed on the pipelines.
 * It has the modal that is shown when the user wants to attach a file to a node.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import UserActionsComponent from '../shared/UserActionsComponent';
import Modal from 'react-awesome-modal';
import UserFilesComponent from '../shared/files/UserFilesComponent';

const DesignAreaComponent = ( {loadImageVisible, closeModalLoadImage, userImages, searchText, addFileNode,
        cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, fileSelectedHandler, resetChargedFile,
        onChangeFile, sendFile, showDetails, changeShowDetails, findFile, actualFilter, changeFilter,
        filteredUserFiles, sortAscending, sortDescending, isEditNode, isCustomFilter, handleSendPipeline, handleSavePipeline,
        handleNewPipeline, handleCancelCustomNode, undoAction, redoAction, undoActionEnable, redoActionEnable, file, that, handleStopPipeline} ) => {

    return (
        <div>
            {
                <Modal visible={loadImageVisible} width="90%" height="90%" effect="fadeInUp" onClickAway={closeModalLoadImage}>
                    <h2>Select file</h2>
                    <UserFilesComponent 
                        closeModalLoadImage={closeModalLoadImage}
                        userImages={userImages}
                        addFileNode={addFileNode}
                        cppIdParentNode={cppIdParentNode}
                        idActualParam={idActualParam}
                        deleteServersImage={deleteServersImage}
                        deleteFile={deleteFile}
                        fetchUserImages={fetchUserImages}
                        fileSelectedHandler={fileSelectedHandler}
                        file={file}
                        resetChargedFile={resetChargedFile}
                        onChangeFile={onChangeFile}
                        sendFile={sendFile}
                        showDetails={showDetails}
                        changeShowDetails={changeShowDetails}
                        searchFile = {findFile }
                        searchText= {searchText}
                        actualFilter = {actualFilter}
                        changeFilter = {changeFilter}
                        filteredUserFiles = {filteredUserFiles}
                        ascendingOrder = {sortAscending}
                        descendingOrder = {sortDescending}
                        that={that}
                        >    
                    </UserFilesComponent>
                </Modal>          
            }
            <div id="container" className="ui-layout-container container"> 
                <div className="design ui-droppable ui-layout-pane ui-layout-pane-center" id="canvas"/>
            </div>
            { 
                isEditNode || isCustomFilter ? 
                    isEditNode? 
                    <UserActionsComponent>
                        <button onClick = { handleSavePipeline }>Save</button>
                        <button onClick = { handleCancelCustomNode }>Cancel</button>
                        <button onClick = { undoAction } disabled={!undoActionEnable}>Undo</button>
                        <button onClick = { redoAction } disabled={!redoActionEnable}>Redo</button>
                    </UserActionsComponent>:
                    <UserActionsComponent>
                        <button onClick = { handleSavePipeline }>Save</button>
                        <button onClick = { undoAction } disabled={!undoActionEnable}>Undo</button>
                        <button onClick = { redoAction } disabled={!redoActionEnable}>Redo</button>
                    </UserActionsComponent>
                :
                <UserActionsComponent>
                    <button onClick = { handleSendPipeline }>Execute</button>
                    <button onClick= { handleStopPipeline }>Stop</button>
                    <button onClick = { handleSavePipeline }>Save</button>
                    <button onClick = { handleNewPipeline }>Clear</button>
                    <button onClick = { undoAction } disabled={!undoActionEnable}>Undo</button>
                    <button onClick = { redoAction } disabled={!redoActionEnable}>Redo</button>
            </UserActionsComponent>
            }
        </div>
    );
};

DesignAreaComponent.propTypes = {
    title: PropTypes.string.isRequired,
    handleSendPipeline: PropTypes.func.isRequired,
    handleNewPipeline: PropTypes.func.isRequired,
    handleSavePipeline: PropTypes.func.isRequired,
    handleCancelCustomNode: PropTypes.func.isRequired,
    isEditNode: PropTypes.bool.isRequired,
    undoAction: PropTypes.func.isRequired,
    redoAction: PropTypes.func.isRequired,
    undoActionEnable: PropTypes.bool.isRequired,
    redoActionEnable: PropTypes.bool.isRequired,
    loadImageVisible: PropTypes.bool.isRequired,
    closeModalLoadImage: PropTypes.func.isRequired,
    idActualParam: PropTypes.string,
    onChangeFile: PropTypes.func.isRequired,
    deleteServersImage: PropTypes.func.isRequired,
    sendFile: PropTypes.func.isRequired,
    deleteFile: PropTypes.func.isRequired,
    addFileNode: PropTypes.func.isRequired,
    userImages: PropTypes.array.isRequired,
    fetchUserImages: PropTypes.func.isRequired,
    isCustomFilter: PropTypes.bool.isRequired,
    handleStopPipeline: PropTypes.func.isRequired,
};

export default DesignAreaComponent;