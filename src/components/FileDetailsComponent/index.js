/**
 * File description: This contains the component that displays the information of the files.
 * Author: Hellsoft
 */
import React from 'react';
import FileInformationComponent from './FileInformationComponent';
import UserActionsComponent from '../shared/UserActionsComponent';
import PropTypes from 'prop-types';

const FileDetailsComponent = ({rawFile, selectedFile, designSelectedPipeline}) => {
    return (((selectedFile.fileName && selectedFile.fileName !== "") || rawFile) && (
        <div>
            <FileInformationComponent
                name={selectedFile.fileName}
                modificationDate={selectedFile.lastModificationDate}
                creationDate={selectedFile.creationDate}
                executionState={selectedFile.executionState}
                rawFile={rawFile}
                format={selectedFile.format}
                size={selectedFile.size}/> {!rawFile && <UserActionsComponent>
                <button onClick={designSelectedPipeline}>Design</button>
            </UserActionsComponent>
}
        </div>
    ));
};

FileDetailsComponent.propTypes = {
    selectedFile: PropTypes.object,
    designSelectedPipeline: PropTypes.func,
    rawFile: PropTypes.object
}

export default FileDetailsComponent;