/**
 * File description: This component shows the properties of the files.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';

const FileInformationComponent = ({
        rawFile, 
        name, modificationDate, creationDate, executionState
    }) => {
    let modificationDateString = modificationDate ? modificationDate.substring(0,10) : modificationDate;
    return (
        !rawFile?
        <div>
            <p>{`Name: ${name}`}</p>
            <p>{`Modification date: ${modificationDateString}`}</p>
            <p>{`Creation date: ${creationDate}`}</p>
            { executionState && <p>{`State: ${executionState}`}</p> }
        </div> :
        <div>
            <p>{`Name: ${rawFile.fileName}`}</p>
            <p>{`Creation date: ${rawFile.creationDate}`}</p>
            <p>{`Format: ${rawFile.format}`}</p>
            <p>{`Size: ${rawFile.size} MB`}</p>
        </div>
    );
};

FileInformationComponent.propTypes = {
    name: PropTypes.string, 
    modificationDate: PropTypes.string, 
    creationDate: PropTypes.string, 
    executionState: PropTypes.string,
}

export default FileInformationComponent;