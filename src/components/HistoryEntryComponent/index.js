/**
 * File description: This component shows the menu for the pipelines.
 * Author: Hellsoft
*/
import React from 'react';
import EntryInformation from './EntryInformation';
import ResultSetComponent from '../ResultSetComponent';
import Proptypes from 'prop-types';
import EntryMenuItem from './EntryMenuItem';
import {Row, Col} from 'react-flexbox-grid';
import './style.css';

const isRunning = (entry) => (entry.executionState === 'EXECUTING'
    ? true
    : false)

const isPaused = (entry) => (entry.executionState === 'STOPPED'
    ? true
    : false)

const HistoryEntryComponent = ({
    entry,
    resumePipelineExecution,
    stopPipelineExecution,
    cancelPipelineExecution,
    markEntryAsReaded,
    isHistory,
    reloadData
}) => (
    <div className={"itemRow"}>
        <Row>
            <Col xs={3}>
                <EntryInformation entry={entry}/>
            </Col>
            <Col xs={6}>
                {entry.imageResults && <ResultSetComponent entry={entry}/>
}
            </Col>
            <Col xs={3}>
                <EntryMenuItem
                    resumePipelineExecution={resumePipelineExecution}
                    stopPipelineExecution={stopPipelineExecution}
                    cancelPipelineExecution={cancelPipelineExecution}
                    markEntryAsReaded={markEntryAsReaded}
                    isRunning={isRunning(entry)}
                    isPaused={isPaused(entry)}
                    fileId={entry.fileId}
                    isHistory={isHistory}
                    reloadData={reloadData}/>
            </Col>
        </Row>
    </div>
)

HistoryEntryComponent.propTypes = {
    entry: Proptypes.object.isRequired,
    resumePipelineExecution: Proptypes.func.isRequired,
    stopPipelineExecution: Proptypes.func.isRequired,
    cancelPipelineExecution: Proptypes.func.isRequired,
    markEntryAsReaded: Proptypes.func.isRequired,
    isHistory: Proptypes.bool.isRequired,
    reloadData: Proptypes.func.isRequired
}

export default HistoryEntryComponent;