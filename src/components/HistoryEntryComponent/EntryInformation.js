/**
 * File description: This component contains the information of the pipelines.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExecutionStateIcon from './ExecutionStateIcon';

const EntryInformation = ({entry}) => (
    <div>
        <div
            style={{
            paddingLeft: "0.3vw",
            marginTop: "0",
            marginBottom: "0.5vh"
        }}>
            <span
                style={{
                fontSize: "2em",
                padding: "0"
            }}>{`${entry.fileName}`}</span>
            <ListItemIcon
                style={{
                paddingLeft: "0.9vw",
                marginTop: "0.3vh"
            }}>
                <ExecutionStateIcon executionState={entry.executionState}/>
            </ListItemIcon>
            <br/> {`${entry.description}`}
        </div>
        <div >
            {`${entry.executionState}`}<br/> {entry.lastExecutionTime && (`${entry.lastExecutionTime}`)}{entry.executionResult !== undefined && <div>
                {entry.executionResult && (
                    <div>
                        <span
                            style={{
                            fontWeight: "bold"
                        }}>Result:</span>
                        {`${entry.executionResult}`}
                    </div>
                )}</div>}
        </div>

    </div>
)

EntryInformation.propTypes = {
    entry: PropTypes.object.isRequired
};

export default EntryInformation;