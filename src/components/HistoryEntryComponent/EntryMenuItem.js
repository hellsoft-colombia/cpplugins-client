/**
 * File description: This component contains the button to interact with the pipelines.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/styles';
import StopIcon from '@material-ui/icons/Stop';
import Cancel from '@material-ui/icons/Cancel';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Done from '@material-ui/icons/Done';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const MarkAsReaded = styled(Done)({ color: 'green' });

const StopExecution = styled(StopIcon)({ color: '#DF4242' });

const ResumeExecution = styled(PlayArrow)({ color: '#0875B0' });

const CancelExecution = styled(Cancel)({ color: 'red' });

const EntryMenuItem = ({
    resumePipelineExecution,
    stopPipelineExecution,
    cancelPipelineExecution,
    markEntryAsReaded,
    isRunning,
    isPaused,
    fileId,
    isHistory,
    reloadData
}) => !isHistory && (
    <div>
        {isPaused
            ? (
                <div
                    onClick={_ => {
                        resumePipelineExecution(fileId);
                        reloadData();
                    }}
                    style={{
                        backgroundColor: "white",
                        color: "black",
                        border: "2px solid #022b3a",
                        width: "13vw",
                        height: "3vw",
                        alignItems: "center",
                        display: "flex",
                        margin: "0.4vw auto",
                    }}>
                    <ListItemIcon
                        style={{
                            display: "flex",
                            verticalAlign: "middle",
                        }}><ResumeExecution fontSize="large" /></ListItemIcon>
                    <span
                        style={{
                            fontSize: "1em",
                            padding: "0.5em",
                            verticalAlign: "middle",
                        }}>Resume</span>
                </div>

            )
            : (
                <div
                    onClick={_ => {
                        stopPipelineExecution(fileId);
                        reloadData();
                    }}
                    style={{
                        backgroundColor: "white",
                        color: "black",
                        border: "2px solid #022b3a",
                        width: "13vw",
                        height: "3vw",
                        alignItems: "center",
                        display: "flex",
                        margin: "0.4vw auto",
                    }}>
                    <ListItemIcon
                        style={{
                            display: "flex",
                            alignItems: "center",
                        }}><StopExecution fontSize="large" /></ListItemIcon>
                    <span
                        style={{
                            fontSize: "1em",
                            padding: "0.5em",
                        }}>Stop</span>
                </div>

            )}
        {isRunning || isPaused
            ? (
                <div
                    onClick={_ => {
                        cancelPipelineExecution(fileId);
                        reloadData();
                    }}
                    style={{
                        backgroundColor: "white",
                        color: "black",
                        border: "2px solid #022b3a",
                        width: "13vw",
                        height: "3vw",
                        alignItems: "center",
                        display: "flex",
                        margin: "0.4vw auto",
                    }}>
                    <ListItemIcon
                        style={{
                            display: "flex",
                            verticalAlign: "middle",
                        }}><CancelExecution fontSize="large" /></ListItemIcon>
                    <span
                        style={{
                            fontSize: "1em",
                            padding: "0.5em",
                            verticalAlign: "middle",
                        }}>Cancel</span>
                </div>

            )
            : (
                <div
                    onClick={_ => {
                        markEntryAsReaded(fileId);
                        reloadData();
                    }}
                    style={{
                        backgroundColor: "white",
                        color: "black",
                        border: "2px solid #022b3a",
                        width: "13vw",
                        height: "3vw",
                        alignItems: "center",
                        display: "flex",
                        margin: "0.4vw auto",
                        overflow: "hidden"
                    }}>
                    <ListItemIcon
                        style={{
                            display: "flex",
                            verticalAlign: "middle",
                        }}><MarkAsReaded fontSize="large" /></ListItemIcon>
                    <span
                        style={{
                            fontSize: "1em",
                            padding: "0.5em",
                            verticalAlign: "middle",
                        }}>Archive</span>
                </div>

            )
        }
    </div>
);

EntryMenuItem.propTypes = {
    resumePipelineExecution: PropTypes.func.isRequired,
    stopPipelineExecution: PropTypes.func.isRequired,
    cancelPipelineExecution: PropTypes.func.isRequired,
    markEntryAsReaded: PropTypes.func.isRequired,
    isRunning: PropTypes.bool.isRequired,
    isPaused: PropTypes.bool.isRequired,
    isHistory: PropTypes.bool.isRequired,
    reloadData: PropTypes.func.isRequired
}

export default EntryMenuItem;