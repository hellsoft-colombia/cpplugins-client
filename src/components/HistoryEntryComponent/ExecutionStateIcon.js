/**
 * File description: This component contains the constants for the different icons for the pipeline state.
 * Author: Hellsoft
*/
import React from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import StopIcon from '@material-ui/icons/Stop';
import MoodBad from '@material-ui/icons/MoodBad';
import Report from '@material-ui/icons/Report';
import DirectionsRun from '@material-ui/icons/DirectionsRun';
import AcUnit from '@material-ui/icons/AcUnit';
import {styled} from '@material-ui/styles';
import Cancel from '@material-ui/icons/Cancel';

const FinishedPipeline = styled(CheckCircleIcon)({color: 'green'});

const ExecutingPipeline = styled(DirectionsRun)({color: '#FFBF00'});

const CancelledPipeline = styled(Cancel)({color: '#E73636'});

const StoppedPipeline = styled(StopIcon)({color: '#DF4242'});

const ExpiredPipeline = styled(Report)({color: '#F8DA1F'});

const NotRecognized = styled(MoodBad)({color: '#EC7C7C'});

const NotExecuting = styled(AcUnit)({color: '#339CFF'});

const ExecutionStateIcon = ({executionState}) => {
    switch (executionState) {
        case 'STOPPED':
            return (<StoppedPipeline fontSize="large"/>);
        case 'CANCELLED':
            return (<CancelledPipeline fontSize="large"/>);
        case 'EXECUTING':
            return (<ExecutingPipeline fontSize="large"/>);
        case 'EXPIRED':
            return (<ExpiredPipeline fontSize="large"/>);
        case 'FINISHED':
            return (<FinishedPipeline fontSize="large"/>);
        case 'NOT_EXECUTING':
            return (<NotExecuting fontSize="large"/>);
        default:
            return (<NotRecognized fontSize="large"/>);
    }
}

export default ExecutionStateIcon;