/**
 * File description: Displays a context menu with actions that can be performed on files.
 * Author: Hellsoft
 */
import React from 'react';
import {Menu, Item, Separator} from 'react-contexify';
import PropTypes from 'prop-types';

const FileMenuItem = ({EditFileOnClick, SelectFileOnClick, DeleteFileOnClick}) => (
    <Menu id='menu_id'>
        <Item onClick={EditFileOnClick}>Edit</Item>
        <Item onClick={DeleteFileOnClick}>Delete</Item>
        <Separator/>
        <Item onClick={SelectFileOnClick}>Properties</Item>
    </Menu>
);

FileMenuItem.propTypes = {
    EditFileOnClick: PropTypes.func.isRequired,
    SelectFileOnClick: PropTypes.func.isRequired,
    DeleteFileOnClick: PropTypes.func.isRequired,
}

export default FileMenuItem;