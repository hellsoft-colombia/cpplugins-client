/**
 * File description: Displays buttons to filter the pipelines by a cryterial.
 * Author: Hellsoft
 */
import React from 'react';
import {Grid, Col} from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import { filterSearchCryteria,  filterOrderCryteria, filterCryteria } from '../../utils/SearchFunctions';

const FilterSearchComponent = ( { filterPipelines, setAllPipelines,
    changeSearchOrderCryteria, changeSearchCryteria, defaultSearchCryteria, defaultSearchOrder } ) => {
    return ( 
        <div>  
            <Grid fluid className="pipelines-nav">             
                <Col className="FilterButtonGroupSearch" xs={8}>
                                    {/*     
                    <button onClick = {setAllPipelines}>All pipelines</button>
                    <button onClick = {filterPipelines}>Excecution pipelines</button>
                                    */}
                </Col>

                <Col className="FilterTextGroupSearch" xs={4}>
                    <button 
                        onClick = { changeSearchCryteria }>
                        { filterCryteria(filterSearchCryteria, defaultSearchCryteria).criteria }
                    </button>
                    <button 
                        onClick = { changeSearchOrderCryteria }>
                        { filterCryteria(filterOrderCryteria    , Number(defaultSearchOrder)).order }
                    </button>
                </Col>
             </Grid>
        </div>
    );                     
}

FilterSearchComponent.propTypes = {
    changeSearchOrderCryteria: PropTypes.func.isRequired,
    changeSearchCryteria: PropTypes.func.isRequired,
    defaultSearchCryteria: PropTypes.number.isRequired,
    defaultSearchOrder: PropTypes.bool.isRequired,
    filterPipelines: PropTypes.func.isRequired,
    setAllPipelines: PropTypes.func.isRequired,
};

export default FilterSearchComponent;