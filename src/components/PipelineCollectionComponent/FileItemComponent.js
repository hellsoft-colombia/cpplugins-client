/**
 * File description: Displays the file name of a pipeline.
 * Author: Hellsoft
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DescriptionIcon from '@material-ui/icons/Description';

class FileItemComponent extends Component {
    render(){
        let {fileName} = this.props;     
        
        return (
            <div className="file-item">
                <DescriptionIcon fontSize="large"></DescriptionIcon>
                <p> { fileName } </p>
            </div>
        );
   }
    
};

FileItemComponent.propTypes = {
    fileName: PropTypes.string.isRequired,
};

export default FileItemComponent;