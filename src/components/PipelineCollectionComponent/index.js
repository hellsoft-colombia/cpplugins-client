/**
 * File description: Displays a list of pipelines and use other components to show the information.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Grid, Row, Col} from 'react-flexbox-grid';
import {MenuProvider} from 'react-contexify';
import FilterSearchComponent from './FilterSearchComponent';
import FileItemComponent from './FileItemComponent';
import FileMenuItem from './FileMenuItem';

class PipelineCollectionComponent extends Component {

    editFileOnClick = ({event, props}) => {
        let {selectFileAction} = this.props;
        selectFileAction(props);
    }

    selectFileOnClick = ({event, props}) => {
        let {setSelectedFileProperties, cleanRawFile} = this.props;
        setSelectedFileProperties(props);
        cleanRawFile();
    }

    deleteFileOnClick = ({event, props}) => {
        let {deleteFileAction} = this.props;
        deleteFileAction(props);
    }

    renderPipelineItems = (pipelineItems) => {
        return (
            pipelineItems.map(
                (item) =>(
                    <MenuProvider 
                        key={item.fileId} 
                        id="menu_id" 
                        component="div" 
                        data={item}
                        storeRef={false}>
                        <FileItemComponent key={item.fileId} fileName={item.fileName}/>
                    </MenuProvider>
                )
            )
        ) 
    }

    validFileItems = (fileItems) => {
        if (fileItems.length >= 0) 
            return true;
        return false;
    }

    render() {
        let {
            title,
            pipelineItems,
            changeSearchOrderCryteria,
            changeSearchCryteria,
            defaultSearchCryteria,
            defaultSearchOrder,
            filterPipelines,
            setAllPipelines
        } = this.props;
        return (
            <div>
                <h1>{ title }</h1>
                <Grid fluid className="pipelines">
                    <Row>
                        <Col xs={12}>
                            <FilterSearchComponent
                                changeSearchOrderCryteria={changeSearchOrderCryteria}
                                changeSearchCryteria={changeSearchCryteria}
                                defaultSearchCryteria={defaultSearchCryteria}
                                defaultSearchOrder={defaultSearchOrder}
                                filterPipelines={filterPipelines}
                                setAllPipelines={setAllPipelines}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} className="pipelines-content">
                            {
                                this.validFileItems(pipelineItems) ?
                                this.renderPipelineItems(pipelineItems) :
                                <p>No response from server</p>
                            }
                            <FileMenuItem 
                                EditFileOnClick={this.editFileOnClick} 
                                SelectFileOnClick={this.selectFileOnClick}
                                DeleteFileOnClick={this.deleteFileOnClick}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

PipelineCollectionComponent.propTypes = {
    title: PropTypes.string.isRequired,
    fileItems: PropTypes.array,
    changeSearchOrderCryteria: PropTypes.func.isRequired,
    changeSearchCryteria: PropTypes.func.isRequired,
    defaultSearchCryteria: PropTypes.number.isRequired,
    defaultSearchOrder: PropTypes.bool.isRequired,
    selectFileAction: PropTypes.func.isRequired,
    filterPipelines: PropTypes.func.isRequired,
    setAllPipelines: PropTypes.func.isRequired,
    cleanRawFile: PropTypes.func.isRequired,
};

export default PipelineCollectionComponent;