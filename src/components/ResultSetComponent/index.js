/**
 * File description: Displays the result images in the results page through a slider.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import Slider from "react-slick";
import FileItemComponent from './FileItemComponent';
import {generateRandomColor} from '../../utils/ColorFunctions';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    arrows: true
};

const ResultSetComponent = ({entry}) => {
    let {imageResults} = entry;
    if (imageResults.length >= 1) {
        return (
            <div style={{textAlign: "center", paddingTop: "0", marginTop:"0"}}>
                <h3 style={{paddingTop: "2vh", marginTop:"0"}} >Execution Results</h3>
                <Slider {...settings}>
                    {imageResults.map((item) => (<FileItemComponent
                        key={"Result" + item.fileId}
                        fileItem={item}
                        color={generateRandomColor()}/>))}
                </Slider>
            </div>
        );
    }
    return (
        <br/>
    )
}

ResultSetComponent.propTypes = {
    entry: PropTypes.object.isRequired,
};

export default ResultSetComponent;