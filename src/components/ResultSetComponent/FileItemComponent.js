/**
 * File description: Displays the information of a file.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import {styled} from '@material-ui/styles';
import AttachFile from '@material-ui/icons/AttachFile';
import {urlGetSpecificImage} from '../../api/urls/files.urls';

const FileIcon = styled(AttachFile)({color: '#a3a3a3'});

const FileItemComponent = ({fileItem, color}) => {
    return (
        <div style={{alignItems: 'center', display: 'flex', justifyContent: 'center'}}>
            <FileIcon fontSize="large"/>
            <a href={`${urlGetSpecificImage}${fileItem.fileId}`} target={"_blank"}>
                {fileItem
                    .fileName
                    .split('.')[0]}
            </a>
        </div>
    )
}

FileItemComponent.propTypes = {
    fileItem: PropTypes.object.isRequired,
    color: PropTypes.string.isRequired
};

export default FileItemComponent;