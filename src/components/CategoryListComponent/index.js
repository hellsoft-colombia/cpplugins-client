/**
 * File description: This component shows the buttons that filter the pipelines by:
 * -> Pipelines
 * -> Custom nodes
 * -> My files
 * Also, it constains the component for load/create new pipelines.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import NewPipelineComponent from '../NewPipelineComponent';

const CategoryListComponent = ({
    isPipelinesAdministration,
    switchToPipelines,
    switchToFiles,
    switchToCustomNodes,
    loadFileVisible,
    openModalLoadFile,
    closeModalLoadFile,
    newPipelineVisible,
    openModalNewPipeline,
    closeModalNewPipeline,
    onChangeFile,
    onFileFormSubmit,
    newPipelineName,
    newPipelineDescription,
    onChange,
    onSubmit,
    changeCheckedState,
    title
}) => {
    return (
        <div>
            <NewPipelineComponent
                newPipelineVisible = { newPipelineVisible }
                openModalNewPipeline = { openModalNewPipeline }
                closeModalNewPipeline = { closeModalNewPipeline }
                newPipelineName={newPipelineName}
                newPipelineDescription={newPipelineDescription}
                onChange={onChange}
                onSubmit={onSubmit}
                changeCheckedState={changeCheckedState}
                title={title}
            />
            <button onClick={switchToPipelines}>
                My pipelines
            </button>
            <button onClick={switchToCustomNodes}>
                Custom nodes
            </button>
            <button onClick={switchToFiles}>
                My files
            </button>
        </div>
    );
};

CategoryListComponent.propTypes = {
    isPipelinesAdministration: PropTypes.func.isRequired,
    switchToPipelines: PropTypes.func.isRequired,
    switchToFiles: PropTypes.func.isRequired,
    loadFileVisible: PropTypes.bool.isRequired,
    openModalLoadFile: PropTypes.func.isRequired,
    closeModalLoadFile: PropTypes.func.isRequired,
    newPipelineVisible: PropTypes.bool.isRequired,
    onChangeFile: PropTypes.func.isRequired,
    onFileFormSubmit: PropTypes.func.isRequired,
    newPipelineName: PropTypes.object.isRequired,
    newPipelineDescription: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    changeCheckedState: PropTypes.func.isRequired,
    switchToCustomNodes: PropTypes.func.isRequired
};

export default CategoryListComponent;
