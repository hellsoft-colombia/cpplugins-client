/**
 * File description: The component describes the options that will be displayed when the user makes right clic 
 * in the files.  
 * Author: Hellsoft
 */
import React from 'react';
import {Menu, Item, Separator} from 'react-contexify';
import PropTypes from 'prop-types';

const ImgMenuItem = ({SelectFileOnClick, DeleteFileOnClick}) => (
    <Menu id='menu_id'>
        <Item onClick={DeleteFileOnClick}>Delete</Item>
        <Separator/>
        <Item onClick={SelectFileOnClick}>Properties</Item>
    </Menu>
);

ImgMenuItem.propTypes = {
    SelectFileOnClick: PropTypes.func.isRequired,
    DeleteFileOnClick: PropTypes.func.isRequired,
}

export default ImgMenuItem;