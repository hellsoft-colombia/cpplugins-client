/**
 * File description: Component that displays the body for each page, according to the parameter.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';

const AppBody = ({body}) => {
    return (
        <div className="app-body">
            {body}
        </div>
    );
};

AppBody.propTypes = {
    body: PropTypes.element.isRequired,
};

export default AppBody;