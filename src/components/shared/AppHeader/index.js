/**
 * File description: Displays the information shown in the header of the application.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import HeaderActionsComponent from './HeaderActionsComponent';
import PropTypes from 'prop-types';
import {getUserInformation, getAuthenticationState} from '../../../selectors/authenticatedUserState';
import {fetchUserInfo, userLogout} from '../../../actions';

class AppHeader extends Component {
    componentDidMount(){
        let {loggedInState, user, fetchUserInfo} = this.props;
        if (loggedInState){
            if (user.userName === "" || user === undefined) {
                fetchUserInfo();
            }
        }
    }

    handleLogout = () => {
        this.props.userLogout();
    }

    render() {
        return (
            <div className="app-header">
                <h1>{this.props.header}</h1>
                <HeaderActionsComponent 
                    user={this.props.user} 
                    isUserLoggedIn={this.props.loggedInState}
                    handleLogout={this.handleLogout}
                />
            </div>
        )
    };
};

const mapStateToProps = state => ({user: getUserInformation(state), loggedInState: getAuthenticationState(state)});

const mapDispatchToProps = dispatch => ({
    userLogout: _ => dispatch(userLogout()),
    fetchUserInfo: _ => dispatch(fetchUserInfo())
});

AppHeader.propTypes = {
    header: PropTypes.string.isRequired,
    fetchUserInfo: PropTypes.func.isRequired,
    loggedInState: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps, null)(AppHeader);