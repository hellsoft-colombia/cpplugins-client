/**
 * File description: Displays the name of the user that has login in the application.
 * Author: Hellsoft
 */
import React from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const UserComponent = ({userFullName}) => {
    return (
        <div>
            <AccountCircleIcon fontSize="large"></AccountCircleIcon>
            <p>{`Welcome, ${userFullName}`}</p>
        </div>
    );
};

export default UserComponent;