/**
 * File description: Displays the following links in the header:
 * -> Information
 * -> About
 * Author: Hellsoft
 */
import React from 'react';
import {Link} from 'react-router-dom';
import NotificationsContainer from './NotificationsContainer';
import UserComponent from './UserComponent';
import {LOGIN_ROUTE, REGISTER_ROUTE, FILE_AREA_ROUTE} from '../../../constants/routes';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const HeaderActionsComponent = ({isUserLoggedIn, user, handleLogout}) => {
    return (
        <div className="Navbar">
            {!isUserLoggedIn
                ? <nav className="Navbar__Items">
                        <div className="Navbar__Link">
                            <a href="/#InformativeComponent">Information</a>
                        </div>
                        <div className="Navbar__Link">
                            <a href="/#TeamInformationComponent">About</a>
                        </div>
                    </nav>
                : <div>
                    <div className="Navbar__Link Navbar__Link-brand">
                        <Link to={FILE_AREA_ROUTE}>cpPlugins 3.0</Link>
                    </div>
                </div>
}
            <nav className="Navbar__Items--right">
                {!isUserLoggedIn
                    ? <div className="Navbar__Items">
                            <div className="Navbar__Link">
                                <Link to={LOGIN_ROUTE}>Login</Link>
                            </div>
                            <div className="Navbar__Link">
                                <Link to={REGISTER_ROUTE}>Register</Link>
                            </div>
                        </div>
                    : <div className="Navbar__Items">
                        <div className="Navbar__Link">
                            <NotificationsContainer/>
                        </div>
                        <div className="Navbar__Link">
                            <UserComponent userFullName={`${user.firstName} ${user.lastName}`}/>
                        </div>
                        <div className="Navbar__Link">
                            <div>
                                <ExitToAppIcon fontSize="large"></ExitToAppIcon>
                                <p className = "logout" onClick={ handleLogout }>Logout</p>
                            </div>
                        </div>
                    </div>
}
            </nav>
        </div>
    );
}

export default HeaderActionsComponent;