/**
 * File description: This container executes some functions to present the information of
 * notifications.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Autorenew from '@material-ui/icons/Autorenew';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Pause from '@material-ui/icons/Pause';
import Done from '@material-ui/icons/Done';
import PlayArrow from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import {styled} from '@material-ui/styles';
import Badge from '@material-ui/core/Badge';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {fetchUserNotifications, stopExecutingPipeline, resumeExecutingPipeline, checkExecutingPipeline} from '../../../actions';
import {getPipelineNotifications} from '../../../selectors/filesState';
import {filter, some, differenceWith, isEqual} from 'lodash';
import {TIME_REFRESH_NOTIFICATIONS} from '../../../constants';
import {Link} from 'react-router-dom';

const FinishedPipeline = styled(CheckCircleIcon)({color: 'green'});

const MarkAsReaded = styled(Done)({color: 'green'});

const ExecutingPipeline = styled(Autorenew)({color: '#FFBF00'});

const StopExecution = styled(StopIcon)({color: '#DF4242'});

const ResumeExecution = styled(PlayArrow)({color: '#0875B0'});

const PausedPipeline = styled(Pause)({color: '#F5D44C'});

class NotificationsContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            showNotification: false,
            requestedPipelines: [],
            incomingPipelines: []
        }
    };

    fetchNewNotificationsForUser = _ => {
        this
            .props
            .fetchUserNotifications()
            .then(_ => {
                if (differenceWith(this.props.userPipelines, this.state.requestedPipelines, isEqual).length > 0) {
                    let newNotification = differenceWith(this.props.userPipelines, this.state.requestedPipelines, isEqual);
                    this.setState({requestedPipelines: this.props.userPipelines, incomingPipelines: newNotification});
                } else {
                    this.setState({incomingPipelines: []});
                };
            })
    }

    componentDidMount() {
        if (this.props.userPipelines === undefined) {
            this
                .props
                .fetchUserNotifications()
                .then(_ => {
                    this.setState({requestedPipelines: this.props.userPipelines, incomingPipelines: this.props.userPipelines})
                });
            setInterval(this.fetchNewNotificationsForUser, TIME_REFRESH_NOTIFICATIONS);
        }
    };

    isPipelineOver = _ => {
        if (some(this.state.incomingPipelines, {'executionState': 'FINISHED'})) {
            return true;
        }
        return false;
    };

    handleClick = event => {
        this.fetchNewNotificationsForUser();
        this.setState({
            anchorEl: event.currentTarget,
            showNotification: !this.state.showNotification
        });
    };

    handleClose = () => {
        this.setState({anchorEl: null, incomingPipelines: []});
    };

    showPipelines = _ => {
        const finishedPipelines = filter(this.state.requestedPipelines, p => p.executionState === 'FINISHED');
        const executingPipelines = filter(this.state.requestedPipelines, p => p.executionState === 'EXECUTING');
        const pausedPipelines = filter(this.props.userPipelines, p => p.executionState === 'STOPPED');
        return (
            <Menu
                id="notifications-pipeline"
                anchorEl={this.state.anchorEl}
                getContentAnchorEl={null}
                anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
            }}
                transformOrigin={{
                vertical: "top",
                horizontal: "center"
            }}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                PaperProps={{
                style: {
                    maxHeight: 600,
                    width: 160,
                    fontFamily: "Roboto"
                }
            }}
                onClose={_ => this.handleClose()}>
                {finishedPipelines.length !== 0 || executingPipelines.length !== 0
                    ? <div>
                            {finishedPipelines.map(p => <MenuItem key={p.fileId}>
                                <ListItemIcon className="list-item-icon">
                                    <FinishedPipeline fontSize="large"/>
                                </ListItemIcon>
                                <p>
                                    {p
                                        .fileName
                                        .split('.')[0]}
                                    <br/> {p.executionState}
                                    <br/> {p.finishDate}
                                </p>
                                <MarkAsReaded
                                    fontSize="large"
                                    onClick={e => {
                                    checkExecutingPipeline(p.fileId);
                                    this.fetchNewNotificationsForUser(e);
                                    this.setState({
                                        anchorEl: null
                                    });
                                }}/>
                            </MenuItem>)
}
                            {executingPipelines.map(p => <MenuItem key={p.fileId}>
                                <ListItemIcon className="list-item-icon">
                                    <ExecutingPipeline fontSize="large"/>
                                </ListItemIcon>
                                <p>
                                    {p
                                        .fileName
                                        .split('.')[0]}
                                    <br/> {p.executionState}
                                </p>
                                <StopExecution
                                    fontSize="large"
                                    onClick={e => {
                                    stopExecutingPipeline(p.fileId);
                                    this.fetchNewNotificationsForUser(e);
                                    this.setState({
                                        anchorEl: null,
                                    });
                                }}/>
                            </MenuItem>)
}
                            {pausedPipelines.map(p => <MenuItem key={p.fileId}>
                                <ListItemIcon className="list-item-icon">
                                    <PausedPipeline fontSize="large"/>
                                </ListItemIcon>
                                <p>
                                    {p
                                        .fileName
                                        .split('.')[0]}
                                    <br/> {p.executionState}
                                </p>
                                <ResumeExecution
                                    fontSize="large"
                                    onClick={e => {
                                    console.log(e);
                                    resumeExecutingPipeline(p.fileId);
                                    this.fetchNewNotificationsForUser(e);
                                    this.setState({
                                        anchorEl: null,
                                    });
                                }}/>
                            </MenuItem>)
}
                            <MenuItem
                                style={{
                                justifyContent: 'center'
                            }}>
                                <Link to={'/execution-history'}>See complete history</Link>
                            </MenuItem>
                        </div>
                    : <MenuItem><p>There are no pipelines</p></MenuItem>
}
            </Menu>
        );
    };

    render() {
        return (
            <div>
                {this.isPipelineOver()
                    ? <Badge
                            badgeContent={filter(this.state.incomingPipelines, p => p.executionState === 'FINISHED').length}
                            color="secondary"
                            invisible={this.state.showNotification}>
                            <NotificationsIcon fontSize="large" onClick={e => this.handleClick(e)}></NotificationsIcon>
                        </Badge>
                    : <NotificationsIcon fontSize="large" onClick={e => this.handleClick(e)}></NotificationsIcon>
}
                {this.showPipelines()}
            </div>
        );
    };
};

NotificationsContainer.propTypes = {
    fetchUserNotifications: PropTypes.func.isRequired,
    stopExecutingPipeline: PropTypes.func.isRequired,
    userPipelines: PropTypes.array,
    resumeExecutingPipeline: PropTypes.func.isRequired,
    checkExecutingPipeline: PropTypes.func.isRequired
};

const mapStateToProps = state => ({userPipelines: getPipelineNotifications(state)});

const mapDispatchToProps = dispatch => ({
    fetchUserNotifications: _ => dispatch(fetchUserNotifications()),
    stopExecutingPipeline: pipeline => dispatch(stopExecutingPipeline(pipeline)),
    resumeExecutingPipeline: pipelineId => dispatch(resumeExecutingPipeline(pipelineId)),
    checkExecutingPipeline: pipelineId => dispatch(checkExecutingPipeline(pipelineId))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsContainer);