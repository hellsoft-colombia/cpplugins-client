/**
 * File description: Displays the actions passed in the parameters of the component.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';

const UserActionsComponent = ( { children } ) => {
    
    return (
        <div className = "user-actions">
            <div>
                <div>{ children }</div>
            </div>
        </div>
    );
};

UserActionsComponent.propTypes = {
    children: PropTypes.node.isRequired,    
};

export default UserActionsComponent;
