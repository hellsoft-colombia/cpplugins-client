/**
 * File description: This component present the header and the body for each page, according to the parameters.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import AppHeader from './AppHeader';
import AppBody from './AppBody';
import './style.css';


const AppFrame = ( {header, body} ) => {
    return (
        <div className="app-container">
            <AppHeader className="header" header = {header}></AppHeader>
            <AppBody body = {body} ></AppBody>      
        </div>
    );
};

AppFrame.propTypes = {
    header: PropTypes.string.isRequired,
    body: PropTypes.element.isRequired,
};

export default AppFrame;