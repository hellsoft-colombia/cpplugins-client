/**
 * File description: Displays the field of the username.
 * Author: Hellsoft
 */
import React from 'react'

const UsernameData = ({userName, onChange}) => {
    const type = "text";
    const name = "userName";
    const label = "Username";
    return (
        <div className="user-register-info">
            <div>
                <label htmlFor={name}>{label}</label>
                <div>
                    <input
                        name={name}
                        value={userName.value}
                        placeholder={userName.placeholder}
                        type={type}
                        onChange={e => onChange(e)}/> <br/>
                        {userName.error && <strong>{userName.error}</strong>}
                </div>
            </div>
        </div>
    )
}

export default UsernameData;
