/**
 * File description: Displays the field of the user email.
 * Author: Hellsoft
 */
import React from 'react';

const EmailData = ({ userEmail, onChange }) => {
    const type = "email";
    const name = "userEmail";
    const label = "e-mail";
    return (

        <div className="user-register-info">
            <label htmlFor={name}>{label}</label>
            <div>
                <input
                    name={name}
                    value={userEmail.value}
                    placeholder={userEmail.placeholder}
                    type={type}
                    onChange={e => onChange(e)} /> <br/>
                    {userEmail.error && <strong>{userEmail.error}</strong>}
            </div>
        </div>

    );
}

export default EmailData;