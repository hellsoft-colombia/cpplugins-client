/**
 * File description: Displays the field of the password.
 * Author: Hellsoft
 */
import React from 'react'

const PasswordData = ({password, onChange}) => {

    const name = "password";
    const label = "Password";
    const type = "password";
    return (
        <div className="user-register-info">
            <label htmlFor={name}>{label}</label>
            <div>
                <input
                    name={name}
                    value={password.value}
                    placeholder={password.placeholder}
                    type={type}
                    onChange={e => onChange(e)}/> <br/>
                    {password.error && <strong>{password.error}</strong>}
            </div>
        </div>
    );
}

export default PasswordData;