/**
 * File description: Displays the field of the name and last name of the user.
 * Author: Hellsoft
 */
import React from 'react'

const NameData = ({firstName, lastName, onChange}) => {
    const type = "text";

    const name_fn = "firstName";
    const label_fn = "First name";

    const name_ln = "lastName";
    const label_ln = "Last name";

    return (
        <div>
            <div className="user-register-info">
                <label htmlFor={name_fn}>{label_fn}</label>
                <div>
                    <input
                        name={name_fn}
                        value={firstName.value}
                        placeholder={firstName.placeholder}
                        type={type}
                        onChange={e => onChange(e)}/> <br/>
                        {firstName.error && <strong>{firstName.error}</strong>}
                </div>
            </div>
            <br/>
            <div className="user-register-info">
                <label htmlFor={name_ln}>{label_ln}</label>
                <div>
                    <input
                        name={name_ln}
                        value={lastName.value}
                        placeholder={lastName.placeholder}
                        type={type}
                        onChange={e => onChange(e)}/> <br/>
                        {lastName.error && <strong>{lastName.error}</strong>}
                </div>
            </div>
        </div>
    );
}

export default NameData;
