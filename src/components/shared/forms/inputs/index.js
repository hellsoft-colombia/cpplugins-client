/**
 * File description: Contains the location of the forms used in the application.
 * Author: Hellsoft
 */
import UsernameData from './UsernameData';
import PasswordData from './PasswordData';
import EmailData from './EmailData';
import NameData from './NameData';

export {
    UsernameData,
    PasswordData,
    EmailData,
    NameData
};