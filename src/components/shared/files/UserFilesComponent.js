/**
 * File description: Contains functions to display and interact with the files that the user uploads.
 * Author: Hellsoft
 */
import React from 'react';
import PropTypes from 'prop-types';
import {Grid, Col, Row} from 'react-flexbox-grid';
import {chunk} from 'lodash';
import ImageIcon from '@material-ui/icons/Image';
import {differenceWith, isEqual} from 'lodash';
import ImgMenuItem from '../../FileCollectionComponent/ImgMenuItem';
import {MenuProvider} from 'react-contexify';

const showDetailsGrid = (showDetails) => {
    const {fileName, creationDate, size, format} = showDetails;
    return (
        <Col xs={4}>
            <Row center="xs">
                <h4>{fileName}</h4>
            </Row>
            <Row >
                Format: {format}
            </Row>
            <Row >
                Uploaded day: {creationDate}
            </Row>
            <Row >
                Size: {size}
                MB
            </Row>
        </Col>
    );
};

const renderFileList = (chunks, addFileNode, cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, closeModal, changeShowDetails, that) => (chunks.map(chunk => <Col xs={12} className="files-content" key={`${chunk.length}userfiles`}>
    {chunk.map(img => <MenuProvider
        key={img.fileId}
        data={img}
        id="menu_id"
        component="div"
        storeRef={false}>
        <div key={img.fileId}>
            <div className="file-item">
                <p>{img.fileName}</p>
                <div key={`${img.fileId}img`}>
                    {cppIdParentNode
                        ? <ImageIcon onClick={_ => changeShowDetails(img)} fontSize="large"></ImageIcon>
                        : <ImageIcon fontSize="large"></ImageIcon>
}
                </div>
                <div key={`${img.fileId}add`} className="img-buttons">
                    {closeModal && <button
                        onClick={() => {
                        const param = {
                            nodeId: cppIdParentNode,
                            parameterId: idActualParam,
                            value: {
                                fileId: img.fileId,
                                fileName: img.fileName
                            }
                        };
                        addFileNode(param);
                        closeModal(cppIdParentNode, img.fileName, img.fileId);
                    }}>Select</button>
}
                    {cppIdParentNode && <button
                        onClick={_ => deleteServersImage(deleteFile, img.fileId, fetchUserImages, that)}>Delete</button>
}
                </div>
            </div>
        </div>
    </MenuProvider>)
}
</Col>))

const createFileColumn = (imgs, addFileNode, cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, closeModal, changeShowDetails, that) => {
    const chunks = chunk(imgs, 4);
    return (chunks.length > 0
        ? <div >
                {renderFileList(chunks, addFileNode, cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, closeModal, changeShowDetails, that)}
                {< ImgMenuItem
                DeleteFileOnClick = {
                    ({props}) => deleteServersImage(deleteFile, props.fileId, fetchUserImages, that)
                }
                SelectFileOnClick = {
                    ({props}) => changeShowDetails(props)
                } />
}
            </div>
        : <Row>
            <h4 className="empty-imgs">There are no files, upload a new one!</h4>
        </Row>);
}

const UserFilesComponent = ({
    dispatchSelectedFile,
    closeModalLoadImage,
    idActualParam,
    cppIdParentNode,
    fileSelectedHandler,
    onChangeFile,
    deleteServersImage,
    sendFile,
    deleteFile,
    addFileNode,
    userImages,
    fetchUserImages,
    showDetails,
    changeShowDetails,
    title,
    searchFile,
    searchText,
    actualFilter,
    changeFilter,
    filteredUserFiles,
    ascendingOrder,
    descendingOrder,
    file,
    resetChargedFile,
    that
}) => {
    return (
        <div>
            {< div > {
                title !== null
                    ? <h1>{title}</h1>
                    : <div></div>
            } </div>}
            <div>
                <div xs={12}>
                    <div>
                        <div className="search-bar">
                            <input
                                id="search-file"
                                type="text"
                                onChange={e => searchFile(e)}
                                placeholder={`Search file...`}
                                size="50"></input>
                        </div>

                    </div>
                    <div >
                        <Row className="orderBy-filter">
                            <Col>
                                <h3>
                                    Order By:
                                </h3>
                            </Col>
                            <Col>
                                <Row className="button-filters">
                                    {actualFilter === 'name'
                                        ? <Row>
                                                <Col xs>
                                                    <button value="date" onClick={e => changeFilter(e)}>Date</button>
                                                </Col>
                                                <Col xs>
                                                    <button value="type" onClick={e => changeFilter(e)}>Type</button>
                                                </Col>
                                                <Col xs>
                                                    <button value="size" onClick={e => changeFilter(e)}>Size</button>
                                                </Col>
                                            </Row>
                                        : actualFilter === 'date'
                                            ? <Row>
                                                    <Col xs>
                                                        <button value="name" onClick={e => changeFilter(e)}>Name</button>
                                                    </Col>
                                                    <Col xs>
                                                        <button value="type" onClick={e => changeFilter(e)}>Type</button>
                                                    </Col>
                                                    <Col xs>
                                                        <button value="size" onClick={e => changeFilter(e)}>Size</button>
                                                    </Col>
                                                </Row>
                                            : actualFilter === 'type'
                                                ? <Row>
                                                        <Col xs>
                                                            <button value="name" onClick={e => changeFilter(e)}>Name</button>
                                                        </Col>
                                                        <Col xs>
                                                            <button value="date" onClick={e => changeFilter(e)}>Date</button>
                                                        </Col>
                                                        <Col xs>
                                                            <button value="size" onClick={e => changeFilter(e)}>Size</button>
                                                        </Col>
                                                    </Row>
                                                : <Row>
                                                    <Col xs>
                                                        <button value="name" onClick={e => changeFilter(e)}>Name</button>
                                                    </Col>
                                                    <Col xs>
                                                        <button value="date" onClick={e => changeFilter(e)}>Date</button>
                                                    </Col>
                                                    <Col xs>
                                                        <button value="type" onClick={e => changeFilter(e)}>Type</button>
                                                    </Col>
                                                </Row>
}
                                </Row>
                            </Col>
                        </Row>
                    </div>
                    <div className="sortedBy-text">
                        <h3>Sorted by {actualFilter}</h3>
                    </div>
                    {showDetails !== null && cppIdParentNode
                        ? showDetailsGrid(showDetails)
                        : <h4 >Press an image to see more details</h4>
}
                    <Grid fluid className="files">
                        <Row>
                            <Grid fluid className="files-nav">
                                <Col className="FilterButtonGroupSearch">
                                    <button onClick={() => ascendingOrder()}>Asc</button>
                                    <button onClick={() => descendingOrder()}>Desc</button>
                                </Col>
                            </Grid>
                        </Row>
                        <Row className="imgs-container">
                            {differenceWith(filteredUserFiles, userImages, isEqual).length === 0 && searchText === ""
                                ? createFileColumn(userImages, addFileNode, cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, closeModalLoadImage, changeShowDetails, that)
                                : filteredUserFiles.length === 0 && searchText !== ""
                                ? <p>The search has not generated results</p>
                                : createFileColumn(filteredUserFiles, addFileNode, cppIdParentNode, idActualParam, deleteServersImage, deleteFile, fetchUserImages, closeModalLoadImage, changeShowDetails, that)
}

                        </Row>
                        <Row className="footer-modal" center="xs">
                            <Col xs={6} className="input-file">
                                <input type="file" onChange={fileSelectedHandler}/>
                            </Col>
                            <Col xs={6}>
                                <button
                                    className="send-img-button"
                                    onClick=
                                    {_ => onChangeFile(file, sendFile, fetchUserImages, resetChargedFile)}>Send Image</button>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </div>
        </div>
    );
};

UserFilesComponent.propTypes = {
    closeModalLoadImage: PropTypes.func,
    idActualParam: PropTypes.string,
    cppIdParentNode: PropTypes.number,
    onChangeFile: PropTypes.func.isRequired,
    deleteServersImage: PropTypes.func.isRequired,
    sendFile: PropTypes.func.isRequired,
    deleteFile: PropTypes.func.isRequired,
    addFileNode: PropTypes.func,
    userImages: PropTypes.array.isRequired,
    fetchUserImages: PropTypes.func.isRequired,
    showDetails: PropTypes.object,
    changeShowDetails: PropTypes.func.isRequired,
    title: PropTypes.string,
    searchFile: PropTypes.func.isRequired,
    filteredUserFiles: PropTypes.array.isRequired
}

export default UserFilesComponent;