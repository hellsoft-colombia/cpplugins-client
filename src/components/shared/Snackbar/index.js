import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import ContentWrapper from './ContentWrapper';

const CustomizedSnackbar = ({message, type, handleClose, open}) => {
    let variant_type;
    switch (type) {
        case 1:
            variant_type = 'success';
            break;
        case 2:
            variant_type = 'error';
            break;
        case 3:
            variant_type = 'warning';
            break;
        case 4:
            variant_type = 'info';
            break;
        default:
            variant_type = 'info';
    }
    return (
        <div>
            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left'
            }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}>
                <ContentWrapper onClose={handleClose} variant={variant_type} message={message}/>
            </Snackbar>
        </div>
    );
}

CustomizedSnackbar.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.number.isRequired,
    handleClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
}

export default CustomizedSnackbar;
