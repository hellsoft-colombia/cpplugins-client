/**
 * File description: Displays the footer that appears in the application.
 * Author: Hellsoft
 */
import React, { Component } from 'react';

class AppFooter extends Component {
    render() {
        return (
            <footer id="footer" className="wrapper" style={{backgroundColor: "#173c4f"}}>
                <div className="inner">
        
                    <div className="copyright">
                        &copy; cpPlugins 3.0.
                    </div>
                </div>
            </footer>
        );
    }
}

export default AppFooter;