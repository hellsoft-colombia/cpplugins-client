/**
 * File description: The logic that is executing to show the information about results visualization.
 * Author: Hellsoft
 */
import React, { Component } from 'react';
import AppFrame from '../components/shared/AppFrame';
import VisualizerComponent from '../components/VisualizerComponent';

class VisualizerContainer extends Component {

    constructor() {
        super();
        this.state = {
            barValue: "1",
        };
    }

    handleBarChange = (event) => {
        this.setState({ barValue: event.target.value });
    }

    render() {
        return (
            <div>
                <AppFrame
                    header = "Visualizer"
                    body = {
                        <div>
                            <VisualizerComponent
                                barValue = { this.state.barValue }
                                handleBarChange = { this.handleBarChange }
                            ></VisualizerComponent>
                        </div>
                    }
                ></AppFrame>
            </div>
        );
    }
}

export default VisualizerContainer;