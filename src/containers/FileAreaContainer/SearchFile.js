/**
 * File description: This container are used to functionalities present in the file area.
 * Some functions are:
 * -> Search file using the search bar.
 * -> Filter the files.
 * -> Order by different parameters (Date, name, format).
 * Author: Hellsoft
 */
import { filter, orderBy } from 'lodash';

const searchFile = (files, activeFilte, text) => {
    let filteredList = files;
    if(activeFilte === 'type'){
        filteredList = filter(filteredList, file => file.format.toLowerCase().includes(text.toLowerCase()));
    }
    else{
        filteredList = filter(filteredList, file => file.fileName.toLowerCase().includes(text.toLowerCase()));
    }
    return filteredList;
}

const getDateFromTimeStamp = (dateTime) => {
    const [date, time] = dateTime.split(' ');
    const [month, day, year] = date.split('-');
    const [hours, minutes, seconds] = time.split(':');
    return new Date(year, Number(month)-1, day, hours, minutes, seconds);
}

const ascendingDates = (date1, date2) => {
    const firstDate = getDateFromTimeStamp(date1);
    const secondDate = getDateFromTimeStamp(date2);
    if(firstDate < secondDate){
        return -1;
    }
}

const descendingDates = (date1, date2) => {
    const firstDate = getDateFromTimeStamp(date1);
    const secondDate = getDateFromTimeStamp(date2);
    if(firstDate > secondDate){
        return -1;
    }
}

const ascendingOrder = (files, filter) => {
    let orderedList = files;
    if(filter === 'date'){
        orderedList.sort((f1, f2) => ascendingDates(f1.creationDate, f2.creationDate));
    }
    else if(filter === 'type'){
        orderedList = orderBy(orderedList, ['format'], ['asc']);
    }
    else if(filter === 'size'){
        orderedList.sort((a,b) => {
            return Number(a.size)-Number(b.size);
        });        
    }
    else{
        orderedList = orderBy(orderedList, ['fileName'], ['asc']);
    }
    return orderedList;
}

const descendingOrder = (files, filter) => {
    let orderedList = files;
    if(filter === 'date'){
        orderedList.sort((f1, f2) => descendingDates(f1.creationDate, f2.creationDate));
    }
    else if(filter === 'type'){
        orderedList = orderBy(orderedList, ['format'], ['desc']);
    }
    else if(filter === 'size'){
        orderedList.sort((a,b) => {
            return Number(b.size)-Number(a.size);
        });        
    }
    else{
        orderedList = orderBy(orderedList, ['fileName'], ['desc']);
    }
    return orderedList;
}

//The next functions are utils for containers

export const showDetailsImg = (that, img) => {
    that.setState({showDetails: img})
}

export const findFile = (that, text) => {
    let newList = [];
    if(text.target.value !== ''){
        newList = searchFile(that.props.userImages, that.state.actualFilter, text.target.value.toLowerCase());
    }else{
        newList = that.props.userImages;
    }
    that.setState({
        filteredUserFiles: newList
    });
}

export const changeFilter = (that, filter) => {
    that.setState({
        actualFilter: filter.target.value
    });
}

export const sortAscending = that => {
    that.setState({
        filteredUserFiles: ascendingOrder(that.state.filteredUserFiles, that.state.actualFilter)
    });
};

export const sortDescending = that => {
    that.setState({
        filteredUserFiles: descendingOrder(that.state.filteredUserFiles, that.state.actualFilter)
    });
};
