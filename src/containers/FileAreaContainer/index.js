/**
 * File description: Contains the logic used by the files component.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import AppFrame from '../../components/shared/AppFrame';
import AppFooter from '../../components/shared/AppFooter';
import PropTypes from 'prop-types';
import CategoryListComponent from '../../components/CategoryListComponent';
import PipelineCollectionComponent from '../../components/PipelineCollectionComponent';
import FileDetailsComponent from '../../components/FileDetailsComponent';
import {Grid, Col} from 'react-flexbox-grid';
import {connect} from 'react-redux';
import {modCalc} from '../../utils/SearchFunctions';
import {
    fetchUserPipelines,
    fetchSpecificPipeline,
    setActivePipeLine,
    setSelectedFile,
    sortCurrentList,
    sendFile,
    deleteFile,
    deleteSelectedPipeline,
    deleteSelectedCustomNode,
    resetSelectedFile,
    fetchUserImages,
    setIsCustomFilter,
    storePipeline,
    fetchUserCustomPlugins,
    createCustomNode,
    fetchSpecificCustomNode
} from '../../actions';
import {getSelectedFile, getPipelineList, getFileList} from '../../selectors/filesState';
import {getIsCustomFilter} from '../../selectors/customFilterState';
import {withRouter} from 'react-router-dom';
import {ORDER_CRYTERIA} from '../../constants';
import {DESIGN_AREA_ROUTE} from '../../constants/routes';
import UserFilesComponent from '../../components/shared/files/UserFilesComponent';
import {onChangeFile, deleteServersImage} from '../../utils/ModalFunctions';
import {showDetailsImg, findFile, changeFilter, sortAscending, sortDescending} from './SearchFile';
import {compose} from 'recompose';
import {history} from '../../helpers/history';
import validate from '../../utils/ValidateForm';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import LoadingOverlay from 'react-loading-overlay';
import PacmanLoader from 'react-spinners/PacmanLoader';
import {getIsLoadingStatePage} from '../../selectors/loadingState';

import './../style.css'

class FileAreaContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "Pipelines",
            loadFileVisible: false,
            newPipelineVisible: false,
            defaultSearchCryteria: 0,
            defaultSearchOrder: false,
            //pipelineListReference: [],
            selectedFile: null,
            file: null,
            showDetails: null,
            filteredUserFiles: [],
            actualFilter: "name",
            searchText: "",
            error: null,
            customPluginForm: false,
            formControls: {
                newPipelineName: {
                    error: null,
                    value: "",
                    placeholder: 'Pipeline name',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                newPipelineDescription: {
                    error: null,
                    value: "",
                    placeholder: 'Pipeline desciption',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                }
            },
            formIsValid: false,
            isCustomNode: false
        }
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };

        let validation = validate(value, updatedFormElement.validationRules, name);
        updatedFormElement.value = value;
        updatedFormElement.valid = validation.isValid;
        updatedFormElement.error = validation.errors !== ""
            ? validation.errors
            : null;

        updatedControls[name] = updatedFormElement;
        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({formControls: updatedControls, formIsValid: formIsValid});
    }

    validateRules = _ => {
        let listOfInputs = ['newPipelineName', 'newPipelineDescription'];
        const updatedControls = {
            ...this.state.formControls
        };
        listOfInputs.forEach(name => {
            const updatedFormElement = {
                ...updatedControls[name]
            };

            let validation = validate(updatedFormElement.value, updatedFormElement.validationRules, name);
            updatedFormElement.valid = validation.isValid;
            updatedFormElement.error = validation.errors !== ""
                ? validation.errors
                : null;

            updatedControls[name] = updatedFormElement;
        })
        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({formControls: updatedControls, formIsValid: formIsValid});
    }

    handleError = error => (this.setState({error: error}))

    submit = _ => {
        this.validateRules();
        let {newPipelineName, newPipelineDescription} = this.state.formControls;
        this
            .props
            .setIsCustomFilter(this.state.isCustomNode);

        if (this.state.formIsValid) {
            if (!this.state.isCustomNode) {
                this
                    .props
                    .storePipeline({
                        name: newPipelineName.value,
                        description: newPipelineDescription.value,
                        pipelineDesign: {}
                    }, this.handleError);
            } else {
                this
                    .props
                    .createCustomNode({
                        pluginName: newPipelineName.value,
                        subtitle: newPipelineDescription.value,
                        inputs: [],
                        outputs: [],
                        descriptor: {}
                    }, this.handleError);
            }
        } else {
            this.setState({error: "Please, fill the form in the right way"});
        }
    }

    changeCheckedState = _ => {
        this.setState(prevState => ({
            isCustomNode: !prevState.isCustomNode
        }))
    }

    changeShowDetails = (img) => {
        showDetailsImg(this, img);
        this.setState({showDetails: img})
    }

    componentDidMount() {
        this
            .props
            .resetSelectedFile();
        if (this.props.pipelineList.size === undefined) {
            this
                .props
                .fetchUserPipelines();
        }
    }

    openModalLoadFile = () => {
        this.setState({loadFileVisible: true});
    }

    closeModalLoadFile = () => {
        this.setState({loadFileVisible: false});
    }

    openModalNewPipeline = () => {
        this.setState({newPipelineVisible: true});
    }

    closeModalNewPipeline = () => {
        this.setState({newPipelineVisible: false});
    }

    //TODO: this may change
    //TODO: Change this function pass it to a file
    onChangeFile = (e) => {
        let files = e.target.files;
        let reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = (e) => {
            //const url = ""; const formData = { file: e.target.result }
        }
    }

    //TODO: implement
    onFileFormSubmit = () => {}

    changeSearchOrderCryteria = (event) => {

        this
            .props
            .sortCurrentList({
                order: !this.state.defaultSearchOrder,
                cryteria: ORDER_CRYTERIA[this.state.defaultSearchCryteria]
            });

        this.setState((prevState) => ({
            defaultSearchOrder: !prevState.defaultSearchOrder
        }));
    }

    changeSearchCryteria = (event) => {
        let myOrder = modCalc(this.state.defaultSearchCryteria);

        this
            .props
            .sortCurrentList({order: this.state.defaultSearchOrder, cryteria: ORDER_CRYTERIA[myOrder]});

        this.setState({defaultSearchCryteria: myOrder});
    }

    selectFileAction = (selectedFile) => {
        let {fetchSpecificPipeline, setSelectedFile, fetchSpecificCustomNode} = this.props;
        this.state.title !== 'Custom plugins'
            ? fetchSpecificPipeline(selectedFile['fileId']).then(() => {
                setSelectedFile(selectedFile);
                history.push(DESIGN_AREA_ROUTE);
            })
            : fetchSpecificCustomNode(selectedFile['fileId']).then(() => {
                setSelectedFile(selectedFile);
                history.push(DESIGN_AREA_ROUTE);
            });

    }

    deleteFileAction = (selectedFile) => {
        let {deleteSelectedPipeline, resetSelectedFile, fetchUserPipelines, deleteSelectedCustomNode, fetchUserCustomPlugins} = this.props;
        this.state.title !== 'Custom plugins'
            ? deleteSelectedPipeline(selectedFile['fileId']).then(_ => (fetchUserPipelines().then(_ => (resetSelectedFile()))))
            : deleteSelectedCustomNode(selectedFile['fileId']).then(_ => (fetchUserCustomPlugins().then(_ => (resetSelectedFile()))))
    }

    setSelectedFileProperties = (selectedFile) => {
        this
            .props
            .setSelectedFile(selectedFile);
    }

    designSelectedPipeline = _ => {
        this.selectFileAction(this.props.selectedFile);
    }

    filterPipelines = _ => {
        console.log("filter pipelines");
    }

    setAllPipelines = _ => {
        console.log("all pipelines");
    }

    isPipelinesAdministration = _ => {
        if (this.state.title === "Pipelines" || this.state.title === "Custom plugins") {
            return true;
        }
        return false;
    }

    fileSelectedHandler = event => {
        this.setState({file: event.target.files[0]});
    }

    switchToPipelines = _ => {
        this
            .props
            .resetSelectedFile();
        this.setState({customPluginForm: false, showDetails: null})
        this
            .props
            .fetchUserPipelines();
        this.setState({title: "Pipelines"});
    }

    switchToCustomNodes = _ => {
        this
            .props
            .resetSelectedFile();
        this.setState({customPluginForm: true, showDetails: null})
        this
            .props
            .fetchUserCustomPlugins();
        this.setState({title: "Custom plugins"});
    }

    switchToFiles = _ => {
        this
            .props
            .resetSelectedFile();
        this
            .props
            .fetchUserImages();
        this.setState({title: "Files"});
    }

    setPipelineName = name => {
        this.setState({newPipelineName: name});
    }

    searchFile = text => {
        this.setState({searchText: text});
        findFile(this, text);
    }

    resetChargedFile = _ => {
        this.setState({file: null});
    }

    cleanRawFile = _ =>{
        this.setState({showDetails: null});
    }

    render() {
        return (
            <div>
            <LoadingOverlay active={this.props.isLoading} spinner={< PacmanLoader />}>
                <AppFrame
                    header="File Area"
                    body=
                    { <div>
                        <Grid fluid className="file-area-grid">
                        <div className="no-pc-fallback">
                                    <WarningRoundedIcon fontSize="large"></WarningRoundedIcon>
                                    <p>
                                        Por favor abre la aplicación en un computador
                                    </p>
                        </div> 
                            <Col xs={2} className="file-area-category-list" > 
                            <CategoryListComponent
                                isPipelinesAdministration={this.isPipelinesAdministration}
                                switchToPipelines={this.switchToPipelines}
                                switchToCustomNodes={this.switchToCustomNodes}
                                switchToFiles={this.switchToFiles}
                                loadFileVisible={this.state.loadFileVisible}
                                openModalLoadFile={this.openModalLoadFile}
                                closeModalLoadFile={this.closeModalLoadFile}
                                newPipelineVisible={this.state.newPipelineVisible}
                                openModalNewPipeline={this.openModalNewPipeline}
                                closeModalNewPipeline={this.closeModalNewPipeline}
                                onChangeFile={this.onChangeFile}
                                onFileFormSubmit={this.onFileFormSubmit}
                                newPipelineName={this.state.formControls.newPipelineName}
                                newPipelineDescription={this.state.formControls.newPipelineDescription}
                                onChange={this.changeHandler}
                                onSubmit={this.submit}
                                changeCheckedState={this.changeCheckedState}
                                title={this.state.title} /> 
                            </Col> 
                            <Col xs={7} className="file-area-content"> 
                                { this.isPipelinesAdministration() ? (
                                    <PipelineCollectionComponent 
                                        title = { this.state.title } 
                                        selectFileAction = {this.selectFileAction} 
                                        deleteFileAction = {this.deleteFileAction} 
                                        setSelectedFileProperties = {this.setSelectedFileProperties} 
                                        pipelineItems = { this.props.pipelineList } 
                                        changeSearchOrderCryteria = { this.changeSearchOrderCryteria } 
                                        changeSearchCryteria = { this.changeSearchCryteria } 
                                        defaultSearchCryteria = { this.state.defaultSearchCryteria } 
                                        defaultSearchOrder = { this.state.defaultSearchOrder } 
                                        filterPipelines = {this.filterPipelines} 
                                        setAllPipelines = {this.setAllPipelines} 
                                        cleanRawFile = {this.cleanRawFile} />) 
                                    :(
                                    <UserFilesComponent
                                        dispatchSelectedFile = {true}
                                        userImages={this.props.userImages}
                                        deleteServersImage={deleteServersImage}
                                        deleteFile={this.props.deleteFile}
                                        fetchUserImages={this.props.fetchUserImages}
                                        resetChargedFile={this.resetChargedFile}
                                        fileSelectedHandler={this.fileSelectedHandler}
                                        onChangeFile={onChangeFile}
                                        sendFile={this.props.sendFile}
                                        showDetails={this.state.showDetails}
                                        changeShowDetails={this.changeShowDetails}
                                        title={this.state.title}
                                        searchFile={this.searchFile}
                                        searchText={this.state.searchText}
                                        actualFilter={this.state.actualFilter}
                                        changeFilter={filter => changeFilter(this, filter)}
                                        filteredUserFiles={this.state.filteredUserFiles}
                                        ascendingOrder={_ => sortAscending(this)}
                                        descendingOrder={_ => sortDescending(this)}
                                        file={this.state.file}
                                        that={this}/>
                                    )
                                } 
                            </Col> 
                            <Col xs={3} className="file-area-details"> 
                                <FileDetailsComponent 
                                        rawFile={this.state.showDetails}
                                        selectedFile={this.props.selectedFile} 
                                        designSelectedPipeline={this.designSelectedPipeline} /> 
                            </Col> 
                        </Grid> 
                    </div >}/>
                </LoadingOverlay>
                <AppFooter/>
            </div>
        );
    }
}

FileAreaContainer.propTypes = {
    fetchSpecificPipeline: PropTypes.func.isRequired,
    fetchUserPipelines: PropTypes.func.isRequired,
    setActivePipeLine: PropTypes.func.isRequired,
    pipelineList: PropTypes.array.isRequired,
    selectedFile: PropTypes.object.isRequired,
    userImages: PropTypes.array.isRequired,
    deleteFile: PropTypes.func.isRequired,
    fetchUserImages: PropTypes.func.isRequired,
    sendFile: PropTypes.func.isRequired,
    deleteSelectedPipeline: PropTypes.func.isRequired,
    resetSelectedFile: PropTypes.func.isRequired,
    storePipeline: PropTypes.func.isRequired,
    createCustomNode: PropTypes.func.isRequired,
    fetchUserCustomPlugins: PropTypes.func.isRequired,
    fetchSpecificCustomNode: PropTypes.func.isRequired
};

const mapStateToProps = state => ({selectedFile: getSelectedFile(state), pipelineList: getPipelineList(state), userImages: getFileList(state), isCustomFilterValue: getIsCustomFilter(state), isLoading: getIsLoadingStatePage(state)});

const mapDispatchToProps = dispatch => ({
    fetchUserPipelines: _ => dispatch(fetchUserPipelines()),
    fetchSpecificPipeline: params => {
        return new Promise((resolve, reject) => {
            dispatch(fetchSpecificPipeline(params));
            resolve();
        });
    },
    setActivePipeLine: pipeline => dispatch(setActivePipeLine(pipeline)),
    sortCurrentList: data => dispatch(sortCurrentList(data)),
    setSelectedFile: fileId => dispatch(setSelectedFile(fileId)),
    deleteFile: file => dispatch(deleteFile(file)),
    fetchUserImages: _ => dispatch(fetchUserImages()),
    sendFile: (file, callback) => dispatch(sendFile(file, callback)),
    deleteSelectedPipeline: pipelineId => dispatch(deleteSelectedPipeline(pipelineId)),
    deleteSelectedCustomNode: pipelineId => dispatch(deleteSelectedCustomNode(pipelineId)),
    resetSelectedFile: _ => dispatch(resetSelectedFile()),
    storePipeline: (newPipeline, handleError) => dispatch(storePipeline(newPipeline, handleError)),
    createCustomNode: (newCustomNode, handleError) => dispatch(createCustomNode(newCustomNode, handleError)),
    fetchUserCustomPlugins: _ => dispatch(fetchUserCustomPlugins()),
    setIsCustomFilter: data => dispatch(setIsCustomFilter(data)),
    fetchSpecificCustomNode: params => {
        return new Promise((resolve, reject) => {
            dispatch(fetchSpecificCustomNode(params));
            resolve();
        });
    }
});

FileAreaContainer.defaultProps = {
    pipelineList: [],
    selectedFile: {
        creationDate: "",
        executionState: "",
        fileId: "",
        fileName: "",
        lastModificationDate: ""
    }
}

const connectFunc = connect(mapStateToProps, mapDispatchToProps);

export default compose(connectFunc, withRouter,)(FileAreaContainer);