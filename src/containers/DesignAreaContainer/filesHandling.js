/**
 * File description: This file allows to handle the modal when the user is trying to add a file
 * to a node.
 * Author: Hellsoft
 */
import { parametersSelector } from './../../utils/PipelineElements'
import { ADD_FILE_READER } from '../../constants';

export const openModalLoadImage = (infoReader, that) => {
    that.setState({
        loadImageVisible : true,
        actualCppIdNode: infoReader.cppId,
        idActualParam: infoReader.parameterId,
    });
    that.props.fetchUserImages();
}

export const closeModalLoadImage = (cppIdParentNode, fileName, fileId, that) => {
    const reader = that.state.workSpace.getFigures().data.filter(f => f.cppId === cppIdParentNode)[0];
    if (reader !== undefined){
        console.log(
                parametersSelector(reader));
        that.eventTriggered(
            {
                nodeId: cppIdParentNode, 
                parameterId: that.state.idActualParam,
                value: {
                    fileId: fileId,
                    fileName: parametersSelector(reader).getInputPort(that.state.idActualParam).inputValue.text,
                }
            },
            ADD_FILE_READER
        );
        parametersSelector(reader).getInputPort(that.state.idActualParam).createParameterInput(fileName);
        parametersSelector(reader).getInputPort(that.state.idActualParam).deleteConnections();
    }
    that.setState({
        loadImageVisible : false
    });
}