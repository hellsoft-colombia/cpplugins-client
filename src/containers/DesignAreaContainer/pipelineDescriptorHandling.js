/**
 * File description: Contains functions to generate a descriptor that will be sent to the 
 * server for its processing.
 * Author: Hellsoft
 */
import { createNode as _createNode } from '../../utils/PipelineElements';
import { 
    CREATE_NODE, 
    DELETE_NODE, 
    MOVEMENT_NODE, 
    CREATE_NODE_CONNECTION, 
    DELETE_NODE_CONNECTION,
    CREATE_PARAMETER_CONNECTION,
    DELETE_PARAMETER_CONNECTION,
} from '../../constants';

export const dispatchNodeCreation = (node, that) => {
    let newNode = _createNode({
        node,
        dispatchNodeInfo: that.dispatchNodeInfo,
        handleSendPartialPipeline: that.handleSendPartialPipeline,
        dispatchNodeChangePosition: that.dispatchNodeChangePosition,
        dispatchParameterEdition: that.dispatchParameterEdition,
        dispatchParameterValueDelete: that.dispatchParameterValueDelete,
        openModalLoadImage: that.openModalLoadImage,
        deleteFileImage: that.deleteFileParam,
        isCustomFilter: that.props.isCustomFilter,
        addInputCustomFilter: that.props.addInputCustomFilter,
        addOutputCustomFilter: that.props.addOutputCustomFilter,
        deleteInputCustomFilter: that.props.deleteInputCustomFilter,
        deleteOutputCustomFilter: that.props.deleteOutputCustomFilter,
        eventTriggered: that.eventTriggered,
    });
    that.state.workSpace.add(newNode);

    createNodeEvent(newNode, that);
    return newNode.id;
}

const createNodeEvent = (node, that) => {
    let { id, cppId, cppType, cppClass, x, y, parameters, inputs, outputs, title,pluginId } = node;
    
    that.props.addDescriptorNode({
        descriptor: true,
        title,
        nodeId: cppId,
        d2dId: id,
        nodeClass: cppClass,
        type: cppType,
        x, y,
        parameters,
        inputs,
        outputs,
        pluginId
    });
    let newNode = {
        id , cppId, type: cppType, nodeClass: cppClass, x, y, parameters, inputs, outputs, title
    };
    that.eventTriggered(newNode, CREATE_NODE);
}

export const dispatchNodeDelete = (node, that) => {
    that.props.deleteDescriptorNode({
        nodeId: node.cppId,
    });
    let deletedNode = {
        id: node.id,
        cppId: node.cppId,
        type: node.cppType,
        nodeClass: node.cppClass,
        x: node.x,
        y: node.y,
        inputs: node.inputs,
        outputs: node.outputs,
        parameters: node.parameters,
        title: node.title
    };
    that.eventTriggered(deletedNode, DELETE_NODE);
}

export const dispatchConnectionCreation = (connection, that) => {
    that.props.addDescriptorConnection({
        connectionId: connection.cppId, 
        from: {
            nodeId: connection.sourcePort.getNode().cppId,
            portId: connection.sourcePort.cppId,
            name: connection.sourcePort.name
        },
        to: {
            nodeId: connection.targetPort.getNode().cppId,
            portId: connection.targetPort.cppId,
            name: connection.targetPort.name
        }
    });
    let newConnection = {
        id: connection.getId()
    };
    that.eventTriggered(newConnection, CREATE_NODE_CONNECTION);
}

export const dispatchConnectionDelete = (connection, that) => {
    that.props.deleteDescriptorConnection({
        connectionId: connection.cppId, 
    });
    const deletedConnection = {
        cppId : connection.cppId,
        sourceNode: connection.sourcePort.getNode().getId(),
        sourcePort: connection.sourcePort.cppId,
        targetNode: connection.targetPort.getNode().getId(),
        targetPort: connection.targetPort.cppId,
        router: connection.getRouter(),
        id: connection.getId()
    }
    that.eventTriggered(deletedConnection, DELETE_NODE_CONNECTION);
}

export const dispatchParameterConnectionCreation = (connection, that) => {
    if(connection.targetPort.isParameterInputPort){
        that.props.addDescriptorParameterConnection({
            nodeId: connection.targetPort.getNode().cppId,
            parameterId: connection.targetPort.name,
            value: {
                nodeId: connection.sourcePort.getNode().cppId,
                parameterId: connection.sourcePort.name
            }
        });
    }
    else{
        that.props.addDescriptorParameterConnection({
            nodeId: connection.sourcePort.getNode().cppId,
            parameterId: connection.sourcePort.name,
            value: {
                nodeId: connection.targetPort.getNode().cppId,
                parameterId: connection.targetPort.name
            }
        });
    }
    let parameterConnection = {
        id: connection.getId()
    };
    that.eventTriggered(parameterConnection, CREATE_PARAMETER_CONNECTION);
}

export const dispatchParameterConnectionDelete = (connection, that) => {
    if(connection.targetPort.isParameterInputPort){
        that.props.deleteDescriptorParameterConnection({
            nodeId: connection.targetPort.getNode().cppId,
            parameterId: connection.targetPort.name,
        });
    }
    else{
        that.props.deleteDescriptorParameterConnection({
            nodeId: connection.sourcePort.getNode().cppId,
            parameterId: connection.sourcePort.name,
        });
    }
    const deletedParameterConnection = {
        cppId : connection.cppId,
        sourceNode: connection.sourcePort.getNode().getId(),
        sourcePort: connection.sourcePort,
        targetNode: connection.targetPort.getNode().getId(),
        targetPort: connection.targetPort,
        router: connection.getRouter(),
        id: connection.getId()
    }
    that.eventTriggered(deletedParameterConnection, DELETE_PARAMETER_CONNECTION);
}

export const dispatchParameterValueDelete = (parameterInfo, that) => {
    that.props.deleteDescriptorParameterConnection({
        nodeId: parameterInfo.nodeId,
        parameterId: parameterInfo.parameterId,
    });
}

export const dispatchParameterEdition = (parameter, that) =>{
    that.props.addDescriptorParameterValue(
        parameter
    );
}

export const dispatchNodeChangePosition = ( node, that ) => {
    let activeNode = that.props.activePipeline.nodes.find(n => n.d2dId === node.id);
    let nodePosition = {
        id: node.id,
        x: activeNode.x,
        y: activeNode.y
    };
    that.eventTriggered(nodePosition, MOVEMENT_NODE);
    that.props.changeNodePosition({
        nodeId: node.cppId,
        x: node.x,
        y: node.y
    });
}

export const dispatchNodeInfo = (node, that) => {
    that.props.setNode({
        nodeId: node.cppId
    });
}

export const dispatchNodeFile = (node,that) => {
    that.props.addFileNode(node);
}