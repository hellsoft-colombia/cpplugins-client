/**
 * File description: It has the actions to handle the creation and edition of nodes.
 * Author: Hellsoft
 */
export const createNodeFunction = (that) => {
    console.log("hello world from node creation");
}

export const editNodeFunction = (pipelineId, title, that) => {
    let { setPrevPipeline, activePipeline, fetchSpecificCustomNode } = that.props;
    that.setState({
        isEditNode: true,
        pipelineTitle: title
    });
    setPrevPipeline(activePipeline);
    that.handleNewPipeline();
    fetchSpecificCustomNode(pipelineId);
}