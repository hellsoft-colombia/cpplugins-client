/**
 * File description: This file handles the events that are executed by the command stack
 * where are saved the actions that the user is doing during the design of pipelines.
 * Author: Hellsoft
 */
import { addToCommandStack, doInverseOperation } from '../../utils/CommandStack';

export const eventTriggered = (item, command, that) => {
    if (that.state.triggerByUndoButton){
        addToCommandStack(item, command, that.props.redoCommandStack, that.props, 
            that.dispatchNodeCreation, that.incrementId);
        that.setState((prevState,props) => ({
            triggerByUndoButton: !prevState.triggerByUndoButton
        }));
    }else if (that.state.triggerByRedoButton){
        addToCommandStack(item, command, that.props.undoCommandStack, that.props);
        that.setState((prevState,props) => ({
            triggerByRedoButton: !prevState.triggerByRedoButton
        }));
    }else{
        addToCommandStack(item, command, that.props.undoCommandStack, that.props);
    }        
}

export const undoAction = (that) => {
    that.setState((prevState, props) => ({
        triggerByUndoButton: !prevState.triggerByUndoButton
    }), () => {
        if (Array.isArray(that.props.undoCommandStack) && that.props.undoCommandStack.length){
            const element = that.props.undoCommandStack.pop();
            doInverseOperation(element, that);
        }
    });
}

export const redoAction = (that) => {
    that.setState((prevState, props) => ({
        triggerByRedoButton: !prevState.triggerByRedoButton
    }), () => {
        if (Array.isArray(that.props.redoCommandStack) && that.props.redoCommandStack.length){
            const element = that.props.redoCommandStack.pop();
            doInverseOperation(element, that);
        }
    });
}