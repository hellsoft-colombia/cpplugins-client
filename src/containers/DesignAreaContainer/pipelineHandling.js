/**
 * File description: Handle actions of interaction with the pipelines.
 * Author: Hellsoft
 */
export const handleCancelCustomNode = (that) => {
    let {prevPipeline, setActivePipeLine, cleanPrevPipeline} = that.props;
    if (prevPipeline) {
        that.setState({isEditNode: false, pipelineTitle: "Design pipeline"});
        setActivePipeLine(prevPipeline);
        cleanPrevPipeline();
        that
            .props
            .clearStack();
        that.handleNewPipeline();
    }
}

export const handleSavePipeline = (that) => {
    let {
        prevPipeline,
        setActivePipeLine,
        cleanPrevPipeline,
        editPipelineRequest,
        editCustomPluginRequest,
        isCustomFilter
    } = that.props;

    if (prevPipeline != null) {
        that.setState({isEditNode: false, pipelineTitle: "Design pipeline"});
        that.savePipeline(editPipelineRequest);
        that.handleNewPipeline();
        setActivePipeLine(prevPipeline);
        cleanPrevPipeline();
    } else {
        if (isCustomFilter) {
            that.saveCustomPlugin(editCustomPluginRequest);
        } else {
            that.savePipeline(editPipelineRequest);
        }
    }

}

export const savePipeline = (editPipelineRequest, that) => {
    that.changeSuccessMessage("Your active pipeline have been saved !");
    that
        .props
        .editPipeline(editPipelineRequest);
    that.handleOpenSnackBar();
}

export const handleStopPipeline = (that) => {
    that.changeSuccessMessage("Your pipeline was stopped !");
    that
        .props
        .stopExecutingPipeline(that.props.activePipeline.pipelineId);
    that.handleOpenSnackBar();
}

export const saveCustomPlugin = (editCustomPluginRequest, that) => {
    that.changeSuccessMessage("Your custom plugin have been saved !");
    that
        .props
        .editCustomNode(editCustomPluginRequest);
    let infoInputsOutputs = {
        inputs: editCustomPluginRequest.inputs,
        outputs: editCustomPluginRequest.outputs
    };
    that.props.saveInputsOutputsCustomFilter(infoInputsOutputs);
    that.handleOpenSnackBar();
}