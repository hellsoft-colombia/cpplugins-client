/**
 * File description: This container handle the design area where the user can:
 * -> Create and delete a node.
 * -> Create connections.
 * -> Node movement.
 * -> Delete nodes.
 * -> Undo and redo actions.
 * -> Add files to a node.
 * -> Clear the canvas. 
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import DesignAreaComponent from '../../components/DesignAreaComponent';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import './../style.css';
import './../styles/jquery.layout.css';
import {
    setNode,
    sendPipeline,
    setexecutionNodeId,
    changeNodePosition,
    addDescriptorNode,
    deleteDescriptorNode,
    addDescriptorConnection,
    deleteDescriptorConnection,
    addDescriptorParameterValue,
    addDescriptorParameterConnection,
    deleteDescriptorParameterConnection,
    addUndoCommandStack,
    addRedoCommandStack,
    clearStack,
    cleanLocalStoragePipeline,
    setPrevPipeline,
    cleanPrevPipeline,
    setActivePipeLine,
    fetchSpecificPipeline,
    storePipeline,
    editPipeline,
    addFileNode,
    sendFile,
    deleteFile,
    fetchUserImages,
    editCustomNode,
    fetchSpecificCustomNode,
    addInputCustomFilter,
    addOutputCustomFilter,
    deleteInputCustomFilter,
    deleteOutputCustomFilter,
    addPipelineTimeStamp,
    clearActivePipeline,
    setActivePipelineInfo,
    stopExecutingPipeline,
    saveInputsOutputsCustomFilter
} from '../../actions';
import {
    getActivePipeline,
    getStoredPipeline,
    getPrevPipeline,
    getNodesPipeline,
    getReadersFromPipeline,
    getEditCustomPluginRequest,
    getEditPipelineRequest,
    getParamsNode,
} from '../../selectors/pipelinesState';
import {getIsCustomFilter} from '../../selectors/customFilterState';
import {getUndoCommandStack} from '../../selectors/undoCommandStackState';
import {getRedoCommandStack} from '../../selectors/redoCommandStackState';
import {getFileList, getSelectedFile} from '../../selectors/filesState';
import {Canvas} from '../../utils/PipelineElements';
import {createPipeline} from '../../utils/PipelineConstructor';
import {onChangeFile, deleteServersImage} from '../../utils/ModalFunctions';
import {
    dispatchNodeCreation as _dispatchNodeCreation,
    dispatchNodeDelete as _dispatchNodeDelete,
    dispatchConnectionCreation as _dispatchConnectionCreation,
    dispatchConnectionDelete as _dispatchConnectionDelete,
    dispatchParameterEdition as _dispatchParameterEdition,
    dispatchParameterConnectionCreation as _dispatchParameterConnectionCreation,
    dispatchParameterConnectionDelete as _dispatchParameterConnectionDelete,
    dispatchParameterValueDelete as _dispatchParameterValueDelete,
    dispatchNodeChangePosition as _dispatchNodeChangePosition,
    dispatchNodeInfo as _dispatchNodeInfo,
    dispatchNodeFile as _dispatchNodeFile,
} from './pipelineDescriptorHandling';
import {handleSendPartialPipeline as _handleSendPartialPipeline, areFilesUploaded as _areFilesUploaded, handleSendPipeline as _handleSendPipeline} from './sendPipelineHandling';
import {eventTriggered as _eventTriggered, undoAction as _undoAction, redoAction as _redoAction} from './commandStackHandling';
import {handleCancelCustomNode as _handleCancelCustomNode, handleSavePipeline as _handleSavePipeline, savePipeline as _savePipeline, saveCustomPlugin as _saveCustomPlugin, handleStopPipeline as _handleStopPipeline} from './pipelineHandling';
import {openModalLoadImage as _openModalLoadImage, closeModalLoadImage as _closeModalLoadImage} from './filesHandling';
import {showDetailsImg, findFile, changeFilter, sortAscending, sortDescending} from '../../containers/FileAreaContainer/SearchFile';
import CustomizedSnackbar from '../../components/shared/Snackbar';

class DesignAreaContainer extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            workSpace: null,
            triggerByUndoButton: false,
            triggerByRedoButton: false,
            isEditNode: false,
            loadImageVisible: false,
            actualCppIdNode: null,
            idActualParam: null,
            missingNodesForParams: [],
            pipelineTitle: "Design pipeline",
            showDetails: null,
            filteredUserFiles: [],
            actualFilter: "name",
            searchText: "",
            file: null,
            openSuccess: false,
            successMessage: "",
        }
    }

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({openSuccess: false});
    };

    changeSuccessMessage = message => {
        this.setState({
            successMessage: message
        });
    }

    handleOpenSnackBar = _ => {
        this.setState({openSuccess: true});
    }

    componentDidMount() {
        this.renderCanvas();
        if (this.props.userImages.size === undefined) {
            this
                .props
                .fetchUserImages()
                .then(_ => {
                    this.setState({filteredUserFiles: this.props.userImages});
                });
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.storedPipeline.nodes !== this.props.storedPipeline.nodes && 
            prevProps.storedPipeline.connections !== this.props.storedPipeline.connections && 
            prevProps.storedPipeline.parameters !== this.props.storedPipeline.parameters ) {
            this
                .props
                .clearStack();
            this.handleNewPipeline();
            createPipeline({
                pipelineTitle: this.state.pipelineTitle, 
                selectedFile: this.props.selectedFile, 
                setActivePipelineInfo: this.props.setActivePipelineInfo, 
                storedPipeline: this.props.storedPipeline, 
                canvas: this.state.workSpace, 
                dispatchNodeCreation: this.dispatchNodeCreation, 
                dispatchConnectionCreation: this.dispatchConnectionCreation, 
                dispatchParameterConnectionCreation: this.dispatchParameterConnectionCreation, 
                dispatchParameterEdition: this.dispatchParameterEdition, 
                addFileNode: this.props.addFileNode, 
                eventTriggered: this.eventTriggered, });
        }
    }

    handleNewPipeline = () => {
        this
            .state
            .workSpace
            .clear();
        this
            .props
            .clearStack();
    }

    updatePipeLineTimeStamp = _ => {
        let currentTime = new Date();
        this
            .props
            .addPipelineTimeStamp(currentTime.getTime());
    }

    dispatchNodeCreation = (node) => {
        return _dispatchNodeCreation(node, this);
    }

    dispatchNodeDelete = (node) => {
        _dispatchNodeDelete(node, this);
    }

    dispatchConnectionCreation = (connection) => {
        _dispatchConnectionCreation(connection, this);
    }

    dispatchConnectionDelete = (connection) => {
        _dispatchConnectionDelete(connection, this);
    }

    dispatchParameterEdition = (parameter) => {
        _dispatchParameterEdition(parameter, this);
    }

    dispatchParameterValueDelete = (parameterInfo) => {
        _dispatchParameterValueDelete(parameterInfo, this);
    }

    dispatchParameterConnectionCreation = (connection) => {
        _dispatchParameterConnectionCreation(connection, this);
    }

    dispatchParameterConnectionDelete = (connection) => {
        _dispatchParameterConnectionDelete(connection, this);
    }

    dispatchNodeChangePosition = (node) => {
        _dispatchNodeChangePosition(node, this);
    }

    dispatchNodeInfo = (node) => {
        _dispatchNodeInfo(node, this);
    }

    dispatchNodeFile = (fileInfo) => {
        _dispatchNodeFile(fileInfo, this);
    };

    areFilesUploaded = () => {
        _areFilesUploaded(this);
    }

    handleSendPipeline = () => {
        _handleSendPipeline(this);
    }

    handleStopPipeline = () => {
        _handleStopPipeline(this);
    }

    handleSendPartialPipeline = (cppId) => {
        _handleSendPartialPipeline(cppId, this);
    }

    eventTriggered = (item, command) => {
        _eventTriggered(item, command, this);
    }

    undoAction = () => {
        _undoAction(this);
    }

    redoAction = () => {
        _redoAction(this);
    }

    handleCancelCustomNode = () => {
        _handleCancelCustomNode(this);
    }

    handleSavePipeline = () => {
        this.updatePipeLineTimeStamp();
        _handleSavePipeline(this);
    }

    fileSelectedHandler = event => {
        this.setState({file: event.target.files[0]});
    }

    resetChargedFile = _ => {
        this.setState({file: null});
    }

    savePipeline = (editPipelineRequest) => {
        _savePipeline(editPipelineRequest, this);
    }

    saveCustomPlugin = (editCustomPluginRequest) => {
        _saveCustomPlugin(editCustomPluginRequest, this);
    }

    openModalLoadImage = (infoReader) => {
        _openModalLoadImage(infoReader, this);
    }

    closeModalLoadImage = (cppIdParentNode, fileName, fileId) => {
        _closeModalLoadImage(cppIdParentNode, fileName, fileId, this);
    }

    setSelectedFileProperties = (selectedFile) => {
        this
            .props
            .setSelectedFile(selectedFile);
    }

    changeShowDetails = (img) => {
        showDetailsImg(this, img, true);
        this.setState({showDetails: img})
    }

    searchFile = text => {
        this.setState({searchText: text});
        findFile(this, text);
    }

    renderCanvas() {
        let myCanvas = new Canvas({
            canvasId: 'canvas',
            incrementIdFunc: this.incrementId,
            onDelNodeFunc: this.dispatchNodeDelete,
            onDelConnectionFunc: this.dispatchConnectionDelete,
            onDelParameterConnectionFunc: this.dispatchParameterConnectionDelete,
            onDelParameterValue: this.dispatchParameterValueDelete,
            onCreateConnectionFunc: this.dispatchConnectionCreation,
            onCreateParameterConnectionFunc: this.dispatchParameterConnectionCreation,
            addCommandStack: this.eventTriggered
        });
        this.setState({workSpace: myCanvas});
        return myCanvas;
    }

    render() {
        return (
            <div>
                <DesignAreaComponent
                    title={this.state.pipelineTitle}
                    handleSendPipeline={this.handleSendPipeline}
                    handleNewPipeline={this.handleNewPipeline}
                    handleSavePipeline={this.handleSavePipeline}
                    handleCancelCustomNode={this.handleCancelCustomNode}
                    isEditNode={this.state.isEditNode}
                    undoAction={this.undoAction}
                    redoAction={this.redoAction}
                    undoActionEnable={(Array.isArray(this.props.undoCommandStack) && this.props.undoCommandStack.length)
                    ? true
                    : false}
                    redoActionEnable={(Array.isArray(this.props.redoCommandStack) && this.props.redoCommandStack.length)
                    ? true
                    : false}
                    isCustomFilter={this.props.isCustomFilter}
                    loadImageVisible={this.state.loadImageVisible}
                    closeModalLoadImage={this.closeModalLoadImage}
                    cppIdParentNode={this.state.actualCppIdNode}
                    idActualParam={this.state.idActualParam}
                    fileSelectedHandler={this.fileSelectedHandler}
                    file={this.state.file}
                    resetChargedFile={this.resetChargedFile}
                    onChangeFile={onChangeFile}
                    deleteServersImage={deleteServersImage}
                    sendFile={this.props.sendFile}
                    deleteFile={this.props.deleteFile}
                    addFileNode={this.props.addFileNode}
                    deleteFileNode={this.props.deleteFileNode}
                    fetchUserImages={this.props.fetchUserImages}
                    userImages={this.props.userImages}
                    searchText={this.state.searchText}
                    showDetails={this.state.showDetails}
                    changeShowDetails={this.changeShowDetails}
                    findFile={this.searchFile}
                    actualFilter={this.state.actualFilter}
                    changeFilter=
                    {filter => changeFilter(this, filter) }
                    filteredUserFiles={this.state.filteredUserFiles}
                    sortAscending=
                    {_ => sortAscending(this)}
                    sortDescending=
                    {_ => sortDescending(this)}
                    that={this}
                    handleStopPipeline={this.handleStopPipeline}/>
                    <CustomizedSnackbar message={this.state.successMessage} type={1} handleClose={this.handleCloseSnackBar} open={this.state.openSuccess}/>
            </div>
        );
    }
}

DesignAreaContainer.propTypes = {
    setNode: PropTypes.func.isRequired,
    sendPipeline: PropTypes.func.isRequired,
    setexecutionNodeId: PropTypes.func.isRequired,
    storedPipeline: PropTypes.object,
    changeNodePosition: PropTypes.func.isRequired,
    addDescriptorNode: PropTypes.func.isRequired,
    deleteDescriptorNode: PropTypes.func.isRequired,
    addDescriptorConnection: PropTypes.func.isRequired,
    deleteDescriptorConnection: PropTypes.func.isRequired,
    addDescriptorParameterValue: PropTypes.func.isRequired,
    addDescriptorParameterConnection: PropTypes.func.isRequired,
    deleteDescriptorParameterConnection: PropTypes.func.isRequired,
    activePipeline: PropTypes.object.isRequired,
    undoCommandStack: PropTypes.array.isRequired,
    redoCommandStack: PropTypes.array.isRequired,
    addUndoCommandStack: PropTypes.func.isRequired,
    addRedoCommandStack: PropTypes.func.isRequired,
    cleanLocalStoragePipeline: PropTypes.func.isRequired,
    setPrevPipeline: PropTypes.func.isRequired,
    setActivePipeLine: PropTypes.func.isRequired,
    fetchSpecificPipeline: PropTypes.func.isRequired,
    prevPipeline: PropTypes.object,
    cleanPrevPipeline: PropTypes.func.isRequired,
    storePipeline: PropTypes.func.isRequired,
    editPipeline: PropTypes.func.isRequired,
    addFileNode: PropTypes.func.isRequired,
    sendFile: PropTypes.func.isRequired,
    deleteFile: PropTypes.func.isRequired,
    fetchUserImages: PropTypes.func.isRequired,
    userImages: PropTypes.array.isRequired,
    paramNode: PropTypes.object,
    readersFromPipeline: PropTypes.array.isRequired,
    parametersFromNode: PropTypes.array.isRequired,
    nodesActivePipeline: PropTypes.array.isRequired,
    editPipelineRequest: PropTypes.object.isRequired,
    fetchSpecificCustomNode: PropTypes.func.isRequired,
    editCustomNode: PropTypes.func.isRequired,
    isCustomFilter: PropTypes.bool.isRequired,
    addInputCustomFilter: PropTypes.func.isRequired,
    addOutputCustomFilter: PropTypes.func.isRequired,
    deleteInputCustomFilter: PropTypes.func.isRequired,
    deleteOutputCustomFilter: PropTypes.func.isRequired,
    editCustomPluginRequest: PropTypes.object.isRequired,
    clearActivePipeline: PropTypes.func.isRequired,
    setActivePipelineInfo: PropTypes.func.isRequired,
    saveInputsOutputsCustomFilter: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const undo = getUndoCommandStack(state)
    undo.isUndo = true;
    return {
        activePipeline: getActivePipeline(state),
        redoCommandStack: getRedoCommandStack(state),
        storedPipeline: getStoredPipeline(state),
        prevPipeline: getPrevPipeline(state),
        userImages: getFileList(state),
        nodesActivePipeline: getNodesPipeline(state),
        readersFromPipeline: getReadersFromPipeline(state),
        parametersFromNode: getParamsNode(state),
        editPipelineRequest: getEditPipelineRequest(state),
        editCustomPluginRequest: getEditCustomPluginRequest(state),
        undoCommandStack: undo,
        isCustomFilter: getIsCustomFilter(state),
        selectedFile: getSelectedFile(state),
    }
};

const mapDispatchToProps = dispatch => ({
    setNode: node => dispatch(setNode(node)),
    sendPipeline: activePipeline => dispatch(sendPipeline(activePipeline)),
    setexecutionNodeId: nodeId => dispatch(setexecutionNodeId(nodeId)),
    changeNodePosition: node => dispatch(changeNodePosition(node)),
    addDescriptorNode: node => dispatch(addDescriptorNode(node)),
    deleteDescriptorNode: node => dispatch(deleteDescriptorNode(node)),
    addDescriptorConnection: connection => dispatch(addDescriptorConnection(connection)),
    deleteDescriptorConnection: connection => dispatch(deleteDescriptorConnection(connection)),
    addDescriptorParameterValue: parameter => dispatch(addDescriptorParameterValue(parameter)),
    addDescriptorParameterConnection: parameter => dispatch(addDescriptorParameterConnection(parameter)),
    deleteDescriptorParameterConnection: parameter => dispatch(deleteDescriptorParameterConnection(parameter)),
    addUndoCommandStack: data => dispatch(addUndoCommandStack(data)),
    addRedoCommandStack: data => dispatch(addRedoCommandStack(data)),
    clearStack: _ => dispatch(clearStack()),
    cleanLocalStoragePipeline: _ => dispatch(cleanLocalStoragePipeline()),
    setPrevPipeline: prevPipeline => dispatch(setPrevPipeline(prevPipeline)),
    setActivePipeLine: prevPipeline => dispatch(setActivePipeLine(prevPipeline)),
    fetchSpecificPipeline: params => dispatch(fetchSpecificPipeline(params)),
    cleanPrevPipeline: _ => dispatch(cleanPrevPipeline()),
    storePipeline: activePipeline => dispatch(storePipeline(activePipeline)),
    editPipeline: editPipelineRequest => dispatch(editPipeline(editPipelineRequest)),
    editCustomNode: editCustomPluginRequest => dispatch(editCustomNode(editCustomPluginRequest)),
    addFileNode: file => (dispatch(addFileNode(file))),
    fetchUserImages: _ => dispatch(fetchUserImages()),
    sendFile: (file, callback) => dispatch(sendFile(file, callback)),
    deleteFile: file => dispatch(deleteFile(file)),
    fetchSpecificCustomNode: pipelineId => dispatch(fetchSpecificCustomNode(pipelineId)),
    addInputCustomFilter: input => dispatch(addInputCustomFilter(input)),
    addOutputCustomFilter: output => dispatch(addOutputCustomFilter(output)),
    deleteInputCustomFilter: inputName => dispatch(deleteInputCustomFilter(inputName)),
    deleteOutputCustomFilter: outputName => dispatch(deleteOutputCustomFilter(outputName)),
    addPipelineTimeStamp: timestamp => dispatch(addPipelineTimeStamp(timestamp)),
    clearActivePipeline: _ => dispatch(clearActivePipeline()),
    stopExecutingPipeline: pipelineId => dispatch(stopExecutingPipeline(pipelineId)),
    setActivePipelineInfo: info => dispatch(setActivePipelineInfo(info)),
    saveInputsOutputsCustomFilter: data => dispatch(saveInputsOutputsCustomFilter(data)),
});

export default
connect(mapStateToProps, mapDispatchToProps, null, {forwardRef: true})(DesignAreaContainer);