/**
 * File description: Handle actions to sent the information to the server.
 * Author: Hellsoft
 */
import {parametersSelector} from '../../utils/PipelineElements';

export const handleSendPartialPipeline = (cppId, that) => {
    that
        .props
        .setexecutionNodeId(cppId);
    that.handleSendPipeline();
}

export const areFilesUploaded = (that) => {
    const readers = that.props.readersFromPipeline;
    const params = that.props.parametersFromNode;
    let isValid = true;
    readers.forEach(r => {
        let nodeParams = params.filter(p => p.nodeId === r.nodeId);
        if (nodeParams.length <= 0) {
            that
            .state
            .missingNodesForParams
            .push(r.d2dId);
            isValid = false;
        }
    });
    return isValid;
}

export const handleSendPipeline = (that) => {
    if (areFilesUploaded(that)) {
        let {sendPipeline, activePipeline} = that.props;
        return sendPipeline({pipelineDesign: activePipeline});
    } else {
        alert("Please upload a file before executing !");
        that
            .state
            .missingNodesForParams
            .map(node => {
                let parameter = parametersSelector(that.state.workSpace.getFigure(node))
                    .getInputPorts()
                    .data[1]
                    .inputValue;
                parameter.setColor('#FF0000');
                parameter.setStroke(2);
                return true;
            });
        setTimeout(() => that.state.missingNodesForParams.map(node => {
            let parameter = parametersSelector(that.state.workSpace.getFigure(node))
                .getInputPorts()
                .data[1]
                .inputValue;
            parameter.setColor('#000000');
            parameter.setStroke(1);
            return true;
        }));
    }
}
