/**
 * File description: The logic that is executing to show the information about execution history.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import AppFrame from '../components/shared/AppFrame';
import AppFooter from '../components/shared/AppFooter';
import PropTypes from 'prop-types';
import {compose} from 'recompose';
import {withRouter} from 'react-router-dom';
import {Grid, Col} from 'react-flexbox-grid';
import {
    getExecutionStateHistory,
    fetchActiveEntries,
    fetchFinishedPipelines,
    getExecutionStateHistoryFinished,
    checkExecutingPipeline,
    stopExecutingPipeline,
    cancelExecutingPipeline,
    resumeExecutingPipeline,
    changeToAllEntries,
    changeToActiveEntries,
    changeToFinishedEntries,
    changeToFinishedPipelines
} from '../actions';
import ExecutionStateCategoryComponent from '../components/ExecutionStateCategoryComponent';
import ExecutionHistoryCollectionComponent from '../components/ExecutionHistoryCollectionComponent';
import {getHistoryList, getHasMoreFlag, getCurrentPage, getIsLoading} from '../selectors/executionHistoryState';
import {connect} from 'react-redux';
import './style.css'

class ExecutionHistoryContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeOption: 1
        }
    }

    componentDidMount() {
        let {getExecutionStateHistory, currentPage} = this.props;
        getExecutionStateHistory(currentPage);
    }

    changeActiveOption = activeOption => this.setState({activeOption: activeOption})

    swtichToAllEntries = _ => (this.props.changeToAllEntries(this.changeActiveOption, 1))

    switchToActivePipelines = _ => (this.props.changeToActiveEntries(this.changeActiveOption, 2))

    switchToFinishedPipelines = _ => (this.props.changeToFinishedEntries(this.changeActiveOption, 3))

    switchToFinishedPipelinesEntries = _ => (this.props.changeToFinishedPipelines(this.changeActiveOption, 4))

    renderBody = ({
        historyList,
        hasMore,
        resumeExecutingPipeline,
        stopExecutingPipeline,
        fetchActiveEntries,
        getExecutionStateHistoryFinished,
        getExecutionStateHistory,
        cancelExecutingPipeline,
        checkExecutingPipeline,
        currentPage,
        isLoading
    }) => (
        <div>
            <Grid fluid className="file-area-grid">
                <Col xs={2} className="file-area-category-list">
                    <ExecutionStateCategoryComponent
                        swtichToAllEntries={this.swtichToAllEntries}
                        switchToActivePipelines={this.switchToActivePipelines}
                        switchToFinishedPipelines={this.switchToFinishedPipelines}
                        switchToFinishedPipelinesEntries={this.switchToFinishedPipelinesEntries}/>
                </Col>
                <Col xs={10} className="file-area-content" >
                    <ExecutionHistoryCollectionComponent
                        pageNumber={currentPage}
                        executionHistory={historyList}
                        fetchUserExecutionHistory={this.state.activeOption === 1
                        ? getExecutionStateHistory
                        : this.state.activeOption === 2
                            ? fetchActiveEntries
                            : this.state.activeOption === 3
                                ? getExecutionStateHistoryFinished
                                : fetchFinishedPipelines}
                        hasMore={hasMore}
                        resumePipelineExecution={resumeExecutingPipeline}
                        stopPipelineExecution={stopExecutingPipeline}
                        cancelPipelineExecution={cancelExecutingPipeline}
                        markEntryAsReaded={checkExecutingPipeline}
                        isLoading={isLoading}
                        isHistory={this.state.activeOption === 1 || this.state.activeOption === 3
                        ? true
                        : false}
                        reloadData={this.state.activeOption === 1
                        ? this.swtichToAllEntries
                        : this.state.activeOption === 2
                            ? this.switchToActivePipelines
                            : this.state.activeOption === 3
                                ? this.switchToFinishedPipelines
                                : this.switchToFinishedPipelinesEntries}/>
                </Col>
            </Grid>
        </div >
    )

    render() {
        return (
            <div>
                <AppFrame header="Execution History" body={this.renderBody(this.props)}/>
                <AppFooter/>
            </div>
        );
    }
}

ExecutionHistoryContainer.propTypes = {
    historyList: PropTypes.array.isRequired,
    hasMore: PropTypes.bool.isRequired,
    resumeExecutingPipeline: PropTypes.func.isRequired,
    stopExecutingPipeline: PropTypes.func.isRequired,
    fetchActiveEntries: PropTypes.func.isRequired,
    getExecutionStateHistoryFinished: PropTypes.func.isRequired,
    getExecutionStateHistory: PropTypes.func.isRequired,
    cancelExecutingPipeline: PropTypes.func.isRequired,
    checkExecutingPipeline: PropTypes.func.isRequired,
    currentPage: PropTypes.number.isRequired,
    isLoading: PropTypes.bool.isRequired,
    changeToAllEntries: PropTypes.func.isRequired,
    changeToActiveEntries: PropTypes.func.isRequired,
    changeToFinishedEntries: PropTypes.func.isRequired,
    changeToFinishedPipelines: PropTypes.func.isRequired
}

const mapStateToProps = state => ({historyList: getHistoryList(state), hasMore: getHasMoreFlag(state), currentPage: getCurrentPage(state), isLoading: getIsLoading(state)});

const mapDispatchToProps = dispatch => ({
    resumeExecutingPipeline: pipelineId => dispatch(resumeExecutingPipeline(pipelineId)),
    stopExecutingPipeline: pipelineId => dispatch(stopExecutingPipeline(pipelineId)),
    cancelExecutingPipeline: pipelineId => dispatch(cancelExecutingPipeline(pipelineId)),
    checkExecutingPipeline: pipelineId => dispatch(checkExecutingPipeline(pipelineId)),
    fetchActiveEntries: _ => dispatch(fetchActiveEntries()),
    getExecutionStateHistoryFinished: pageNumber => dispatch(getExecutionStateHistoryFinished(pageNumber)),
    getExecutionStateHistory: pageNumber => dispatch(getExecutionStateHistory(pageNumber)),
    changeToAllEntries: (callback, option) => dispatch(changeToAllEntries(callback, option)),
    changeToActiveEntries: (callback, option) => dispatch(changeToActiveEntries(callback, option)),
    changeToFinishedEntries: (callback, option) => dispatch(changeToFinishedEntries(callback, option)),
    changeToFinishedPipelines: (callback, option) => dispatch(changeToFinishedPipelines(callback, option)),
    fetchFinishedPipelines: _ => dispatch(fetchFinishedPipelines())
});

const connectFunc = connect(mapStateToProps, mapDispatchToProps);

export default compose(connectFunc, withRouter,)(ExecutionHistoryContainer);