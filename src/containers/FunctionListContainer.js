/**
 * File description: The logic that is executing to show the information about plugins.
 * Author: Hellsoft
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './style.css';
import { toggleExpandedForAll } from 'react-sortable-tree';
import { fecthFunctionList, 
         setFunctionList,
         addNodeInformation } from '../actions';
import { getActivePipeline } from '../selectors/pipelinesState';
import { getFunctionList, getIsFunctionListFilled } from '../selectors/functionListState';
import FunctionListComponent from '../components/FunctionListComponent';
import { getNodeInformation } from '../selectors/nodeInformationState';
import ArrowBackIcon from '@material-ui/icons/NavigateBefore';
import ArrowForwardIcon from '@material-ui/icons/NavigateNext';


class FunctionListContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchString: '',
      searchFocusIndex: 0,
      isOpen: true
    };
  }

  componentDidMount() {
    this.props.fecthFunctionList();
  }

  selectPrevMatch = () => {
    const { searchFocusIndex, searchFoundCount } = this.state;

    this.setState({
      searchFocusIndex:
        searchFocusIndex !== null
          ? (searchFoundCount + searchFocusIndex - 1) % searchFoundCount
          : searchFoundCount - 1,
    });
  };

  selectNextMatch = () => {
    const { searchFocusIndex, searchFoundCount } = this.state;

    this.setState({
      searchFocusIndex:
        searchFocusIndex !== null
          ? (searchFocusIndex + 1) % searchFoundCount
          : 0,
    });
  };

  handleSearchOnChange = e => {
    this.setState({
      searchString: e.target.value,
    });
  };

  toggleNodeExpansion = expanded => {
    this.props.setFunctionList(toggleExpandedForAll({
      treeData: this.props.functionList,
      expanded,
    }),
    );
  };

  hideInfoNodeInfo = () => {
    this.setState({ dataNode: null });
  }

  modalNodeInfo = ({ node }) => {
    // const objectString = Object.keys(node)
    //   .map(k => (k === 'children' ? 'children: Array' :`${k}: '${node[k]}'`))
    //   .join(',\n\n   ');
   
    if(node.isDirectory === true){
      let packages = "";
    
      node.children.forEach(p => {
        packages += `\nPackage(s): \n\n${p.title}\n\n Package functions: `; 
        if(p.children !== undefined){
          p.children.forEach(n => {
            packages += `${n.title}, `;
          })
          packages += "\n";

        }
        
      });
      this.props.addNodeInformation(
        `\n\n${node.title}\n\n` +
        `\n\n${node.subtitle}\n\n`+
        packages 
    ); 
    }else{
      let inputs = "";
      node.inputs.forEach(i => {
        inputs+= `${i.title}, `;

      })
      inputs += "\n";

      let outputs = "";
      node.outputs.forEach(o => {
        outputs+= `${o.title}, `;

      })
      outputs += "\n";


      this.props.addNodeInformation(
        `\n\n${node.title}\n\n` +
        `\n\n${node.subtitle}\n\n`+
        `\n\nInputs: \n\n`+inputs +
        `\n\nOutputs: \n\n`+outputs +
        `\n\nParameters: \n${node.parameters}\n\n`
    ); 

    }

   
  };

  toggleSidebar = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  renderSidebar = () => {
    return (<FunctionListComponent
      functionList = {this.props.functionList}
      handleSearchOnChange = {this.handleSearchOnChange}
      searchFocusIndex = {this.state.searchFocusIndex}
      searchString = {this.state.searchString}
      setFunctionList = {this.props.setFunctionList}
      toggleNodeExpansion = {this.toggleNodeExpansion}
      addFilterToCanvas = {this.props.addFilterToCanvas}
      hideInfoNodeInfo = { this.hideInfoNodeInfo }
      modalNodeInfo = { this.modalNodeInfo }
      nodeInformation={this.props.nodeInformation}
      dataNode = {this.state.dataNode}
      isOpen = {this.state.isOpen}
    ></FunctionListComponent>);
 }

  render() {    
    return (
      <div>
        {
          this.state.isOpen ?
        <h2 className = "pipeline-name">{this.props.activePipeline.pipelineName}</h2> :
          <div></div>
        }
        <button className="toggle-bar-button" onClick = {this.toggleSidebar}>
          {
            this.state.isOpen ?
              <ArrowBackIcon fontSize="large"></ArrowBackIcon>            
            :
              <ArrowForwardIcon fontSize="large"></ArrowForwardIcon>  
          }
        </button>
        {this.renderSidebar()}        
      </div>
    );
  }
}

FunctionListContainer.propTypes = {
  fecthFunctionList: PropTypes.func.isRequired,
  setFunctionList: PropTypes.func.isRequired,
  addNodeInformation: PropTypes.func.isRequired,
  addFilterToCanvas: PropTypes.func.isRequired,
  functionList: PropTypes.array.isRequired,
  activePipeline: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  functionList: getFunctionList(state),
  nodeInformation: getNodeInformation(state),
  isFilled: getIsFunctionListFilled(state),
  activePipeline: getActivePipeline(state),
});

const mapDispatchToProps = dispatch => ({
  addNodeInformation: data => dispatch(addNodeInformation(data)),
  fecthFunctionList: _ => dispatch(fecthFunctionList()), 
  setFunctionList: data => dispatch(setFunctionList(data)), 

});

FunctionListContainer.defaultProps = {
  nodeInformation: ""
}

export default withRouter(
  connect(
    mapStateToProps, 
    mapDispatchToProps)(FunctionListContainer));
