/**
 * File description: The logic that is executing to show the information about registration.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import AppFrame from '../components/shared/AppFrame';
import UserRegisterComponent from '../components/UserRegisterComponent';
import {userSignUp} from '../actions/index';
import validate from '../utils/ValidateForm';

class RegisterContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            formControls: {
                lastName: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your lat name',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                firstName: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your name',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                userEmail: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your email',
                    valid: false,
                    validationRules: {
                        isRequired: true,
                        regex: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/
                    }
                },
                userName: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your user name',
                    valid: false,
                    validationRules: {
                        minLength: 6,
                        isRequired: true
                    }
                },
                password: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your password',
                    valid: false,
                    validationRules: {
                        regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                        isRequired: true
                    }
                }
            },
            formIsValid: false
        };
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };

        let validation = validate(value, updatedFormElement.validationRules,name);
        updatedFormElement.value = value;
        updatedFormElement.valid = validation.isValid;
        updatedFormElement.error = validation.errors !== ""
            ? validation.errors
            : null;

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }

        this.setState({formControls: updatedControls, formIsValid: formIsValid});
    }

    handleError = error => (this.setState({error: error}))

    submit = _ => {
        let {userEmail, firstName, lastName, userName, password} = this.state.formControls;
        if (this.state.formIsValid) {
            this
                .props
                .userSignUp({
                    email: userEmail.value,
                    firstName: firstName.value,
                    lastName: lastName.value,
                    userName: userName.value,
                    password: password.value
                }, this.handleError);
        } else {
            this.setState({error: "Please, fill the form in the right way"});
        }
    }

    render() {
        return (
            <div className="register-container">
                <AppFrame
                    header="Register"
                    body={< UserRegisterComponent error = {
                    this.state.error
                }
                userName = {
                    this.state.formControls.userName
                }
                userEmail = {
                    this.state.formControls.userEmail
                }
                password = {
                    this.state.formControls.password
                }
                firstName = {
                    this.state.formControls.firstName
                }
                lastName = {
                    this.state.formControls.lastName
                }
                onChange = {
                    this.changeHandler
                }
                onSubmit = {
                    this.submit
                } />}></AppFrame>
            </div>
        );
    }
}

RegisterContainer.propTypes = {
    userSignUp: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
    userSignUp: (user, errorHandler) => dispatch(userSignUp(user, errorHandler))
});

export default withRouter(connect(null, mapDispatchToProps)(RegisterContainer));
