/**
 * File description: The logic that is executing to show the information about login.
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import AppFrame from './../components/shared/AppFrame';
import UserLoginComponent from '../components/UserLoginComponent';
import {userLogin} from '../actions/index';
import validate from '../utils/ValidateForm';

class LoginContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            formControls: {
                userName: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your user name',
                    valid: false,
                    validationRules: {
                        minLength: 5,
                        isRequired: true
                    }
                },
                password: {
                    error: null,
                    value: "",
                    placeholder: 'Enter your password',
                    valid: false,
                    validationRules: {
                        regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                        isRequired: true
                    }
                }
            },
            formIsValid: false
        };
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };

        let validation = validate(value, updatedFormElement.validationRules, name);
        updatedFormElement.value = value;
        updatedFormElement.valid = validation.isValid;
        updatedFormElement.error = validation.errors;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }

        this.setState({formControls: updatedControls, formIsValid: formIsValid});
    }

    handleError = error => (this.setState({error: error}))

    submit = _ => {
        let {userName, password} = this.state.formControls;
        if (this.state.formIsValid) {
            this
                .props
                .userLogin({
                    userName: userName.value,
                    password: password.value
                }, this.handleError);
        } else {
            this.setState({error: "Please, fill the form in the right way"});
        }
    }

    render() {
        return (
            <div className="login-container">
                <AppFrame
                    header="Login"
                    body={< UserLoginComponent error = {
                    this.state.error
                }
                userName = {
                    this.state.formControls.userName
                }
                password = {
                    this.state.formControls.password
                }
                onChange = {
                    this.changeHandler
                }
                onSubmit = {
                    this.submit
                } />}/>
            </div>
        );
    }
}

LoginContainer.propTypes = {
    userLogin: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
    userLogin: (user, errorHandler) => dispatch(userLogin(user, errorHandler))
});

export default withRouter(connect(null, mapDispatchToProps)(LoginContainer));
