/**
 * File description: The logic that is executing to show the information about:
 * -> List of plugins.
 * -> Design area.
 * -> User action that can apply on the design area. 
 * Author: Hellsoft
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import AppFrame from '../components/shared/AppFrame';
import AppFooter from '../components/shared/AppFooter';
import FunctionListContainer from './FunctionListContainer';
import DesignAreaContainer from './DesignAreaContainer';
import {getActivePipeline, getStoredPipeline, getEditPipelineRequest, getEditCustomPluginRequest} from '../selectors/pipelinesState';
import {getIsCustomFilter} from '../selectors/customFilterState';
import {addPipelineId, addPipelineTimeStamp, setActivePipeLine, editPipeline, editCustomNode, setIsCustomFilter} from '../actions';
import {TIME_TO_SAVE_WIP} from '../constants';
import {compose} from 'recompose';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';

class WorkSpaceContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loadImageVisible: false,
            image: null
        }
        this.handleNodeCreation = this.handleNodeCreation;
        this.workSpaceRef = React.createRef();
        this.handleLocalStorageSync = this.handleLocalStorageSync;
    }

    updatePipeLineTimeStamp = _ => {
        //TODO: To a function component
        let currentTime = new Date();
        this
            .props
            .addPipelineTimeStamp(currentTime.getTime());
    }

    updatePipeLineID = (id) => {
        //TODO : Move to File area for new Pipelines could be UUID instead of numbers
        this
            .props
            .addPipelineId(id);
    }

    handleNodeCreation = (newNode) => {
        this
            .workSpaceRef
            .current
            .dispatchNodeCreation(newNode);
    }

    handleLocalStorageSync = (event) => {
        this.updatePipeLineTimeStamp();
        let { activePipeline, isCustomFilter} = this.props;
        let json = JSON.stringify(activePipeline);
        localStorage.setItem('Json' + activePipeline.pipelineId.toString(), json);
        localStorage.setItem( 'isCustomFilter', isCustomFilter);
    }

    storeCurrentPipeLine = () => {
        this.updatePipeLineTimeStamp();
        let {editPipeline, editPipelineRequest, editCustomNode, editCustomPluginRequest, isCustomFilter} = this.props;
        !isCustomFilter
            ? editPipeline(editPipelineRequest)
            : editCustomNode(editCustomPluginRequest);
    }

    setMostRecentPipeLine = () => {
        let {storedPipeline} = this.props;
        if(storedPipeline){
            let localStoragePipeLine = localStorage.getItem('Json' + storedPipeline.pipelineId.toString());
            this.props.setIsCustomFilter(localStorage.getItem('isCustomFilter') === "true" ? true:false);

            if (localStoragePipeLine !== null) {
                let ServerDate = new Date(storedPipeline.timeStamp);
                localStoragePipeLine = JSON.parse(localStoragePipeLine);
                let LocalStorageDate = new Date(localStoragePipeLine.timeStamp);
                if (LocalStorageDate >= ServerDate) {
                    this
                        .props
                        .setActivePipeLine(localStoragePipeLine);
                }
    
            } else {
                this.handleLocalStorageSync();
            }
        }
    }

    openModalLoadImage = () => {
        this.setState({loadImageVisible: true});
    }

    closeModalLoadImage = () => {
        this.setState({loadImageVisible: false});
    }

    onChangeImage = (files) => {
        //TODO: When all it's ok delete this
        this.setState({image: files[0]})
    }

    componentDidMount() {
        window.addEventListener("beforeunload", this.handleLocalStorageSync);
        this.setMostRecentPipeLine();
        setInterval(this.storeCurrentPipeLine, TIME_TO_SAVE_WIP);
    }

    componentWillUnmount() {
        this.handleLocalStorageSync();
        window.removeEventListener("beforeunload", this.handleLocalStorageSync)   
    }

    render() {
        return (
            <div>
                {/*showLinuxAlert()*/}
                
                <AppFrame
                    header="Design Area"
                    body=
                    {   <div className="grid">
                            <div className="left-bar"> 
                                <FunctionListContainer 
                                    addFilterToCanvas = { this.handleNodeCreation }  
                                /> 
                            </div> 
                            <div className="right-bar">
                                <DesignAreaContainer 
                                    ref={this.workSpaceRef} 
                                    sendPipeline={this.sendPipeline} 
                                    handleLocalStorageSync = {this.handleLocalStorageSync}/>
                            </div> 
                            <div className="no-pc-fallback">
                                <WarningRoundedIcon fontSize="large"></WarningRoundedIcon>
                                <p>
                                    Please open the application in a desktop browser !
                                </p>
                            </div>
                        </div> 
                    }>
                    </AppFrame>
                <AppFooter></AppFooter>
            </div>
        );
    }
}

WorkSpaceContainer.propTypes = {
    activePipeline: PropTypes.object.isRequired,
    storedPipeline: PropTypes.object.isRequired,
    addPipelineId: PropTypes.func.isRequired,
    addPipelineTimeStamp: PropTypes.func.isRequired,
    setActivePipeLine: PropTypes.func.isRequired,
    editPipeline: PropTypes.func.isRequired,
    editPipelineRequest: PropTypes.object.isRequired,
    isCustomFilter: PropTypes.bool.isRequired,
    editCustomNode: PropTypes.func.isRequired,
    editCustomPluginRequest: PropTypes.object.isRequired,
    setIsCustomFilter: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    activePipeline: getActivePipeline(state),
    storedPipeline: getStoredPipeline(state),
    editPipelineRequest: getEditPipelineRequest(state),
    isCustomFilter: getIsCustomFilter(state),
    editCustomPluginRequest: getEditCustomPluginRequest(state)
});

const mapDispatchToProps = dispatch => ({
    addPipelineId: id => dispatch(addPipelineId(id)),
    addPipelineTimeStamp: timeStamp => dispatch(addPipelineTimeStamp(timeStamp)),
    setActivePipeLine: pipeline => dispatch(setActivePipeLine(pipeline)),
    editPipeline: activePipeline => dispatch(editPipeline(activePipeline)),
    editCustomNode: editCustomPluginRequest => dispatch(editCustomNode(editCustomPluginRequest)),
    setIsCustomFilter: data => dispatch(setIsCustomFilter(data))
});

const connectFunc = connect(mapStateToProps, mapDispatchToProps, null);

export default compose(connectFunc,)(WorkSpaceContainer);
