import {createAction} from 'redux-actions';
import {
    SET_NODE,
    SEND_PIPELINE,
    SET_EXCECUTION_NODE_ID,
    CHANGE_NODE_POSITION,
    STORE_PIPELINE,
    EDIT_PIPELINE,
    ADD_PIPELINE_ID,
    ADD_PIPELINE_TIME_STAMP,
    ADD_DESCRIPTOR_NODE,
    DELETE_DESCRIPTOR_NODE,
    ADD_DESCRIPTOR_CONNECTION,
    DELETE_DESCRIPTOR_CONNECTION,
    ADD_DESCRIPTOR_PARAMETER_VALUE,
    ADD_DESCRIPTOR_PARAMETER_CONNECTION,
    DELETE_DESCRIPTOR_PARAMETER_CONNECTION,
    SET_ACTIVE_PIPELINE,
    CLEAN_LOCAL_STORAGE_PIPELINE,
    CLEAR_ACTIVE_PIPELINE,
    ADD_NODE_INFORMATION,
    SET_PREV_PIPELINE,
    CLEAN_PREV_PIPELINE,
    STOP_EXECUTING_PIPELINE,
    SET_PIPELINE_LIST,
    FETCH_PIPELINE_LIST,
    FETCH_SPECIFIC_PIPELINE,
    DELETE_SELECTED_PIPELINE,
    STORE_PIPELINE_FAILURE,
    RESET_ERRORS,
    EDIT_PIPELINE_FAILURE,
    EDIT_CUSTOM_PLUGIN,
    EDIT_CUSTOM_PLUGIN_FAILURE,
    CREATE_CUSTOM_NODE,
    CREATE_CUSTOM_NODE_FAILURE,
    DELETE_CUSTOM_NODE,
    ADD_INPUT_CUSTOM_FILTER,
    DELETE_INPUT_CUSTOM_FILTER,
    ADD_OUTPUT_CUSTOM_FILTER,
    DELETE_OUTPUT_CUSTOM_FILTER,
    IS_CUSTOM_FILTER,
    SET_SELECTED_FILE,
    CLEAN_SELECTED_FILE,
    CHECK_EXECUTING_PIPELINE,
    CANCEL_EXECUTING_PIPELINE,
    RESUME_EXECUTING_PIPELINE,
    FETCH_NOTIFICATION_PIPELINES,
    SET_ACTIVE_PIPELINE_INFO,
    SAVE_INPUTS_OUTPUTS
} from '../constants';
import {apiGet, apiPost, apiPut, apiDelete} from '../api';
import {
    urlNewPipeline,
    urlUpdatePipeline,
    urlExecutePipeline,
    urlStopPipeline,
    urlGetUserPipelines,
    urlGetSpecificPipeline,
    urlDeletePipeline,
    urlGetSpecificCustomPlugin,
    urlCreateCustomPlugin,
    urlUpdateCustomPlugin,
    urlDeleteCustomPlugin,
    urlGetUserCustomPlugins,
    urlCheckPipeline,
    urlCancelPipeline,
    urlResumePipeline
} from '../api/urls/index';
import {handleErrorInMessage} from '../api/errors/handleErrorInMessage';
import {success, failure} from '../utils/actionType';
import {history} from '../helpers/history';
import {DESIGN_AREA_ROUTE} from '../constants/routes';
import {_PIPELINE_FUNCTIONS, _PLUGIN_FUNCTIONS} from '../constants/componentFunctions';
import {changeIsLoadingPageState} from './loadingPage.actions';

//Selected files
export const setSelectedFile = createAction(SET_SELECTED_FILE, data => data);
export const resetSelectedFile = createAction(CLEAN_SELECTED_FILE);

//Pipeline descriptor actions
export const addDescriptorNode = createAction(ADD_DESCRIPTOR_NODE, data => data);
export const deleteDescriptorNode = createAction(DELETE_DESCRIPTOR_NODE, data => data);
export const addDescriptorConnection = createAction(ADD_DESCRIPTOR_CONNECTION, data => data);
export const deleteDescriptorConnection = createAction(DELETE_DESCRIPTOR_CONNECTION, data => data);
export const addDescriptorParameterValue = createAction(ADD_DESCRIPTOR_PARAMETER_VALUE, data => data);
export const addDescriptorParameterConnection = createAction(ADD_DESCRIPTOR_PARAMETER_CONNECTION, data => data);
export const deleteDescriptorParameterConnection = createAction(DELETE_DESCRIPTOR_PARAMETER_CONNECTION, data => data);
export const addPipelineId = createAction(ADD_PIPELINE_ID, data => data);
export const addPipelineTimeStamp = createAction(ADD_PIPELINE_TIME_STAMP, data => data);
export const setActivePipeLine = createAction(SET_ACTIVE_PIPELINE);
export const setexecutionNodeId = createAction(SET_EXCECUTION_NODE_ID, data => data);
export const changeNodePosition = createAction(CHANGE_NODE_POSITION, data => data);
export const cleanLocalStoragePipeline = createAction(CLEAN_LOCAL_STORAGE_PIPELINE);
export const setUserPipelines = createAction(SET_PIPELINE_LIST);
export const clearActivePipeline = createAction(CLEAR_ACTIVE_PIPELINE);
export const setActivePipelineInfo = createAction(SET_ACTIVE_PIPELINE_INFO);

//PipeLine server actions
export const sendPipeline = pipeline => dispatch => {
    apiPost(urlExecutePipeline, pipeline)().then(result => {

        dispatch(success(SEND_PIPELINE, result))
    }, error => {
        dispatch(failure(EDIT_CUSTOM_PLUGIN_FAILURE, handleErrorInMessage(error, _PIPELINE_FUNCTIONS)));
    })
};
export const storePipeline = (newPipeline, handleError) => (dispatch => (apiPost(urlNewPipeline, newPipeline)().then(pipeline => {
    dispatch(success(STORE_PIPELINE, pipeline));
    dispatch(clearErrorMessages());
    dispatch(fetchSpecificPipeline(pipeline.fileId));
    dispatch(setSelectedFile(pipeline));
}, error => {
    dispatch(failure(STORE_PIPELINE_FAILURE, handleErrorInMessage(error, _PIPELINE_FUNCTIONS)));
    handleError(handleErrorInMessage(error, _PIPELINE_FUNCTIONS));
    return error;
}).then(v => {
    if (v === undefined) {
        history.push(DESIGN_AREA_ROUTE);
    }
})));

export const editPipeline = (pipelineData) => (dispatch => {
    apiPut(urlUpdatePipeline, pipelineData)().then(pipeline => {
        dispatch(success(EDIT_PIPELINE, pipeline));
        dispatch(clearErrorMessages());
    }, error => {
        dispatch(failure(EDIT_PIPELINE_FAILURE, handleErrorInMessage(error, _PIPELINE_FUNCTIONS)))
    })
})

export const fetchUserPipelines = _ => dispatch => { 
    dispatch(changeIsLoadingPageState(true));
    apiGet(urlGetUserPipelines)().then( 
        pipelines => {
            dispatch(success(FETCH_PIPELINE_LIST,pipelines ));
            dispatch(changeIsLoadingPageState(false));
        }) 
};

export const fetchUserNotifications = createAction(FETCH_NOTIFICATION_PIPELINES, apiGet(urlGetUserPipelines));
export const fetchSpecificPipeline = (params) => (dispatch => {
    dispatch(changeIsLoadingPageState(true));
    apiGet(urlGetSpecificPipeline, params)().then(pipeline => {
        dispatch(setIsCustomFilter(false));
        dispatch(clearActivePipeline());
        dispatch(success(FETCH_SPECIFIC_PIPELINE, pipeline));
        dispatch(changeIsLoadingPageState(false));
    });
});
export const stopExecutingPipeline = createAction(STOP_EXECUTING_PIPELINE, pipelineId => apiGet(urlStopPipeline, pipelineId)());
export const checkExecutingPipeline = createAction(CHECK_EXECUTING_PIPELINE, pipelineId => apiGet(urlCheckPipeline, pipelineId)());
export const cancelExecutingPipeline = createAction(CANCEL_EXECUTING_PIPELINE, pipelineId => apiGet(urlCancelPipeline, pipelineId)());
export const resumeExecutingPipeline = createAction(RESUME_EXECUTING_PIPELINE, pipelineId => apiGet(urlResumePipeline, pipelineId)());
export const deleteSelectedPipeline = createAction(DELETE_SELECTED_PIPELINE, pipelineId => apiDelete(urlDeletePipeline, pipelineId)());

//Node Information
export const addNodeInformation = createAction(ADD_NODE_INFORMATION, data => data);
export const setNode = createAction(SET_NODE);

//Custom node
export const setIsCustomFilter = createAction(IS_CUSTOM_FILTER, data => data);
export const setPrevPipeline = createAction(SET_PREV_PIPELINE, data => data);
export const cleanPrevPipeline = createAction(CLEAN_PREV_PIPELINE, data => data);
export const addInputCustomFilter = createAction(ADD_INPUT_CUSTOM_FILTER, data => data);
export const deleteInputCustomFilter = createAction(DELETE_INPUT_CUSTOM_FILTER, data => data);
export const addOutputCustomFilter = createAction(ADD_OUTPUT_CUSTOM_FILTER, data => data);
export const deleteOutputCustomFilter = createAction(DELETE_OUTPUT_CUSTOM_FILTER, data => data);
export const saveInputsOutputsCustomFilter = createAction(SAVE_INPUTS_OUTPUTS, data => data);

export const fetchUserCustomPlugins = _ => dispatch => { 
    dispatch(changeIsLoadingPageState(true));
    apiGet(urlGetUserCustomPlugins)().then( 
        plugins => {
            dispatch(success(FETCH_PIPELINE_LIST,plugins ));
            dispatch(changeIsLoadingPageState(false));
        }) 
};

export const fetchSpecificCustomNode = params => (dispatch => {
    dispatch(changeIsLoadingPageState(true));
    apiGet(urlGetSpecificCustomPlugin, params)().then(node => {
        dispatch(setIsCustomFilter(true));
        dispatch(clearActivePipeline());
        dispatch(success(FETCH_SPECIFIC_PIPELINE, node));
        dispatch(changeIsLoadingPageState(false));
    });
})
export const deleteSelectedCustomNode = createAction(DELETE_CUSTOM_NODE, customNodeId => apiDelete(urlDeleteCustomPlugin, customNodeId)());
export const createCustomNode = (customNode, handleError) => (dispatch => (apiPost(urlCreateCustomPlugin, customNode)().then(node => {
    dispatch(success(CREATE_CUSTOM_NODE, node));
    dispatch(clearErrorMessages());
    dispatch(fetchSpecificCustomNode(node.fileId));
    dispatch(setSelectedFile(node));
}, error => {
    dispatch(failure(CREATE_CUSTOM_NODE_FAILURE, handleErrorInMessage(error, _PLUGIN_FUNCTIONS)));
    handleError(handleErrorInMessage(error, _PLUGIN_FUNCTIONS));
    return error;
}).then(v => {
    if (v === undefined) {
        history.push(DESIGN_AREA_ROUTE);
    }
})));

export const editCustomNode = (customNode) => (dispatch => {
    apiPut(urlUpdateCustomPlugin, customNode)().then(pipeline => {
        dispatch(success(EDIT_CUSTOM_PLUGIN, pipeline));
        dispatch(clearErrorMessages());
    }, error => {
        dispatch(failure(EDIT_CUSTOM_PLUGIN_FAILURE, handleErrorInMessage(error, _PIPELINE_FUNCTIONS)))
    })
})

//Errors
export const clearErrorMessages = createAction(RESET_ERRORS);