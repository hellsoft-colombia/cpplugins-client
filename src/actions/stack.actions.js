import { createAction } from 'redux-actions';
import { ADD_UNDO_COMMAND_STACK, ADD_REDO_COMMAND_STACK, CLEAR_STACK } from '../constants';

export const addUndoCommandStack = createAction(ADD_UNDO_COMMAND_STACK, payload => payload);
export const addRedoCommandStack = createAction(ADD_REDO_COMMAND_STACK, payload => payload);
export const clearStack = createAction(CLEAR_STACK, payload => payload);
