import {createAction} from 'redux-actions';
import {
    USER_LOGIN,
    USER_LOGIN_FAILURE,
    USER_REGISTER,
    USER_REGISTER_FAILURE,
    USER_INFO,
    USER_INFO_FAILURE,
    USER_LOGOUT
} from '../constants';
import {FILE_AREA_ROUTE} from '../constants/routes';
import {apiPost, apiGet} from '../api';
import {urlUserAuthentication, urlUserInformation, urlUserRegister, urlUserLogout} from '../api/urls/index';
import {success, failure} from '../utils/actionType';
import {history} from '../helpers/history';
import {handleErrorInMessage} from '../api/errors/handleErrorInMessage';
import {_USER_FUNCTIONS} from '../constants/componentFunctions';

export const fetchUserInfo = _ => (dispatch => {
    apiGet(urlUserInformation)().then(user => {
        dispatch(success(USER_INFO, user))
    }, error => {
        dispatch(failure(USER_INFO_FAILURE, error.status()));
    })
});

export const userLogin = (userCredentials, handleError) => (dispatch => {
    apiPost(urlUserAuthentication, userCredentials)().then(user => {
        dispatch(success(USER_LOGIN, user));
        handleError(null);
    }, error => {
        handleError("Wrong credentials !");
        dispatch(failure(USER_LOGIN_FAILURE, error.status));
        return error;
    }).then(v => {
        if (v === undefined) {
            dispatch(fetchUserInfo())
            history.push(FILE_AREA_ROUTE);
        }
    })
});

export const userSignUp = (userData, handleError) => (dispatch => {
    apiPost(urlUserRegister, userData)().then(user => {
        dispatch(success(USER_REGISTER, user));
        handleError(null);
    }, error => {
        handleError((handleErrorInMessage(error,_USER_FUNCTIONS)));
        dispatch(failure(USER_REGISTER_FAILURE, error.status));
        return error;
    }).then(v => {
        if (v === undefined) {
            dispatch(userLogin({userName: userData.userName, password: userData.password}, handleError))
        }
    })
});

export const userLogout = createAction(USER_LOGOUT, apiGet(urlUserLogout));
