import {createAction} from 'redux-actions';
import {
    ELEMENTS_PER_PAGE,
    HISTORY_FAILURE,
    FETCH_USER_EXECUTION_HISTORY,
    FETCH_ACTIVE_PIPELINES_ENTRIES,
    FETCH_FINISHED_PIPELINES,
    CHANGE_HAS_MORE_STATE,
    RESET_HISTORY_LIST,
    INCREASE_CURRENT_PAGE,
    UPDATE_IS_LOADING,
    CHANGE_TO_ALL_ENTRIES,
    CHANGE_TO_ACTIVE_PIPELINES,
    CHANGE_TO_FINISHED_PIPELINES,
    CHANGE_TO_FINISHED_ENTRIES
} from '../constants';
import {apiGet} from '../api';
import {urlGetUserHistory, urlGetUserPipelinesByState, urlGetUserHistoryByState} from '../api/urls';
import {handleErrorInMessage} from '../api/errors/handleErrorInMessage';
import {success, failure} from '../utils/actionType';
import {_EXECUTION_HISTORY} from '../constants/componentFunctions';

//Execution History

const changeHasMoreState = createAction(CHANGE_HAS_MORE_STATE, data => data);

const changeIsLoadingState = createAction(UPDATE_IS_LOADING, data => data);

const resetHistoryState = (callback, option) => (dispatch => {
    dispatch(success(RESET_HISTORY_LIST));
    callback(option);
});

export const getExecutionStateHistory = pageNumber => (dispatch => {
    dispatch(changeIsLoadingState(true));
    apiGet(urlGetUserHistory + pageNumber + "/" + ELEMENTS_PER_PAGE)().then(elements => {
        if (elements.length > 0) {
            dispatch(success(FETCH_USER_EXECUTION_HISTORY, elements));
            dispatch(success(INCREASE_CURRENT_PAGE));
        } else {
            dispatch(changeIsLoadingState(false));
            dispatch(changeHasMoreState(false));
        }
    }, error => {
        dispatch(changeIsLoadingState(false));
        dispatch(failure(HISTORY_FAILURE, handleErrorInMessage(error, _EXECUTION_HISTORY)));
    })
});

export const getExecutionStateHistoryFinished = (pageNumber) => (dispatch => {
    dispatch(changeIsLoadingState(true));
    apiGet(urlGetUserHistoryByState + "FINISHED/" + pageNumber + "/" + ELEMENTS_PER_PAGE)().then(elements => {
        if (elements.length > 0) {
            dispatch(success(FETCH_USER_EXECUTION_HISTORY, elements));
            dispatch(success(INCREASE_CURRENT_PAGE));
        } else {
            dispatch(changeIsLoadingState(false));
            dispatch(changeHasMoreState(false));
        }
    }, error => {
        dispatch(changeIsLoadingState(false));
        dispatch(failure(HISTORY_FAILURE, handleErrorInMessage(error, _EXECUTION_HISTORY)));
    })
});

export const fetchActiveEntries = _ => (dispatch => {
    dispatch(changeIsLoadingState(true));
    apiGet(urlGetUserPipelinesByState + 'EXECUTING')().then(elements => {
        if (elements.length > 0) {
            dispatch(success(FETCH_ACTIVE_PIPELINES_ENTRIES, elements));
            dispatch(success(INCREASE_CURRENT_PAGE));
        }
        dispatch(changeIsLoadingState(false));
        dispatch(changeHasMoreState(false));
    }, error => {
        dispatch(changeIsLoadingState(false));
        dispatch(failure(HISTORY_FAILURE, handleErrorInMessage(error, _EXECUTION_HISTORY)));
    })
});

export const fetchFinishedPipelines = _ => (dispatch => {
    dispatch(changeIsLoadingState(true));
    apiGet(urlGetUserPipelinesByState + 'FINISHED')().then(elements => {
        if (elements.length > 0) {
            dispatch(success(FETCH_FINISHED_PIPELINES, elements));
            dispatch(success(INCREASE_CURRENT_PAGE));
        }
        dispatch(changeIsLoadingState(false));
        dispatch(changeHasMoreState(false));
    }, error => {
        dispatch(changeIsLoadingState(false));
        dispatch(failure(HISTORY_FAILURE, handleErrorInMessage(error, _EXECUTION_HISTORY)));
    })
});

export const changeToAllEntries = (callback, option) => (dispatch => {
    dispatch(success(CHANGE_TO_ALL_ENTRIES));
    dispatch(resetHistoryState(callback, option));
    dispatch(getExecutionStateHistory(0));
});

export const changeToActiveEntries = (callback, option) => (dispatch => {
    dispatch(success(CHANGE_TO_ACTIVE_PIPELINES));
    dispatch(resetHistoryState(callback, option));
    dispatch(fetchActiveEntries());
});

export const changeToFinishedEntries = (callback, option) => (dispatch => {
    dispatch(success(CHANGE_TO_FINISHED_PIPELINES));
    dispatch(resetHistoryState(callback, option));
    dispatch(getExecutionStateHistoryFinished(0));
});

export const changeToFinishedPipelines = (callback, option) => (dispatch => {
    dispatch(success(CHANGE_TO_FINISHED_ENTRIES));
    dispatch(resetHistoryState(callback, option));
    dispatch(fetchFinishedPipelines());
});
