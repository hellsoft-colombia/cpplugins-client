import {createAction} from 'redux-actions';
import {FETCH_FUNCTION_LIST, SET_FUNCTION_LIST} from '../constants';
import {apiGet} from '../api';
import {urlFunctionList} from '../api/urls/index';

//Plugins actions
export const setFunctionList = createAction(SET_FUNCTION_LIST, data => data);

//Plugins server functions
export const fecthFunctionList = createAction(FETCH_FUNCTION_LIST, apiGet(urlFunctionList));