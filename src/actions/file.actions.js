import {createAction} from 'redux-actions';
import {
    SET_FILE_LIST,
    FETCH_FILE_LIST,
    SORT_CURRENT_LIST,
    SEND_FILE,
    DELETE_FILE,
    ADD_FILE_NODE
} from '../constants';
import {apiFormDataPost, apiGet, apiDelete} from '../api';
import {urlGetUserImages, urlUploadImage, urlDeleteImage} from '../api/urls/index';
import {success,} from '../utils/actionType';
import { changeIsLoadingPageState } from './loadingPage.actions';

//File Actions
export const setUserFileList = createAction(SET_FILE_LIST);
export const sortCurrentList = createAction(SORT_CURRENT_LIST, data => data);

//File server functions
export const fetchUserImages = createAction(FETCH_FILE_LIST, apiGet(urlGetUserImages));
export const sendFile = (file, callback) => (dispatch => {
    //Review 
    //dispatch(changeIsLoadingPageState(true));
    apiFormDataPost(urlUploadImage, file)().then(result => (dispatch(success(SEND_FILE, result)))).then(
        response => {
            dispatch(changeIsLoadingPageState(false));
            callback();
        },
        error => {
            dispatch(changeIsLoadingPageState(false));
        }
    )    
});

export const deleteFile = createAction(DELETE_FILE, fileId => apiDelete(urlDeleteImage, fileId)())

// File parameters 
export const addFileNode = createAction(ADD_FILE_NODE, data => data);
