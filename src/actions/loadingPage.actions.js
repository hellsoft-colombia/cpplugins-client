import {createAction} from 'redux-actions';
import {CHANGE_IS_LOADING_PAGE} from '../constants';

export const changeIsLoadingPageState = createAction(CHANGE_IS_LOADING_PAGE, data => data);