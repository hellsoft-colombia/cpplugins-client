/**
 * File description: Util functions to create the pipeline with the help of a descriptor,
 * which contains the information of the nodes created before by the user.
 * Author: Hellsoft
 */
import { createConnection, nodeSelector, parametersSelector, getParameterPort } from './PipelineElements';
import { PARAMETER_EDITION } from '../constants';

export const createPipeline = ( { pipelineTitle, selectedFile, setActivePipelineInfo, storedPipeline, canvas, dispatchNodeCreation, 
    dispatchConnectionCreation, dispatchParameterConnectionCreation, dispatchParameterEdition, 
    addFileNode, eventTriggered } ) => {
    if(storedPipeline === undefined)
        return;
        
    let pipelineName;
    let { nodes, connections, parameters, 
        pipelineId, executionNodeId, timeStamp } = storedPipeline;
    
    
    if(selectedFile.fileName !== "")
        pipelineName = selectedFile.fileName;
    else if(storedPipeline.pipelineName && storedPipeline.pipelineName !== "")
        pipelineName = storedPipeline.pipelineName;
    else
        pipelineName = pipelineTitle;

    setActivePipelineInfo({
        pipelineId,
        pipelineName,
        executionNodeId,
        timeStamp
    });
    if(nodes != null){
        for(let node of nodes){
            node.d2dId = dispatchNodeCreation(node);
        }
    }
    
    if(connections != null){
        for(let connection of connections){
            let source = canvas.getFigure(
                getNodeD2dId(nodes, connection.from.nodeId)
            );
            let target = canvas.getFigure(
                getNodeD2dId(nodes, connection.to.nodeId)
            );
            
            let sourcePort;
            let targetPort;
            if(source && target){
                sourcePort = nodeSelector(source).getOutputPort( connection.from.name );
                targetPort = nodeSelector(target).getInputPort( connection.to.name );
            }

            if(!sourcePort){
                sourcePort = nodeSelector(source).getInputPort( connection.from.name );
                targetPort = nodeSelector(target).getOutputPort( connection.to.name );
            }
            
            if(sourcePort && targetPort){
                canvas.add(
                    new createConnection({ 
                        source: sourcePort,
                        target: targetPort,
                        onCreateConnectionFunc: dispatchConnectionCreation,
                        onCreateParameterConnectionFunc: dispatchParameterConnectionCreation,
                        pipelineConstructor: true
                    })
                );
            }
        }
    }
    
    if(parameters != null){
        for(let parameter of parameters){
            let source = null;
            let target = null;
            /*
            if(!source){
                target = canvas.getFigure(
                    getNodeD2dId(nodes, parameter.value.nodeId)
                );
                source = canvas.getFigure(
                    getNodeD2dId(nodes, parameter.nodeId)
                );
            }
            */
            if(parameter.value !== undefined && parameter.nodeId !== undefined){
                source = canvas.getFigure(
                    getNodeD2dId(nodes, parameter.value.nodeId)
                );
                target = canvas.getFigure(
                    getNodeD2dId(nodes, parameter.nodeId)
                );
            }
            

            if(parameter.value instanceof Object){
                if(parameter.value.fileId){
                    let mNode = canvas.getFigure(
                        getNodeD2dId(nodes, parameter.nodeId));
                    if(mNode){
                        let reader = parametersSelector(mNode);
                        const param = {
                            nodeId: parameter.nodeId,
                            parameterId: parameter.parameterId,
                            value: { 
                                fileId : parameter.value.fileId, 
                                fileName: parameter.value.fileName
                            }
                        };
                        addFileNode(param);
                        if(getParameterPort(parameter.parameterId, true, reader) && getParameterPort(parameter.parameterId, true, reader).createParameterInput)
                            getParameterPort(parameter.parameterId, true, reader).createParameterInput(parameter.value.fileName);
                        eventTriggered(
                            {
                                nodeId: mNode.cppId,
                                parameterId: parameter.parameterId,
                                value: parameter.parameterId,
                            },
                            PARAMETER_EDITION
                        );
                    }
                }
                else{
                    if(source && target){
                        canvas.add(
                            new createConnection({ 
                                source: parametersSelector(source).getOutputPort( parameter.value.parameterId ), 
                                target: parametersSelector(target).getInputPort( parameter.parameterId ),
                                onCreateConnectionFunc: dispatchConnectionCreation,
                                onCreateParameterConnectionFunc: dispatchParameterConnectionCreation,
                                pipelineConstructor: true
                            })
                        );
                    }
                }
            }
            else {
                //console.log("111")
                
                if(!target){
                    target = canvas.getFigure(
                        getNodeD2dId(nodes, parameter.nodeId)
                    );
                }
                //console.log(parameter.nodeId);
                //console.log(getNodeD2dId(nodes, parameter.nodeId));

                if(target){   
                    parametersSelector(target).getInputPort( parameter.parameterId ).createParameterInput(parameter.value);
                    dispatchParameterEdition(parameter);
                    eventTriggered(
                        {
                            nodeId: target.cppId,
                            parameterId: parameter.parameterId,
                            value: parameter.parameterId,
                        },
                        PARAMETER_EDITION
                    );
                }
                else if(source){
                    parametersSelector(source).getInputPort( parameter.parameterId ).createParameterInput(parameter.value);
                    dispatchParameterEdition(parameter);
                    eventTriggered(
                        {
                            nodeId: source.cppId,
                            parameterId: source.parameterId,
                            value: parameter.parameterId,
                        },
                        PARAMETER_EDITION
                    );
                }
                
            }
            /*
            else{
                parametersSelector(target).getInputPort( parameter.parameterId ).createParameterInput(parameter.value);
                dispatchParameterEdition(parameter);
            }*/
        }
    }
    if( selectedFile.inputs || selectedFile.outputs || storedPipeline.inputs || storedPipeline.outputs){
        let inputs = storedPipeline.inputs === undefined ? selectedFile.inputs : storedPipeline.inputs;
        let outputs = storedPipeline.outputs === undefined ? selectedFile.outputs : storedPipeline.outputs;
        if(inputs.length > 0){
            inputs.map( input => {
                let nodeInput = canvas.getFigure(getNodeD2dId(nodes, input.reference.nodeId));
                if(nodeInput === null)
                    return 1;
                let inputPort = nodeSelector(nodeInput).getInputPort( input.reference.ioId );
                let inputP = {
                    name: input.name,
                    reference: { 
                        nodeId: input.reference.nodeId,
                        ioId: input.reference.ioId,
                    }
                };
                inputPort.setColor("#ff0000");
                inputPort.addInputCustomFilter(inputP);
                return 1;
            }
            );
        }

        if(outputs.length > 0){
            outputs.map( output => {
                let nodeOutput = canvas.getFigure(getNodeD2dId(nodes, output.reference.nodeId));
                if(nodeOutput === null)
                    return 1;
                let outputPort = nodeSelector(nodeOutput).getOutputPort( output.reference.ioId );
                let outputP = {
                    name: output.name,
                    reference: { 
                        nodeId: output.reference.nodeId,
                        ioId: output.reference.ioId,
                    }
                };
                outputPort.setColor("#ff0000");
                outputPort.addOutputCustomFilter(outputP);
                return 1;
            });
        }
    }

}

const getNodeD2dId = (nodes, nodeId) => {
    let i = 0;
    while( i < nodes.length){
        if (nodes[i].nodeId === nodeId){
            return nodes[i].d2dId;
        }
        i++;
    }
    return null;
}