/**
 * File description: Functions that helps to sort files or pipelines accoring to the cryterial.
 * Author: Hellsoft
 */
const sortByTextAsc =  (list,field) =>(
    list.sort((a,b) => a[field].localeCompare(b[field]) )
);

const sortByTextDsc =  (list,field) =>(
    list.sort((a,b) => b[field].localeCompare(a[field]) )
);

const sortByDateAsc =  (list,field) =>(
    list.sort((a,b) => {

        let firstDateArray = a[field].split("/");
        let secondDateArray = b[field].split("/");

        let firstDate = new Date(+firstDateArray[2], firstDateArray[1] - 1, +firstDateArray[0]);
        let secondDate = new Date(+secondDateArray[2], secondDateArray[1] - 1, +secondDateArray[0]);

        return firstDate>secondDate ? -1 : firstDate<secondDate ? 1 : 0;
    })
);

const sortByDateDsc =  (list,field) =>(
    list.sort((a,b) => {

        let firstDateArray = a[field].split("/");
        let secondDateArray = b[field].split("/");

        let firstDate = new Date(+firstDateArray[2], firstDateArray[1] - 1, +firstDateArray[0]);
        let secondDate = new Date(+secondDateArray[2], secondDateArray[1] - 1, +secondDateArray[0]);

        return secondDate>firstDate ? -1 : secondDate<firstDate ? 1 : 0;
    })
);

const sortByStateAsc =  (list,field) =>(
    list.sort((a,b) => {
        let myStates = {
            'EXECUTING' : 10,
            'STOPPED' : 5,
            'EXPIRED' : 0
        };

        return myStates[a[field]]>myStates[b[field]] ? -1 : myStates[a[field]]<myStates[b[field]] ? 1 : 0; 
    })
);

const sortByStateDsc =  (list,field) =>(
    list.sort((a,b) => {
        let myStates = {
            'EXECUTING' : 10,
            'STOPPED' : 5,
            'EXPIRED' : 0
        };

        return myStates[b[field]]>myStates[a[field]] ? -1 : myStates[b[field]]<myStates[a[field]] ? 1 : 0; 
    })
);

export const sortElementsAsc = (list,field) =>{
    if(field === 'fileName'){
        return sortByTextAsc(list,field);
    }else if(field === 'lastModificationDate'){
        return sortByDateAsc(list,field);
    }else{
        return sortByStateAsc(list,field);
    }
};

export const sortElementsDsc = (list,field) =>{
    if(field === 'fileName'){
        return sortByTextDsc(list,field);
    }else if(field === 'lastModificationDate'){
        return sortByDateDsc(list,field);
    }else{
        return sortByStateDsc(list,field);
    }
};