/**
 * File description: Util functions to handle the behaviour the command stack.
 * Author: Hellsoft
 */
import { 
    createConnection, 
    nodeSelector, 
    parametersSelector,
    handleHideConnections,
    handleShowConnections,
    getParameterPort
} from './PipelineElements';
import { 
    CREATE_NODE,
    DELETE_NODE,
    MOVEMENT_NODE,
    CREATE_NODE_CONNECTION,
    DELETE_NODE_CONNECTION,
    CREATE_PARAMETER_CONNECTION,
    DELETE_PARAMETER_CONNECTION,
    SHOW_PARAMETERS,
    HIDE_PARAMETERS,
    PARAMETER_EDITION,
    ADD_FILE_READER,
} from '../constants';
import CPPParameterInputPort from './PipelineElements/Parameter/CPPParameterInputPort';

export const addToCommandStack = (figure, command, stack, props) => {
    let commandObject = {
        command: command,
        infoItem: figure
    }
    if(stack.isUndo){
        props.addUndoCommandStack(commandObject);     
    }else{
        props.addRedoCommandStack(commandObject);     
    }
}

export const doInverseOperation = ({command, infoItem}, { state, dispatchNodeCreation, 
    dispatchNodeChangePosition, dispatchConnectionCreation, dispatchParameterConnectionCreation,
    dispatchParameterEdition, eventTriggered, dispatchNodeFile}) => {
    switch (command) {
        case CREATE_NODE:
            const node = state.workSpace.getFigure(infoItem.id);
            state.workSpace.remove(node);
            break;
        case DELETE_NODE:
            dispatchNodeCreation(infoItem);
            break;
        case MOVEMENT_NODE:
            const currentNode = state.workSpace.getFigure(infoItem.id);
            currentNode.setPosition(infoItem.x, infoItem.y);
            dispatchNodeChangePosition(currentNode);
            break;
        case CREATE_NODE_CONNECTION:
            const connection = state.workSpace.getLine(infoItem.id);  
            state.workSpace.remove(connection);
            break;
        case DELETE_NODE_CONNECTION:
            const sourceNode = state.workSpace.getFigure(infoItem.sourceNode);
            const targetNode = state.workSpace.getFigure(infoItem.targetNode);
            state.workSpace.add(
                new createConnection({ 
                    source: nodeSelector(sourceNode).getPorts().find(
                        port => port.cppId === infoItem.sourcePort),
                    target: nodeSelector(targetNode).getPorts().find(
                        port => port.cppId === infoItem.targetPort),
                    onCreateConnectionFunc: dispatchConnectionCreation,
                    currentId: infoItem.id 
                })
            );
            break;
        case CREATE_PARAMETER_CONNECTION:
            const parameterConnection = state.workSpace.getLine(infoItem.id);  
            state.workSpace.remove(parameterConnection);
            break;
        case DELETE_PARAMETER_CONNECTION:
            const sourceNodeParam = state.workSpace.getFigure(infoItem.sourceNode);
            const targetNodeParam = state.workSpace.getFigure(infoItem.targetNode);
            let sourceParam;
            let targetParam;
            if (infoItem.sourcePort instanceof CPPParameterInputPort) {
                sourceParam = getParameterPort(infoItem.sourcePort.name, true, sourceNodeParam.parametersContainer);
                targetParam = getParameterPort(infoItem.targetPort.name, false, targetNodeParam.parametersContainer);
            }else{
                sourceParam = getParameterPort(infoItem.sourcePort.name, false, sourceNodeParam.parametersContainer);
                targetParam = getParameterPort(infoItem.targetPort.name, true, targetNodeParam.parametersContainer);
            }
            
            state.workSpace.add(
                new createConnection({ 
                    source: sourceParam,
                    target: targetParam,
                    onCreateParameterConnectionFunc: dispatchParameterConnectionCreation,
                    currentId: infoItem.id
                })
            );
            break;
        case PARAMETER_EDITION:
            let editedNode = state.workSpace.getFigures().data.filter(f => f.cppId === infoItem.nodeId)[0];
            let value = parametersSelector(editedNode).getInputPort(infoItem.parameterId).inputValue.text;
            parametersSelector(editedNode).getInputPort(infoItem.parameterId).createParameterInput(infoItem.value);
            dispatchParameterEdition(infoItem);
            eventTriggered(
                {
                    nodeId: infoItem.nodeId,
                    parameterId: infoItem.parameterId,
                    value,
                },
                PARAMETER_EDITION
            );
            break;
        case SHOW_PARAMETERS:
            handleHideConnections(state.workSpace.getFigure(infoItem));
            break;
        case HIDE_PARAMETERS:
            handleShowConnections(state.workSpace.getFigure(infoItem));
            break;
        case ADD_FILE_READER:
            let reader = state.workSpace.getFigures().data.filter(f => f.cppId === infoItem.nodeId)[0];
            let readerValue = parametersSelector(reader).getInputPort(infoItem.parameterId).inputValue.text;
            parametersSelector(reader).getInputPort(infoItem.parameterId).createParameterInput(infoItem.value.fileName);
            dispatchNodeFile(infoItem);
            eventTriggered(
                {
                    nodeId: infoItem.nodeId, 
                    parameterId: infoItem.parameterId,
                    value: {
                        fileId: infoItem.value.fileId,
                        fileName: readerValue,
                    }
                },
                ADD_FILE_READER
            );
            break;
        default:
            break;
    }
}