/**
 * File description: According to the type and the payload creates action types.
 * Author: Hellsoft
 */
export const success = (type, payload) => ({
    type: [type],
    payload
});

export const failure = (type, error) => ({
    type: [type],
    error
});