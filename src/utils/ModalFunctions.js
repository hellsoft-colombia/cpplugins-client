/**
 * File description: Util functions to handle the behaviour the files.
 * Author: Hellsoft
 */
export const onChangeFile = (file, sendFile, fetchUserImages, resetChargedFile) => {

    if (file !== null) {
        const dataName = file
            .name
            .split('.');
        let formData = new FormData();
        formData.append('file', file);
        formData.append('fileName', dataName[0]);
        formData.append('format', dataName[1]);
        formData.append('size', (file.size / 1048576).toFixed(3).toString());
        sendFile(formData, _ => 
            {
                fetchUserImages();
            }
        );
    }
}

export const deleteServersImage = (deleteFile, fileId, fetchUserImages, that) => {
    deleteFile(fileId).then(_ => 
        {
            that.setState({
                showDetails: null
            });
            fetchUserImages();
        }
    );
}