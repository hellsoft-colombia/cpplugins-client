/**
 * File description: Verifies if the user is using the application in a Linux system.
 * Author: Hellsoft
 */
export const showLinuxAlert = () => {
    if (navigator.userAgent.includes("Chrome") && navigator.appVersion.indexOf("Linux")!==-1 ) {
      alert("User other browser in order to have a better experience!"); 
    }
}