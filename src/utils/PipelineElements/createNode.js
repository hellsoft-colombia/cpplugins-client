/**
 * File description: Contains a function that creates a node according to the parameters.
 * Author: Hellsoft
 */
import incrementId from './incrementId';
import CPPNode from './Node/CPPNode';
import { ParameterFileNode } from './Parameter/ParameterFileNode';
import { READER_CLASS } from './../../constants/cpPlugins';

export const createNode = ( 
    { 
        node, 
        dispatchNodeInfo,
        handleSendPartialPipeline,
        dispatchNodeChangePosition,
        dispatchParameterEdition,
        dispatchParameterValueDelete,
        eventTriggered,
        openModalLoadImage,
        deleteFileImage,
        isCustomFilter,
        addInputCustomFilter,
        addOutputCustomFilter,
        deleteInputCustomFilter,
        deleteOutputCustomFilter,
    } ) => {
    let xPos;
    let yPos;
    let inputPortsNumber;
    let outputPortsNumber;
    
    let nodeId;
    if(node.cppId){
        nodeId = node.cppId;
    
    }
    else if(node.nodeId)
    {
        nodeId = node.nodeId;
    }
    else
    {
       nodeId = incrementId();
    }
    inputPortsNumber = node.inputs.length; 
    outputPortsNumber = node.outputs.length; 

    if((node.x && node.y)){
        xPos = node.x;
        yPos = node.y;
    }
    else{
        xPos = 100;
        yPos = 100;
    }

    let props = { 
        x: xPos, 
        y: yPos,
        height: 0,
        width: 100,
        bgColor: '#ffffff', 
        resizeable: false, 
        title: node.title,
        cppId: nodeId,
        d2dId: node.id,
        cppType: node.type,
        cppClass: node.nodeClass,  
        onClickFunc: dispatchNodeInfo,
        onRunFunc: handleSendPartialPipeline,
        onChangePosFunc: dispatchNodeChangePosition,
        onParameterEdition: dispatchParameterEdition,
        onParameterDelete: dispatchParameterValueDelete,
        addCommandStack: eventTriggered,
        parameters: node.parameters,
        pluginId: node.pipelineId,
        inputPortsNumber: inputPortsNumber,
        outputPortsNumber: outputPortsNumber,
        inputs: node.inputs,
        outputs: node.outputs,
        openModalLoadImage,
        deleteFileImage,
        isCustomFilter,
        addInputCustomFilter,
        addOutputCustomFilter,
        deleteInputCustomFilter,
        deleteOutputCustomFilter,
    }
    let newNode;
    if(node.nodeClass === READER_CLASS ){
        newNode = new ParameterFileNode(props);
    }
    else{
        newNode = new CPPNode(props);
    }

    return newNode;
}