/**
 * File description: Useful functions to init some variables or change others.
 * Author: Hellsoft
 */
let pipelineEnumeration = 0;

const incrementId = () => {
    pipelineEnumeration++;
    return pipelineEnumeration;
}

export const initId = () => {
    pipelineEnumeration = 0;
}

export default incrementId;
