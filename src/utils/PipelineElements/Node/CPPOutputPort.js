/**
 * File description: Contains object of an output port used in the creation of nodes.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPInputPort from './CPPInputPort';
import { outputCustomNodeMenu } from './CPPNodeFunctions';

const CPPOutputPort = draw2d.OutputPort.extend({
   init: function (attr) {
      this._super(attr)
      let { cppId, name, isCustomFilter, addOutputCustomFilter,
      deleteOutputCustomFilter } = attr;
      this.cppId = cppId;
      this.name = name;
      this.setRadius(8);
      this.addOutputCustomFilter = addOutputCustomFilter;
      this.deleteOutputCustomFilter = deleteOutputCustomFilter;
      if (isCustomFilter) {
         this.on("contextmenu", outputCustomNodeMenu.bind(this));
      }
   },
   createCommand: function (request) {
      if (request.getPolicy() === draw2d.command.CommandType.CONNECT) {

         if (request.source.getParent().getId() === request.target.getParent().getId()) {
            return null;
         }
         if (request.source instanceof CPPInputPort) {
            return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
         }
         return null;
      }
      return this._super(request);
   }
});

export default CPPOutputPort;