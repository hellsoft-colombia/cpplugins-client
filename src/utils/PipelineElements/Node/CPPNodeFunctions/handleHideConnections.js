/**
 * File description: Contains the function to handle hide connections within a node.
 * Author: Hellsoft
 */
import CPPParameterInputPort from '../../Parameter/CPPParameterInputPort';
import CPPParameterOutputPort from '../../Parameter/CPPParameterOutputPort';
import CPPParameterFakeInputPort from '../../Parameter/CPPParameterFakeInputPort';
import CPPParameterFakeOutputPort from '../../Parameter/CPPParameterFakeOutputPort';
import { createConnection, parametersSelector } from './../../index';
import { FAKE_PARAMETER } from './constants';
import { hasConnection } from './utils';

const handleInputs = (cppNode, canvas) => {
    let inputPorts = parametersSelector(cppNode).getInputPorts().data;
    for(let inputPort of inputPorts){
        if(inputPort.name !== FAKE_PARAMETER){
            inputPort.setVisible(false);
            if(inputPort.inputValue)
                inputPort.inputValue.setVisible(false);
            let connections = inputPort.getConnections().data;
            for(let connection of connections){
                if(connection.isVisible()){
                    if(connection.sourcePort instanceof CPPParameterOutputPort){
                        connection.setVisible(false);
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.sourcePort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.sourcePort,
                                    target: parametersSelector(cppNode).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                    }
                    else if(connection.targetPort instanceof CPPParameterOutputPort){
                        connection.setVisible(false);
                        connection.sourcePort.setVisible(false);
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.targetPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.targetPort,
                                    target: parametersSelector(cppNode).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                    }
                    else if(connection.sourcePort instanceof CPPParameterFakeOutputPort){
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.sourcePort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.sourcePort,
                                    target: parametersSelector(cppNode).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                        canvas.remove(connection);
                    }
                    else if(connection.targetPort instanceof CPPParameterFakeOutputPort){
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.targetPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.targetPort,
                                    target: parametersSelector(cppNode).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                        canvas.remove(connection);
                    }
                }
            }
        }
    }
}

const handleOutputs = (cppNode, canvas) => {
    let outputPorts = parametersSelector(cppNode).getOutputPorts().data;
    for(let outputPort of outputPorts){
        outputPort.setVisible(false);
        if(outputPort.name !== FAKE_PARAMETER){
            let connections = outputPort.getConnections().data;
            for(let connection of connections){
                if(connection.isVisible()){
                    if(connection.sourcePort !== outputPort && connection.sourcePort instanceof CPPParameterInputPort){
                        connection.setVisible(false);
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.sourcePort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.sourcePort,
                                    target: parametersSelector(cppNode).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                    }
                    else if(connection.targetPort !== outputPort && connection.targetPort instanceof CPPParameterInputPort){
                        connection.setVisible(false);
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.targetPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.targetPort,
                                    target: parametersSelector(cppNode).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                    }
                    else if(connection.sourcePort !== outputPort && connection.sourcePort instanceof CPPParameterFakeInputPort){
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.sourcePort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.sourcePort,
                                    target: parametersSelector(cppNode).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                        canvas.remove(connection);
                    }
                    else if(connection.targetPort !== outputPort && connection.targetPort instanceof CPPParameterFakeInputPort){
                        if(!hasConnection({
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.targetPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.targetPort,
                                    target: parametersSelector(cppNode).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                        }
                        canvas.remove(connection);
                    }
                }
            }
        }
    }
}

const handleHideConnections = (cppNode) => {
    let canvas = cppNode.getCanvas();
    handleInputs(cppNode, canvas);
    handleOutputs(cppNode, canvas); 
    parametersSelector(cppNode).staticCppInputPort.setVisible(true); 
    parametersSelector(cppNode).staticCppOutputPort.setVisible(true);
}

export default handleHideConnections;