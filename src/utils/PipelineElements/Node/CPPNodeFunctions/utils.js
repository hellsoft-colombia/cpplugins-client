/**
 * File description: Contains functions that helps node functions.
 * Author: Hellsoft
 */
import 'jquery.browser';
import $ from 'jquery';
import 'jquery-contextmenu';
import 'jquery-ui';
import 'jquery.ui.layout';

export const hasConnection = (params) => {
    let parameterNode = params.cppParametersNode;
    let connections = [];
    if(params.validateInput){
        connections = parameterNode.staticCppInputPort.getConnections().data;
    }
    else if(params.validateOutput){
        connections = parameterNode.staticCppOutputPort.getConnections().data;
    }
    let index = 0;
    while(index < connections.length){
        if(connections[index].sourcePort === params.port ||
            connections[index].targetPort === params.port ){
            return true;
        }
        index++;
    }
    return false;
}

export const getConnection = (params) => {
    let parameterNode = params.cppParametersNode;
    let connections = [];
    if(params.validateInput){
        connections = parameterNode.staticCppInputPort.getConnections().data;
    }
    else if(params.validateOutput){
        connections = parameterNode.staticCppOutputPort.getConnections().data;
    }
    let index = 0;
    while(index < connections.length){
        if(connections[index].sourcePort === params.port ||
            connections[index].targetPort === params.port ){
            return connections[index];
        }
        index++;
    }
    return null;
}

export const menu = function(emitter, event){
    $.contextMenu({
        selector: 'body',
        events:
        {
            hide: () => { $.contextMenu('destroy'); }
        },
        callback: (key, options) => {
            switch (key) {
                case "run":
                    this.onRunFunc(this.cppId);
                    break;
                case "delete":
                    let canvas = this.getCanvas();
                    let ports = this.getPorts();
                    for (let i = 0; i < ports.getSize(); i++) {
                        let connections = ports.get(i).getConnections();
                        for (let j = 0; j < connections.getSize(); j++) {
                            canvas.remove(connections.get(j));
                        }
                    }
                    canvas.remove(this);
                    break;
                default:
                    break;
            }

        },
        x: event.x,
        y: event.y,
        items:
        {
            "run": { name: "Run" },
            "sep1": "---------",
            "delete": { name: "Delete" }
        }
    });
}

export const inputCustomNodeMenu = function (emitter, event) {
    $.contextMenu({
        selector: 'body',
        events:
        {
            hide: () => { $.contextMenu('destroy'); }
        },
        callback: (key, options) => {
            switch (key) {
                case "setInput":
                    let input = {
                        name: this.getNode().title + '-' + this.name,
                        reference: { 
                            nodeId: this.getNode().cppId,
                            ioId: this.name,
                        }
                    };
                    this.setColor("#ff0000");
                    this.addInputCustomFilter(input);
                    break;
                case "quitInput":
                    this.setColor("#000000");
                    this.deleteInputCustomFilter(this.getNode().title + '-' + this.name);
                    break;
                default:
                    break;
            }

        },
        x: event.x,
        y: event.y,
        items:
        {
            "setInput": { name: "Set as input" },
            "sep1": "---------",
            "quitInput": { name: "Quit as input" }
        }
    });
}

export const outputCustomNodeMenu = function (emitter, event) {
    $.contextMenu({
        selector: 'body',
        events:
        {
            hide: () => { $.contextMenu('destroy'); }
        },
        callback: (key, options) => {
            switch (key) {
                case "setOutput":
                    let output = {
                        name: this.getNode().title + '-' + this.name,
                        reference: { 
                            nodeId: this.getNode().cppId,
                            ioId: this.name,
                        }
                    };
                    this.setColor("#ff0000");
                    this.addOutputCustomFilter(output);
                    break;
                case "quitOutput":
                    this.setColor("#000000");
                    this.deleteOutputCustomFilter(this.getNode().title + '-' + this.name);
                    break;
                default:
                    break;
            }

        },
        x: event.x,
        y: event.y,
        items:
        {
            "setOutput": { name: "Set as output" },
            "sep1": "---------",
            "quitOutput": { name: "Quit as output" }
        }
    });
}