/**
 * File description: Contains the function to handle show connections within a node.
 * Author: Hellsoft
 */
import CPPParameterInputPort from '../../Parameter/CPPParameterInputPort';
import CPPParameterOutputPort from '../../Parameter/CPPParameterOutputPort';
import { createConnection, parametersSelector } from './../../index';
import { FAKE_PARAMETER } from './constants';
import { getConnection, hasConnection } from './utils';

const handleInputs = (cppNode, canvas) => {
    let inputPorts = parametersSelector(cppNode).getInputPorts().data;
    for(let inputPort of inputPorts){
        if(inputPort.name !== FAKE_PARAMETER){
            inputPort.setVisible(true);
            if(inputPort.inputValue)
                inputPort.inputValue.setVisible(true);
            let connections = inputPort.getConnections().data;
            for(let connection of connections){
                if(connection.sourcePort instanceof CPPParameterOutputPort){
                    if(connection.sourcePort.getNode().isOpen){
                        connection.setVisible(true);
                        let oldConnection = getConnection({ 
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.sourcePort
                        });
                        if(oldConnection)
                            canvas.remove(oldConnection);
                    }
                    else{
                        if(!hasConnection({ 
                            cppParametersNode: parametersSelector(connection.sourcePort.getNode()),
                            validateInput: false,
                            port: connection.targetPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.targetPort,
                                    target: parametersSelector(connection.sourcePort.getNode()).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                            let oldConnection = getConnection({ 
                                cppParametersNode: parametersSelector(cppNode),
                                validateInput: true,
                                port: parametersSelector(connection.sourcePort.getNode()).staticCppOutputPort
                            });
                            if(oldConnection)
                            canvas.remove(oldConnection);
                        }
                    }
                }
                else if(connection.targetPort instanceof CPPParameterOutputPort){
                    if(connection.targetPort.getNode().isOpen){
                        connection.setVisible(true);
                        let oldConnection = getConnection({ 
                            cppParametersNode: parametersSelector(cppNode),
                            validateInput: true,
                            port: connection.targetPort
                        });
                        if(oldConnection)
                            canvas.remove(oldConnection);
                    }
                    else{
                        if(!hasConnection({ 
                            cppParametersNode: parametersSelector(connection.targetPort.getNode()),
                            validateInput: false,
                            port: connection.sourcePort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: connection.sourcePort,
                                    target: parametersSelector(connection.targetPort.getNode()).staticCppOutputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                            let oldConnection = getConnection({ 
                                cppParametersNode: parametersSelector(cppNode),
                                validateInput: true,
                                port: parametersSelector(connection.targetPort.getNode()).staticCppOutputPort
                            })
                            canvas.remove(oldConnection);
                        }
                    }
                }
            }
        }
    }
}

const handleOutputs = (cppNode, canvas) => {
    let outputPorts = parametersSelector(cppNode).getOutputPorts().data;
    for(let outputPort of outputPorts){
        if(outputPort.name !== FAKE_PARAMETER){
            outputPort.setVisible(true);
            let connections = outputPort.getConnections().data;
            for(let connection of connections){
                if(connection.sourcePort instanceof CPPParameterInputPort){
                    if(connection.sourcePort.getNode().isOpen){
                        connection.setVisible(true);
                        let oldConnection = getConnection({ 
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.sourcePort 
                        });
                        if(oldConnection)
                            canvas.remove(oldConnection);
                    }
                    else{
                        if(!hasConnection({ 
                            cppParametersNode: parametersSelector(connection.sourcePort.getNode()),
                            validateOutput: true,
                            port: outputPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: outputPort,
                                    target: parametersSelector(connection.sourcePort.getNode()).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                            let oldConnection = getConnection({ 
                                cppParametersNode: parametersSelector(cppNode),
                                validateOutput: true,
                                port: parametersSelector(connection.sourcePort.getNode()).staticCppInputPort
                            });
                            canvas.remove(oldConnection);
                        }
                    }
                }
                else if(connection.targetPort instanceof CPPParameterInputPort){
                    if(connection.targetPort.getNode().isOpen){
                        connection.setVisible(true);
                        let oldConnection = getConnection({ 
                            cppParametersNode: parametersSelector(cppNode),
                            validateOutput: true,
                            port: connection.targetPort
                        });
                        if(oldConnection)
                            canvas.remove(oldConnection);
                    }
                    else{
                        if(!hasConnection({ 
                            cppParametersNode: parametersSelector(connection.targetPort.getNode()),
                            validateOutput: true,
                            port: outputPort
                        })){
                            canvas.add(
                                new createConnection({ 
                                    source: outputPort,
                                    target: parametersSelector(connection.targetPort.getNode()).staticCppInputPort,
                                    isFake: true,
                                    selectable: false
                                })
                            );
                            let oldConnection = getConnection({ 
                                cppParametersNode: parametersSelector(cppNode),
                                validateOutput: true,
                                port: parametersSelector(connection.targetPort.getNode()).staticCppInputPort
                            });
                            if(oldConnection)
                                canvas.remove(oldConnection);
                        }
                    }
                }
            }
        }
    }
}

const handleShowConnections = (cppNode) => {
    let canvas = cppNode.getCanvas();
    handleInputs(cppNode, canvas);
    handleOutputs(cppNode, canvas);  
    parametersSelector(cppNode).staticCppInputPort.setVisible(false);
    parametersSelector(cppNode).staticCppOutputPort.setVisible(false); 
}

export default handleShowConnections;