/**
 * File description: Contains the function to create a single parameter within a node.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPMultiplexorInputPort from '../../Parameter/CPPMultiplexorInputPort';
import CPPMultiplexorOutputPort from '../../Parameter/CPPMultiplexorOutputPort';
import CPPParameterInputPort from '../../Parameter/CPPParameterInputPort';
import CPPParameterOutputPort from '../../Parameter/CPPParameterOutputPort';
import { PARAMETER_HEIGHT } from './constants';

const createParameters = (cppNode) => {
    //TODO: Change this
    if (!cppNode.parameters){
        cppNode.parametersContainer.setHeight(10);
        return; 
    }
    cppNode.parameters.forEach(parameter => {
        let inputParameter;
        let outputParameter;
        if(cppNode.cppClass === "Multiplexor"){
            inputParameter = new CPPMultiplexorInputPort({
                parameter: parameter,
                parent: cppNode,
                parentClass: cppNode.cppClass,
                onParameterEdition: cppNode.onParameterEdition,
                onParameterDelete: cppNode.onParameterDelete,
                addCommandStack: cppNode.addCommandStack,
            });
            outputParameter = new CPPMultiplexorOutputPort({
                parameter: parameter,
            });
        }
        else{
            inputParameter = new CPPParameterInputPort({
                parameter: parameter,
                parent: cppNode,
                parentClass: cppNode.cppClass,
                onParameterEdition: cppNode.onParameterEdition,
                onParameterDelete: cppNode.onParameterDelete,
                addCommandStack: cppNode.addCommandStack,
            });
            outputParameter = new CPPParameterOutputPort({
                parameter: parameter,
            });
        }
        inputParameter.getNode = () => (cppNode);
        cppNode.parametersContainer.addPort(
            inputParameter, 
            new draw2d.layout.locator.InputPortLocator()
        );
        outputParameter.getNode = () => (cppNode);   
        cppNode.parametersContainer.addPort(
            outputParameter, 
            new draw2d.layout.locator.OutputPortLocator()
        );  
    });
    if(cppNode.parameters.length === 0){
        cppNode.parametersContainer.setHeight(PARAMETER_HEIGHT);
    }
    else{
        cppNode.parametersContainer.setHeight(cppNode.parameters.length * PARAMETER_HEIGHT);
    }
}

export default createParameters;