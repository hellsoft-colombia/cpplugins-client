/**
 * File description: Contains functions that are useful to generate nodes.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPInputPort from './../CPPInputPort';
import CPPOutputPort from './../CPPOutputPort';
import CPPParameterFakeInputPort from '../../Parameter/CPPParameterFakeInputPort';
import CPPParameterFakeOutputPort from '../../Parameter/CPPParameterFakeOutputPort';
import { nodeSelector, parametersSelector } from './../../index';
import { FAKE_PARAMETER, PARAMETER_HEIGHT } from './constants';
import createParameters from './createParameters';
import handleHideConnections from './handleHideConnections';
import handleShowConnections from './handleShowConnections';
import { 
    menu, 
    inputCustomNodeMenu as _inputCustomNodeMenu, 
    outputCustomNodeMenu as _outputCustomNodeMenu 
} from './utils';
import { SHOW_PARAMETERS, HIDE_PARAMETERS } from '../../../../constants';

export const addNodeContainer = (cppNode) => {
    cppNode.nodeContainer = new draw2d.shape.basic.Rectangle({
        width: 100
    });
    for (let index = 0; index < cppNode.inputs.length; index++) {
        let newInputPort = new CPPInputPort({
            cppId: cppNode.inputs[index].id,
            name: cppNode.inputs[index].title,
            isCustomFilter: cppNode.isCustomFilter,
            addInputCustomFilter: cppNode.addInputCustomFilter,
            deleteInputCustomFilter: cppNode.deleteInputCustomFilter,
        });
        newInputPort.getNode = () => (cppNode);
        cppNode.nodeContainer.addPort(newInputPort, new draw2d.layout.locator.InputPortLocator());
    }
    for (let index = 0; index < cppNode.outputs.length; index++) {
        let newOutputPort = new CPPOutputPort({
            cppId: cppNode.outputs[index].id,
            name: cppNode.outputs[index].title,
            isCustomFilter: cppNode.isCustomFilter,
            addOutputCustomFilter: cppNode.addOutputCustomFilter,
            deleteOutputCustomFilter: cppNode.deleteOutputCustomFilter,
        });
        newOutputPort.getNode = () => (cppNode);
        cppNode.nodeContainer.addPort(newOutputPort, new draw2d.layout.locator.OutputPortLocator());
    }
    if(cppNode.inputs.length === 0 && cppNode.outputs.length === 0){
        cppNode.nodeContainer.setHeight(PARAMETER_HEIGHT);
    }
    else if(cppNode.inputs.length > cppNode.outputs.length){
        cppNode.nodeContainer.setHeight(cppNode.inputs.length*PARAMETER_HEIGHT);
    }
    else{
        cppNode.nodeContainer.setHeight(cppNode.outputs.length*PARAMETER_HEIGHT);
    }
    cppNode.add(cppNode.nodeContainer, new draw2d.layout.locator.TopLocator());
    let size = cppNode.nodeContainer.getHeight();
    let label = new draw2d.shape.basic.Label({
        text: cppNode.title,
        stroke: 0,
        bgColor: null,
        padding: size / 2,
        height: size,
        width: size,
        fontColor: "#4a4a4a",
    });
    label.on("contextmenu", menu.bind(cppNode));
    cppNode.nodeContainer.add(label, new draw2d.layout.locator.CenterLocator());
}

export const addParameterContainer = (cppNode) => {
    cppNode.parametersContainer = new draw2d.shape.basic.Rectangle({
        width: 100
    });
    parametersSelector(cppNode).staticCppInputPort 
        = new CPPParameterFakeInputPort({ parameter: FAKE_PARAMETER, notShowLabel: true });
    parametersSelector(cppNode).staticCppInputPort.setVisible(false);
    parametersSelector(cppNode).addPort(parametersSelector(cppNode).staticCppInputPort, 
        new draw2d.layout.locator.XYAbsPortLocator(0, 15));
    //------
    parametersSelector(cppNode).staticCppOutputPort 
        = new CPPParameterFakeOutputPort({ parameter: FAKE_PARAMETER });
    parametersSelector(cppNode).staticCppOutputPort.setVisible(false);
    parametersSelector(cppNode).addPort(parametersSelector(cppNode).staticCppOutputPort, 
        new draw2d.layout.locator.XYAbsPortLocator(100, 15));
    //------
    createParameters(cppNode);
    cppNode.parametersContainer.on("contextmenu", menu.bind(cppNode));
    cppNode.add(cppNode.parametersContainer, new draw2d.layout.locator.BottomLocator());
}

const hideParameters = (cppNode) => {
    cppNode.imgHide.setVisible(false);
    cppNode.imgShow.setVisible(true);
    cppNode.isOpen = false;
    handleHideConnections(cppNode);
    parametersSelector(cppNode).setHeight(PARAMETER_HEIGHT);
    parametersSelector(cppNode).repaint();
    cppNode.addCommandStack(cppNode.getId(), HIDE_PARAMETERS); 

}

const showParameters = (cppNode) => {
    cppNode.imgShow.setVisible(false);
    cppNode.imgHide.setVisible(true);
    cppNode.isOpen = true;
    handleShowConnections(cppNode);
    parametersSelector(cppNode).setHeight(cppNode.parameters.length*PARAMETER_HEIGHT);
    parametersSelector(cppNode).repaint();
    cppNode.addCommandStack(cppNode.getId(), SHOW_PARAMETERS); 
}

export const addParametersHandling = (cppNode) => {
    cppNode.imgHide = new draw2d.shape.icon.Contract({ minWidth: 20, minHeight: 20, width: 20, height: 20 });
    cppNode.imgShow = new draw2d.shape.icon.Expand({ minWidth: 20, minHeight: 20, width: 20, height: 20, visible: false});
    nodeSelector(cppNode).add(cppNode.imgHide, new draw2d.layout.locator.BottomLocator());
    nodeSelector(cppNode).add(cppNode.imgShow, new draw2d.layout.locator.BottomLocator());
    cppNode.imgShow.on("click", () => {
        showParameters(cppNode); 
    });
    cppNode.imgHide.on("click", () => { 
        hideParameters(cppNode); 
    });
}

export const _handleHideConnections = hideParameters;
export const _handleShowConnections = showParameters;
export const inputCustomNodeMenu = _inputCustomNodeMenu;
export const outputCustomNodeMenu = _outputCustomNodeMenu;