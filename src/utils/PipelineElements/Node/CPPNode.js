/**
 * File description: Contains the object of a node using to displayed in the canvas.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import {addNodeContainer, addParameterContainer, addParametersHandling} from './CPPNodeFunctions';

const CPPNode = draw2d
    .shape
    .basic
    .Rectangle
    .extend({
        init: function (attr) {
            this._super(attr);
            let {
                cppId,
                d2dId,
                cppType,
                cppClass,
                title,
                inputs,
                outputs,
                parameters,
                onRunFunc,
                onParameterEdition,
                onParameterDelete,
                onChangePosFunc,
                addCommandStack,
                isCustomFilter,
                addInputCustomFilter,
                addOutputCustomFilter,
                deleteInputCustomFilter,
                deleteOutputCustomFilter,
                pluginId
            } = attr;
            this.cppId = cppId;
            this.cppType = cppType;
            this.cppClass = cppClass;
            this.title = title;
            this.inputs = inputs;
            this.outputs = outputs;
            this.parameters = parameters;
            this.onRunFunc = onRunFunc;
            this.onChangePosFunc = onChangePosFunc;
            this.addCommandStack = addCommandStack;
            this.onParameterEdition = onParameterEdition;
            this.onParameterDelete = onParameterDelete;
            this.isOpen = true;
            this.setWidth(100);
            this.setHeight(20);
            this.setBackgroundColor("#80b0ff");
            this.isCustomFilter = isCustomFilter;
            this.addInputCustomFilter = addInputCustomFilter;
            this.addOutputCustomFilter = addOutputCustomFilter;
            this.deleteInputCustomFilter = deleteInputCustomFilter;
            this.deleteOutputCustomFilter = deleteOutputCustomFilter;

            //TODO: delete it
            if (d2dId !== undefined) {
                this.id = d2dId;
            }
            if (pluginId !== undefined) {
                this.pluginId = pluginId;
            }
            addNodeContainer(this);
            addParameterContainer(this);
            if (this.parameters) 
                addParametersHandling(this);
            }
        ,
        onDragStart(x, y, shiftKey, ctrlKey) {
            this._super();
            this.startXMousePos = this.getX();
            this.startYMousePos = this.getY();
        },
        onDragEnd(x, y, shiftKey, ctrlKey) {
            this._super();
            if (Math.abs(this.getX() - this.startXMousePos) > 5 || Math.abs(this.getY() - this.startYMousePos) > 5) {
                this.onChangePosFunc({
                    cppId: this.cppId,
                    id: this.getId(),
                    x: this.getX(),
                    y: this.getY()
                });
            }
        }
    });

export default CPPNode;