/**
 * File description: Contains object of an input port used in the creation of nodes.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPOutputPort from './CPPOutputPort';
import { inputCustomNodeMenu } from './CPPNodeFunctions';

const CPPInputPort = draw2d.InputPort.extend({
   init: function (attr) {
      this._super(attr);
      let { cppId, name, isCustomFilter, addInputCustomFilter,
      deleteInputCustomFilter } = attr;
      this.cppId = cppId;
      this.name = name;
      this.setRadius(8);
      this.addInputCustomFilter = addInputCustomFilter;
      this.deleteInputCustomFilter = deleteInputCustomFilter;
      if (isCustomFilter) {
         this.on("contextmenu", inputCustomNodeMenu.bind(this));
      }
   },
   createCommand: function (request) {
      if (request.getPolicy() === draw2d.command.CommandType.CONNECT) {

         if (request.source.getParent().getId() === request.target.getParent().getId()) {
            return null;
         }
         if (request.source instanceof CPPOutputPort) {
            return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
         }
         return null;
      }
      return this._super(request);
   }
});

export default CPPInputPort;