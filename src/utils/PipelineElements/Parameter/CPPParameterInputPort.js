/**
 * File description: Contains the object of an parameter input port displayed in a node.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPParameterOutputPort from './CPPParameterOutputPort';
import CPPMultiplexorOutputPort from './CPPMultiplexorOutputPort';
import { PARAMETER_EDITION } from '../../../constants';
import { READER_CLASS } from './../../../constants/cpPlugins';

const CPPParameterInputPort = draw2d.InputPort.extend({
    init: function (attr) {
        this._super(attr);
        this.setBackgroundColor('#ffff00');
        this.isParameterPort = true;
        this.isParameterInputPort = true;
        let{ parameter, onParameterEdition, onParameterDelete, notShowLabel, parent, parentClass, addCommandStack } = attr;
        this.name = parameter;
        this.setRadius(8);

        this.onParameterEdition = onParameterEdition;
        this.onParameterDelete = onParameterDelete;

        this.deleteConnections = () => {
            let canvas = this.getCanvas();
            let connections = this.getConnections().data;
            for(let con of connections){
                canvas.remove(con);
            }
        }

        this.changeParameterEvent = (_text) => {
            addCommandStack(
                {
                    nodeId: parent.cppId,
                    parameterId: parameter,
                    value: _text,
                },
                PARAMETER_EDITION
            );
        }

        this.createParameterInput = (_text) => {
            if(this.inputValue){
              this.remove(this.inputValue);
            }
            this.inputValue = new draw2d.shape.basic.Label({
                height: 0,
                width: 100,
                fontSize: 10,
                stroke: 1,
                bgColor: "#ffffff",
            });
            if(_text === this.name){
                this.inputValue.setFontColor("#a6a6a6");
                this.inputValue.setText(this.name);
            }
            else{
                this.inputValue.setFontColor("#303030");
                this.inputValue.setText(_text);
            }
            this.inputValue.setWidth(100);
            this.add(this.inputValue, new draw2d.layout.locator.XYAbsPortLocator(10,-10));
            if(parentClass !== READER_CLASS){
                this.inputValue.installEditor(new draw2d.ui.LabelInplaceEditor({
                    onCommit: (value) => {
                        if(value !== "" && value !== this.name){
                            this.onParameterEdition({
                                nodeId: this.getNode().cppId,
                                parameterId: parameter,
                                value: value
                            });
                            this.deleteConnections();
                        }
                        else{
                            //TODO: add to command stack
                            this.onParameterDelete({
                                nodeId: this.getNode().cppId,
                                parameterId: parameter
                            });
                            value = this.name;
                        }
                        if(value === this.name){
                            this.inputValue.setFontColor("#a6a6a6");
                        }
                        else{
                            this.inputValue.setFontColor("#303030");
                        }
                        this.inputValue.repaint();
                        this.changeParameterEvent(_text);
                    },
                    onCancel: function(){
                    }
                 }));
            }
            //this.changeParameterEvent(_text);
        }
        if(!notShowLabel)
            this.createParameterInput(parameter);
    },
    createCommand: function(request)
    {
       if(request.getPolicy() === draw2d.command.CommandType.CONNECT) {
        if(!request.target.getConnections().isEmpty()){
            return null;
        }
        if(request.source.getParent().getId() === request.target.getParent().getId()){
           return null;
        }    
        if (request.source instanceof CPPParameterOutputPort || request.source instanceof CPPMultiplexorOutputPort) {
           return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
        }         
        return null;
       } 
       return this._super(request);
    }
});

export default CPPParameterInputPort;
