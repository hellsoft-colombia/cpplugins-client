/**
 * File description: Contains object of an multiplexor input port used in the creation of nodes.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPMultiplexorOutputPort from './CPPMultiplexorOutputPort';
import CPPParameterOutputPort from './CPPParameterOutputPort';

const CPPMultiplexorInputPort = draw2d.InputPort.extend({
    init: function (attr) {
        this._super(attr);
        this.setBackgroundColor('#fffff0');
        this.isParameterPort = true;
        this.isParameterInputPort = true;
        let{ parameter, onParameterEdition } = attr;
        this.name = parameter;
        this.setRadius(8);

        this.onParameterEdition = onParameterEdition;
        
        this.createParameterInput = (_text) => {
            if(this.inputValue){
                this.remove(this.inputValue);
            }
            this.inputValue = new draw2d.shape.basic.Label({
                text: _text,
                height: 0,
                width: 45,
                fontColor: "#303030",
                fontSize: 10,
                stroke: 1,
                bgColor: "#ffffff",
            });
            this.add(this.inputValue, new draw2d.layout.locator.XYAbsPortLocator(10,-10));
            this.inputValue.installEditor(new draw2d.ui.LabelInplaceEditor({
                onCommit: (value) => {
                    this.onParameterEdition({
                        nodeId: this.getParent().getParent().getParent().cppId,
                        parameterId: parameter,
                        value: value
                    });
                },
                onCancel: function(){
                }
             }));
        }
        this.createParameterInput(parameter);
    },
    createCommand: function(request)
    {
       if(request.getPolicy() === draw2d.command.CommandType.CONNECT) {
        if(request.source.getParent().getId() === request.target.getParent().getId()){
           return null;
        }    
        if (request.source instanceof CPPMultiplexorOutputPort || request.source instanceof CPPParameterOutputPort) {
           return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
        }         
        return null;
       } 
       return this._super(request);
    }
});

export default CPPMultiplexorInputPort;
