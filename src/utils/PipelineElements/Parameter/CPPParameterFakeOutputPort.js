/**
 * File description: Contains object of an fake input port used hide output ports.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';

const CPPParameterFakeOutputPort = draw2d.OutputPort.extend({
    init: function (attr) {
        this._super(attr);
        this.setBackgroundColor('#ff5e6c');
        this.isParameterPort = true;
        this.isParameterInputPort = true;
        this.name = attr.parameter;
        this.setRadius(8);
        this.setDraggable(false);
    }
});

export default CPPParameterFakeOutputPort;