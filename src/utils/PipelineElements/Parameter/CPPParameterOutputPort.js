/**
 * File description: Contains the object of an parameter output port displayed in a node.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPParameterInputPort from './CPPParameterInputPort';
import CPPMultiplexorInputPort from './CPPMultiplexorInputPort';

const CPPParameterOutputPort = draw2d.OutputPort.extend({
    init: function (attr) {
        this._super(attr);
        this.setBackgroundColor('#ffff00');
        this.isParameterPort = true;
        let{ parameter } = attr;
        this.name = parameter;
        this.setRadius(8);
    },
    createCommand: function(request)
    {
       if(request.getPolicy() === draw2d.command.CommandType.CONNECT) {
        if(!request.source.getConnections().isEmpty()){
            return null;
        }
        if(request.source.getParent().getId() === request.target.getParent().getId()){
           return null;
        }    
        if (request.source instanceof CPPParameterInputPort || request.source instanceof CPPMultiplexorInputPort) {
           return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
        }         
        return null;
       } 
       return this._super(request);
    }
});

export default CPPParameterOutputPort;
