/**
 * File description: Contains the object of an parameter file used to upload files in a node.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPNode from './../Node/CPPNode';
import { nodeSelector } from '../index';

export const ParameterFileNode = CPPNode.extend({
    init: function(attr) {
        this._super(attr);
        this.cppType = "Pf";
        this.setBackgroundColor("#80b0ff");

        nodeSelector(this).remove(this.imgHide);
        nodeSelector(this).remove(this.imgShow);

        this.img = new draw2d.shape.icon.Folder(
            { 
                minWidth: 20, 
                minHeight: 20, 
                width: 10, 
                height: 10, 
                selectable: true 
            });

        this.img.on("click", () => {
            attr.openModalLoadImage({
                cppId: this.cppId,
                parameterId: this.parametersContainer.getInputPorts().data[1].name
            });
        });

        nodeSelector(this).add(this.img, new draw2d.layout.locator.BottomLocator());
    }
});