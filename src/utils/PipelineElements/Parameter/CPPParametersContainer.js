/**
 * File description: Contains the object of an parameter container that contains parameter ports.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPParameterInputPort from './CPPParameterInputPort';
import CPPParameterOutputPort from './CPPParameterOutputPort';

const CPPParametersContainer = draw2d.shape.basic.Rectangle.extend({
    init: function (attr) {
        this._super(attr);
        let { parameters, onParameterEdition } = attr;
        this.inputParameters = [];
        this.outputParameters = [];
        if(parameters.length === 0){
            this.setHeight(27);
        }
        else{
            this.setHeight(parameters.length*27);
        }
        for (let parameter of parameters) {
            this.inputParameters.push(
                this.addPort(
                    new CPPParameterInputPort({
                        parameter: parameter,
                        onParameterEdition: onParameterEdition
                    }), 
                    new draw2d.layout.locator.InputPortLocator()
                )
            );
            this.outputParameters.push(
                this.addPort(
                    new CPPParameterOutputPort({
                        parameter: parameter,
                    }), 
                    new draw2d.layout.locator.OutputPortLocator()
                )
            );       
        }
    }
});

export default CPPParametersContainer;