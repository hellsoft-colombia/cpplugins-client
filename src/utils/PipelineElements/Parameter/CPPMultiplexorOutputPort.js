/**
 * File description: Contains object of an multiplexor output port used in the creation of nodes.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import CPPMultiplexorInputPort from './CPPMultiplexorInputPort';
import CPPParameterInputPort from './CPPParameterInputPort';

const CPPMultiplexorOutputPort = draw2d.OutputPort.extend({
    init: function (attr) {
        this._super(attr);
        this.setBackgroundColor('#fffff0');
        this.isParameterPort = true;
        let{ parameter } = attr;
        this.name = parameter;
        this.setRadius(8);
    },
    createCommand: function(request)
    {
       if(request.getPolicy() === draw2d.command.CommandType.CONNECT) {
        if(request.source.getParent().getId() === request.target.getParent().getId()){
           return null;
        }    
        if (request.source instanceof CPPMultiplexorInputPort || request.source instanceof CPPParameterInputPort) {
           return new draw2d.command.CommandConnect(request.target, request.source, request.source, request.router);
        }         
        return null;
       } 
       return this._super(request);
    }
});

export default CPPMultiplexorOutputPort;
