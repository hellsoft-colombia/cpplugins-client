/**
 * File description: This file acts as an interface of different functions used by the application.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import { Canvas as _Canvas } from './Canvas';
import { createConnection as _createConnection } from './createConnection';
import { createNode as _createNode } from './createNode';
import { _handleHideConnections, _handleShowConnections } from './Node/CPPNodeFunctions';
import CPPParameterInputPort from './Parameter/CPPParameterInputPort';
import CPPParameterOutputPort from './Parameter/CPPParameterOutputPort';

export const Canvas = _Canvas;

export const createConnection = _createConnection;

export const createNode = _createNode;

export const JsonWritter = draw2d.io.json.Writer;

export const handleHideConnections = _handleHideConnections;

export const handleShowConnections = _handleShowConnections;

export const nodeSelector = (node) => {
    return node.nodeContainer;
};

export const parametersSelector = (node) => {
    return node.parametersContainer;
};

export const getParameterPort = (name, inputPort, parametersContainer) => {
    let typeFunc;
    if(inputPort){
        typeFunc = (parameter) => {
            if(parameter instanceof CPPParameterInputPort)
                return true;
            return false;
        }
    }
    else{
        typeFunc = (parameter) => {
            if(parameter instanceof CPPParameterOutputPort)
                return true;
            return false;
        }
    }
    let parameterPorts = parametersContainer.getPorts().data;
    let index = 0;
    while(index < parameterPorts.length){
        if(parameterPorts[index].name === name && typeFunc(parameterPorts[index])){
            return parameterPorts[index];
        }
        index++;
    }
    return null;
}
