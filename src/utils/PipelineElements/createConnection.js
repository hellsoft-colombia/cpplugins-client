/**
 * File description: Contains a function that creates a connection between nodes according to the parameters.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import 'jquery.browser';
import $ from 'jquery';
import 'jquery-contextmenu';
import 'jquery-ui';
import 'jquery.ui.layout';
import incrementId from './incrementId';
import CPPParameterInputPort from './Parameter/CPPParameterInputPort';
import { PARAMETER_EDITION } from '../../constants';

export const createConnection = draw2d.Connection.extend({
    init: function (attr, source, target) {
        this._super(attr, source, target);
        let that = this;
        let { onCreateConnectionFunc, 
            onCreateParameterConnectionFunc, 
            onDelParameterValue,
            pipelineConstructor, 
            isFake,
            currentId,
            addCommandStack } = attr;

        this.cppId = incrementId();
        this.pipelineConstructor = pipelineConstructor;
        this.isFake = isFake;

        let router = new draw2d.layout.connection.SplineConnectionRouter();
        router.abortRoutingOnFirstVertexNode = false;
        this.setRouter(router);
        this.setOutlineColor('#303030');
        this.setOutlineStroke(1);
        this.setColor('#00A8F0');
        this.setStroke(3);
        this.setRadius(2);

        if(currentId !== undefined){
            this.setId(currentId);
        }

        this.on("added", function (emitter, event){
            if(!emitter.isFake){
                if(emitter.sourcePort && emitter.sourcePort.isParameterPort){
                    emitter.setColor('#b9dd69');
                    if(emitter.sourcePort instanceof CPPParameterInputPort){
                        if(onDelParameterValue)
                            onDelParameterValue({
                                nodeId: emitter.sourcePort.getNode().cppId,
                              parameterId: emitter.sourcePort.name
                            });
                            if(addCommandStack)
                                addCommandStack(
                                    {
                                        nodeId: emitter.sourcePort.getNode().cppId,
                                        parameterId: emitter.sourcePort.name,
                                        value: emitter.sourcePort.inputValue.getText(),
                                    }
                                , PARAMETER_EDITION);
                        emitter.sourcePort.createParameterInput(emitter.sourcePort.name);
                    }
                    else if(emitter.targetPort instanceof CPPParameterInputPort){
                        if(onDelParameterValue)
                            onDelParameterValue({
                                nodeId: emitter.targetPort.getNode().cppId,
                                parameterId: emitter.targetPort.name
                            });
                        //HEREEEE
                        if(addCommandStack)
                            addCommandStack(
                                {
                                    nodeId: emitter.targetPort.getNode().cppId,
                                    parameterId: emitter.targetPort.name,
                                    value: emitter.targetPort.inputValue.getText(),
                                }
                            , PARAMETER_EDITION);
                            emitter.targetPort.createParameterInput(emitter.targetPort.name);
                        
                    }
                    onCreateParameterConnectionFunc(emitter);
                }
                else{
                    emitter.setColor('#00A8F0');
                    onCreateConnectionFunc(emitter);
                }
            }
            else if(emitter.isFake){
                emitter.setColor('#eb4034');
            }
        });

        if (source) {
            this.setSource(source);
            this.setTarget(target);
        }

        this.on("contextmenu", function(emitter, event){
            $.contextMenu({
                selector: 'body',
                events:
                {
                    hide: function () { $.contextMenu('destroy'); }
                },
                callback: function (key, options) {
                    switch (key) {
                        case "red":
                            this.setColor('#f3546a');
                            break;
                        case "green":
                            this.setColor('#b9dd69');
                            break;
                        case "blue":
                            this.setColor('#00A8F0');
                            break;
                        case "delete":
                            this.getCanvas().remove(this);
                            break;
                        default:
                            break;
                    }
    
                }.bind(that),
                x: event.x,
                y: event.y,
                items:
                {
                    "red": { name: "Red" },
                    "green": { name: "Green" },
                    "blue": { name: "Blue" },
                    "sep1": "---------",
                    "delete": { name: "Delete" }
                }
            });
        });


    }

});