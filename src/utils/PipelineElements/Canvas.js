/**
 * File description: Contains the object of the canvas that is displayed in the design area,
 * where the user design his pipelines.
 * Author: Hellsoft
 */
import draw2d from 'draw2d';
import { createConnection } from './createConnection';
import CPPNode from './Node/CPPNode';

export const Canvas = draw2d.Canvas.extend({
    init: function (attr) {

        let { canvasId, onCreateConnectionFunc, onCreateParameterConnectionFunc,
            onDelConnectionFunc, onDelParameterConnectionFunc, onDelParameterValue, onDelNodeFunc,
            addCommandStack } = attr;
        this._super(canvasId);

        this.installEditPolicy(new draw2d.policy.canvas.BoundingboxSelectionPolicy());
        this.installEditPolicy(new draw2d.policy.canvas.ShowGridEditPolicy());
        this.installEditPolicy(new draw2d.policy.canvas.SnapToGeometryEditPolicy());
        this.installEditPolicy(new draw2d.policy.canvas.SnapToInBetweenEditPolicy());
        this.installEditPolicy(new draw2d.policy.canvas.SnapToCenterEditPolicy());
        this.installEditPolicy(new draw2d.policy.connection.DragConnectionCreatePolicy({
            createConnection: function () {
                return new createConnection({
                    onCreateConnectionFunc: onCreateConnectionFunc,
                    onCreateParameterConnectionFunc: onCreateParameterConnectionFunc,
                    onDelParameterValue: onDelParameterValue,
                    addCommandStack: addCommandStack
                });
            }

        }));

        /*
        this.setDimension({
            height: 50000,
            width: 50000
        });
        */
        this.setScrollArea("#" + canvasId);
        this.on("figure:remove", function (emitter, event) {
            if (event.figure instanceof CPPNode) {
                onDelNodeFunc(event.figure);
            }
            else {
                if (!event.figure.isFake) {
                    if (event.figure.sourcePort.isParameterPort) {
                        onDelParameterConnectionFunc(event.figure);
                    }
                    else {
                        onDelConnectionFunc(event.figure);
                    }
                }
            }
        });

    },
    layout: function () {
        this.appLayout.resizeAll();
    },
    fromDocumentToCanvasCoordinate: function (x, y) {
        return new draw2d.geo.Point(
            (x + window.pageXOffset - this.getAbsoluteX() + this.getScrollLeft()) * this.zoomFactor,
            (y + window.pageYOffset - this.getAbsoluteY() + this.getScrollTop()) * this.zoomFactor);
    },
    
});