/**
 * File description: Util functions to handle the filters applied in the application.
 * Author: Hellsoft
 */
export const filterSearchCryteria = [{ id: 0, criteria: 'Name'}, { id: 1, criteria: 'Date'}, { id: 2, criteria: 'State'}];

export const filterOrderCryteria = [{id: 0, order: 'Asc'}, {id: 1, order: 'Desc'}];

export const modCalc = number => (
    (number+ 1) % filterSearchCryteria.length
);

export const filterCryteria = (arr, state) => (
    arr.filter(x => x.id === state)[0]
);