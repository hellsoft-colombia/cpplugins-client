/**
 * File description: Util functions to handle the fields of the forms using in the application.
 * Author: Hellsoft
 */
const validate = (value, rules, name) => {
    let isValid = true;
    let errors = "";
    let validator = true;

    for (let rule in rules) {
        switch (rule) {
            case 'minLength':
                validator = minLengthValidator(value, rules[rule]);
                isValid = isValid && validator;
                errors = errors.concat(!validator
                    ? "Minimum length is " + rules[rule] + ", "
                    : "");
                break;
            case 'isRequired':
                validator = requiredValidator(value);
                isValid = isValid && validator;
                errors = errors.concat(!validator
                    ? "This field is required "
                    : "");
                break;
            case 'regex':
                validator = regexValidator(value, rules[rule]);
                isValid = isValid && validator;
                if (name === "password") {
                    errors = errors.concat(!validator
                        ? "This field must have 8 characters, 1 Mayus, 1 minus and 1 number, "
                        : "");
                } else {
                    errors = errors.concat(!validator
                        ? "This field is not a valid email of the form email@example.com, "
                        : "");
                }
                break;
            default:
                isValid = true;
        }
    }

    return {isValid, errors};
}

const minLengthValidator = (value, minLength) => (value.length >= minLength)

const requiredValidator = value => (value.trim() !== "")

const regexValidator = (value, pattern) => (pattern.test(value))

export default validate;