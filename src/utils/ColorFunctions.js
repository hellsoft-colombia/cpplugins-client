/**
 * File description: Funcion that generates a random color.
 * Author: Hellsoft
 */
export const generateRandomColor = _ => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
