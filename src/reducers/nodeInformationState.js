/**
 * File description: Contains the reducers of node information that are executing according the actions and the state.
 * Author: Hellsoft
 */
import { handleActions } from 'redux-actions';
import { ADD_NODE_INFORMATION } from '../constants';

export const nodeInformationState = handleActions(
    {
        [ADD_NODE_INFORMATION]: (state, action) => ({
            ...state,
            nodeInformation: action.payload
        }),
    },
    []
);