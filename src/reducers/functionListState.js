/**
 * File description: Contains the reducers of plugins that are executing according the actions and the state.
 * Author: Hellsoft
 */
import { handleActions } from 'redux-actions';
import { FETCH_FUNCTION_LIST, SET_FUNCTION_LIST } from '../constants';

export const functionListState = handleActions(
    {
        [FETCH_FUNCTION_LIST]: (state, action) => ({
            isFilled : true, 
            functionList: action.payload.functionList
            //functionList : [...action.payload.functionList, ...state.functionList]
        }),
        [SET_FUNCTION_LIST]: (state, action) => ({
            functionList : action.payload
        }),
    },
    []
);