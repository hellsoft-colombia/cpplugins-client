/**
 * File description: Contains the reducers of node state that are executing according the actions and the state.
 * Author: Hellsoft
 */
import { handleActions } from 'redux-actions';
import { SET_NODE } from '../constants';

export const nodesState = handleActions(
    {
        [SET_NODE]: (state, action) => ({
            selectedNode: action.payload
        }),
    },
    []
);