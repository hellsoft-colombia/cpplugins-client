/**
 * File description: Contains the reducers of the command stack that are executing according the actions and the state.
 * Author: Hellsoft
 */
import { handleActions } from 'redux-actions';
import { ADD_UNDO_COMMAND_STACK, ADD_REDO_COMMAND_STACK, MAX_SIZE_STACK, CLEAR_STACK } from '../constants';

export const commandStackState = handleActions(
    {
        [ADD_UNDO_COMMAND_STACK]: (state, action) => {
            if (state.undoCommandStackState.commands.length > MAX_SIZE_STACK) {
                state.undoCommandStackState.commands.shift();                                 
            }
            return{
                ...state,
                undoCommandStackState: {
                    ...state.undoCommandStackState,
                    commands: [
                        ...state.undoCommandStackState.commands, action.payload
                    ]                      
                }
            } 
        },
        [ADD_REDO_COMMAND_STACK]: (state, action) => {
            if (state.redoCommandStackState.commands.length > MAX_SIZE_STACK) {
                state.redoCommandStackState.commands.shift();             
            }
            return{
                ...state,
                redoCommandStackState: {
                    ...state.redoCommandStackState,
                    commands: [
                        ...state.redoCommandStackState.commands, action.payload
                    ]                      
                }
            }  
        },
        [CLEAR_STACK]: (state, action) => (
            {
                undoCommandStackState: {
                    commands: [],
                },
                redoCommandStackState: {
                    commands: [],
                }
            }
        ),
    },
    []
);