/**
 * File description: Contains the reducers of authentication that are executing according the actions and the state.
 * Author: Hellsoft
 */
import {handleActions} from 'redux-actions';
import {USER_LOGIN, USER_LOGIN_FAILURE, USER_INFO, USER_LOGOUT, USER_INFO_FAILURE} from '../constants';

const resetDefaultState = {
    user: {
        firstName: "",
        lastName: "",
        userName: "",
        email: ""
    },
    isAuthenticated: false
};

export const authenticatedUserState = handleActions({
    [USER_LOGIN]: (state, action) => ({
        ...state,
        isAuthenticated: true
    }),
    [USER_LOGIN_FAILURE]: (state, action) => ({
        ...resetDefaultState
    }),
    [USER_INFO]: (state, action) => ({
        ...state,
        user: action.payload
    }),
    [USER_INFO_FAILURE]: (state, action) => ({
        ...resetDefaultState
    }),
    [USER_LOGOUT]: (state, action) => ({
        ...resetDefaultState
    })
}, []); 
