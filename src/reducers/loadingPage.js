/**
 * File description: Contains the reducers of loading page that are executing according the actions and the state.
 * Author: Hellsoft
 */
import {handleActions} from 'redux-actions';
import {CHANGE_IS_LOADING_PAGE} from '../constants';

export const isLoadingPage = handleActions({
    [CHANGE_IS_LOADING_PAGE]: (state, action) => (
        action.payload
    )
}, []);