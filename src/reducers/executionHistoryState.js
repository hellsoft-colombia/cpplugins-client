/**
 * File description: Contains the reducers of execution history that are executing according the actions and the state.
 * Author: Hellsoft
 */
import {handleActions} from 'redux-actions';
import {
    CHANGE_HAS_MORE_STATE,
    INCREASE_CURRENT_PAGE,
    FETCH_USER_EXECUTION_HISTORY,
    FETCH_ACTIVE_PIPELINES_ENTRIES,
    FETCH_FINISHED_PIPELINES,
    HISTORY_FAILURE,
    RESET_HISTORY_LIST,
    UPDATE_IS_LOADING
} from '../constants';

export const executionHistoryState = handleActions({
    [CHANGE_HAS_MORE_STATE]: (state, action) => ({
        ...state,
        hasMore: action.payload
    }),
    [INCREASE_CURRENT_PAGE]: (state, action) => ({
        ...state,
        currentPage: state.currentPage + 1
    }),
    [FETCH_USER_EXECUTION_HISTORY]: (state, action) => ({
        ...state,
        historyList: [
            ...state.historyList,
            ...action.payload
        ],
        isLoading: false
    }),
    [FETCH_ACTIVE_PIPELINES_ENTRIES]: (state, action) => ({
        ...state,
        historyList: action.payload,
        isLoading: false
    }),
    [FETCH_FINISHED_PIPELINES]: (state, action) => ({
        ...state,
        historyList: action.payload,
        isLoading: false
    }),
    [HISTORY_FAILURE]: (state, action) => ({
        ...state,
        error: action.error,
        isLoading: false
    }),
    [RESET_HISTORY_LIST]: (state, action) => ({
        hasMore: true, 
        historyList: [],
        currentPage: 0,
        error: "",
        isLoading: true
    }),
    [UPDATE_IS_LOADING]: (state, action) => ({
        ...state,
        isLoading: action.payload,
    }),
}, []);