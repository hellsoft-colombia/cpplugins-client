/**
 * File description: Contains the reducers of manage pipelines that are executing according the actions and the state.
 * Author: Hellsoft
 */
import {handleActions} from 'redux-actions';
import {
    SEND_PIPELINE,
    SET_EXCECUTION_NODE_ID,
    CHANGE_NODE_POSITION,
    ADD_PIPELINE_ID,
    ADD_PIPELINE_TIME_STAMP,
    ADD_DESCRIPTOR_NODE,
    SET_ACTIVE_PIPELINE,
    ADD_DESCRIPTOR_CONNECTION,
    ADD_DESCRIPTOR_PARAMETER_VALUE,
    ADD_DESCRIPTOR_PARAMETER_CONNECTION,
    DELETE_DESCRIPTOR_CONNECTION,
    DELETE_DESCRIPTOR_NODE,
    DELETE_DESCRIPTOR_PARAMETER_CONNECTION,
    FETCH_SPECIFIC_PIPELINE,
    CLEAN_LOCAL_STORAGE_PIPELINE,
    SET_PREV_PIPELINE,
    CLEAN_PREV_PIPELINE,
    ADD_FILE_NODE,
    STORE_PIPELINE_FAILURE,
    RESET_ERRORS,
    EDIT_CUSTOM_PLUGIN_FAILURE,
    CREATE_CUSTOM_NODE_FAILURE,
    EDIT_PIPELINE_FAILURE,
    SET_DESCRIPTOR_CUSTOM_FILTER,
    CLEAR_DESCRIPTOR_CUSTOM_FILTER,
    IS_CUSTOM_FILTER,
    CLEAR_ACTIVE_PIPELINE,
    SET_ACTIVE_PIPELINE_INFO,
    SAVE_INPUTS_OUTPUTS,
} from '../constants';
import { filter,  } from 'lodash';

export const pipelinesState = handleActions({
    [CLEAR_ACTIVE_PIPELINE]: (state, action) => ({
        ...state, 
        activePipeline: {
            pipelineName: "",
            executionNodeId: -1,
            pipelineId: "",
            timeStamp: 0,
            nodes: [],
            connections: [],
            parameters: []
        },
        storedPipeline: {
            pipelineName: "",
            executionNodeId: -1,
            pipelineId: "",
            timeStamp: 0,
            nodes: [],
            connections: [],
            parameters: []
        }
    }),
    [SEND_PIPELINE]: (state, action) => ({
        ...state,
        sentPipeline: action.payload
    }),
    [SET_EXCECUTION_NODE_ID]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            executionNodeId: action.payload
        }
    }),
    [SET_ACTIVE_PIPELINE]: (state, action) => {
        return {
            ...state,
            activePipeline: {
                pipelineName: "",
                executionNodeId: -1,
                pipelineId: "",
                timeStamp: 0,
                nodes: [],
                connections: [],
                parameters: []
            },
            storedPipeline: action.payload
        }
    },
    [SET_ACTIVE_PIPELINE_INFO]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            pipelineId: action.payload.pipelineId,
            pipelineName: action.payload.pipelineName,
            executionNodeId: action.payload.executionNodeId,
            timeStamp: action.payload.timeStamp,
        }
    }),
    [CLEAN_LOCAL_STORAGE_PIPELINE]: (state, action) => ({
        ...state,
        storedPipeline: {
            executionNodeId: -1,
            pipelineId: "",
            timeStamp: 0,
            nodes: [],
            connections: []
        }
    }),
    [ADD_PIPELINE_ID]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            pipelineId: action.payload
        }
    }),
    [ADD_PIPELINE_TIME_STAMP]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            timeStamp: action.payload
        }
    }),
    [ADD_DESCRIPTOR_NODE]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            nodes: [
                ...state.activePipeline.nodes,
                action.payload
            ]
        }
    }),
    [ADD_DESCRIPTOR_CONNECTION]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            connections: [
                ...state.activePipeline.connections,
                action.payload
            ]
        }
    }),
    [IS_CUSTOM_FILTER]: (state, action) => ({
        ...state,
        isCustomFilter: action.payload
    }),
    [ADD_DESCRIPTOR_PARAMETER_VALUE]: (state, action) => {
        let newParameter = action.payload;
        let _parameters = state.activePipeline.parameters;
        let oldParameter = _parameters.find((_parameter) => (_parameter.nodeId === newParameter.nodeId && _parameter.parameterId === newParameter.parameterId));
        if (!oldParameter) {
            _parameters = [
                ..._parameters,
                newParameter
            ];
        } else {
            oldParameter.value = newParameter.value;
        }
        return {
            ...state,
            activePipeline: {
                ...state.activePipeline,
                parameters: _parameters
            }
        }
    },
    [ADD_DESCRIPTOR_PARAMETER_CONNECTION]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            parameters: [
                ...state.activePipeline.parameters,
                action.payload
            ]
        }
    }),
    [DELETE_DESCRIPTOR_PARAMETER_CONNECTION]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            parameters: state
                .activePipeline
                .parameters
                .filter(parameter => !(parameter.nodeId === action.payload.nodeId && parameter.parameterId === action.payload.parameterId))
        }
    }),
    [DELETE_DESCRIPTOR_CONNECTION]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            connections: state
                .activePipeline
                .connections
                .filter(connection => connection.connectionId !== action.payload.connectionId)
        }
    }),
    [DELETE_DESCRIPTOR_NODE]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            nodes: state
                .activePipeline
                .nodes
                .filter(node => node.nodeId !== action.payload.nodeId)
        }
    }),
    [CHANGE_NODE_POSITION]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            nodes: state
                .activePipeline
                .nodes
                .map((item, index) => {
                    if (item.nodeId === action.payload.nodeId) {
                        return {
                            ...item,
                            x: action.payload.x,
                            y: action.payload.y
                        }
                    } else {
                        return item;
                    }
                })
        }
    }),
    [FETCH_SPECIFIC_PIPELINE]: (state, action) => ({
        ...state,
        activePipeline: {
            ...state.activePipeline,
            pipelineId: action.payload.pipelineId
        },
        storedPipeline: action.payload
    }),
    [SET_PREV_PIPELINE]: (state, action) => ({
        ...state,
        prevPipeline: action.payload
    }),
    [CLEAN_PREV_PIPELINE]: (state, action) => ({
        ...state,
        prevPipeline: null
    }),
    [ADD_FILE_NODE]: (state, action) => {
        if (state.activePipeline.parameters.length === 0) {
            return {
                ...state,
                activePipeline: {
                    ...state.activePipeline,
                    parameters: [
                        ...state.activePipeline.parameters,
                        action.payload
                    ]
                }
            }
        } else {
            let newParams = filter(state.activePipeline.parameters, p => p.nodeId !== action.payload.nodeId);
            if (newParams.length === 0) {
                newParams = [];
            }
            newParams.push(action.payload);
            return {
                ...state,
                activePipeline: {
                    ...state.activePipeline,
                    parameters: newParams
                }
            }
        }
    },
    [STORE_PIPELINE_FAILURE]: (state, action) => ({
        ...state,
        errors: action.error
    }),
    [EDIT_PIPELINE_FAILURE]: (state, action) => ({
        ...state,
        errors: action.error
    }),
    [CREATE_CUSTOM_NODE_FAILURE]: (state, action) => ({
        ...state,
        errors: action.error
    }),
    [EDIT_CUSTOM_PLUGIN_FAILURE]: (state, action) => ({
        ...state,
        errors: action.error
    }),
    [RESET_ERRORS]: (state, action) => ({
        ...state,
        errors: ""
    }),
    [SET_DESCRIPTOR_CUSTOM_FILTER]: (state, action) => ({
        ...state,
        actualCustomFilter: {
            ...state.actualCustomFilter,
            descriptor: action.payload
        }
    }),
    [CLEAR_DESCRIPTOR_CUSTOM_FILTER]: (state, action) => ({
        ...state,
        actualCustomFilter: {
            ...state.actualCustomFilter,
            descriptor: {}
        }
    }),
    [SAVE_INPUTS_OUTPUTS]: (state, action) => ({
        ...state,
        activePipeline : {
            ...state.activePipeline,
            inputs: action.payload.inputs,
            outputs: action.payload.outputs
        }
    }),
}, []);
