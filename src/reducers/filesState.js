/**
 * File description: Contains the reducers of files that are executing according the actions and the state.
 * Author: Hellsoft
 */
import {handleActions} from 'redux-actions';
import {sortElementsAsc, sortElementsDsc} from '../utils/SortingFunctions';
import {
    SET_SELECTED_FILE,
    SET_FILE_LIST,
    SET_PIPELINE_LIST,
    FETCH_PIPELINE_LIST,
    FETCH_NOTIFICATION_PIPELINES,
    SORT_CURRENT_LIST,
    CLEAN_SELECTED_FILE,
    FETCH_FILE_LIST,
    ADD_INPUT_CUSTOM_FILTER,
    DELETE_INPUT_CUSTOM_FILTER,
    ADD_OUTPUT_CUSTOM_FILTER,
    DELETE_OUTPUT_CUSTOM_FILTER
} from '../constants';
import { filter, find } from 'lodash';

const defaultSelectedFile = {
    creationDate: "",
    executionState: "",
    fileId: "",
    fileName: "",
    lastModificationDate: ""
}

export const filesState = handleActions({
    [SET_SELECTED_FILE]: (state, action) => ({
        ...state,
        selectedFile: action.payload
    }),
    [CLEAN_SELECTED_FILE]: (state, action) => ({
        ...state,
        selectedFile: defaultSelectedFile
    }),
    [SET_FILE_LIST]: (state, action) => ({
        ...state,
        fileList: action.payload
    }),
    [FETCH_FILE_LIST]: (state, action) => ({
        ...state,
        fileList: action.payload
    }),
    [SET_PIPELINE_LIST]: (state, action) => ({
        ...state,
        pipelineList: action.payload
    }),
    [FETCH_PIPELINE_LIST]: (state, action) => ({
        ...state,
        pipelineList: action.payload
    }),
    [FETCH_NOTIFICATION_PIPELINES]: (state, action) => ({
        ...state,
        notifications: action.payload
    }),
    [SORT_CURRENT_LIST]: (state, action) => {
        let sortedPipeline = state.pipelineList;
        let {order, cryteria} = action.payload;

        if (!order) {
            sortElementsAsc(sortedPipeline, cryteria);
        } else {
            sortElementsDsc(sortedPipeline, cryteria);
        }

        return {
            pipelineList: sortedPipeline,
            ...state
        }
    },
    [ADD_INPUT_CUSTOM_FILTER]: (state, action) => {
        if (find(state.selectedFile.inputs, action.payload) === undefined) {
            return {
                ...state,
                selectedFile: {
                    ...state.selectedFile,
                    inputs: [
                        ...state.selectedFile.inputs,
                        action.payload
                    ]
                }
            }
        } else {
            return {
                ...state
            }
        }
    },
    [DELETE_INPUT_CUSTOM_FILTER]: (state, action) => {
        let filterInputs = filter(state.selectedFile.inputs, e => e.name !== action.payload);
        return {
            ...state,
            selectedFile: {
                ...state.selectedFile,
                inputs: filterInputs
            }
        }
    },
    [ADD_OUTPUT_CUSTOM_FILTER]: (state, action) => {
        if (find(state.selectedFile.outputs, action.payload) === undefined) {
            return {
                ...state,
                selectedFile: {
                    ...state.selectedFile,
                    outputs: [
                        ...state.selectedFile.outputs,
                        action.payload
                    ]
                }
            }
        } else {
            return {
                ...state
            }
        }
    },
    [DELETE_OUTPUT_CUSTOM_FILTER]: (state, action) => {
        let filterOutputs = filter(state.selectedFile.outputs, e => e.name !== action.payload);
        return {
            ...state,
            selectedFile: {
                ...state.selectedFile,
                outputs: filterOutputs
            }
        }
    }
}, []);