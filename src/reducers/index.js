/**
 * File description: Constains the localization of the reducers within the project.
 * Author: Hellsoft
 */
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { nodesState } from './nodesState';
import { pipelinesState } from './pipelinesState';
import { functionListState } from './functionListState';
import { commandStackState } from './commandStackState';
import { filesState } from './filesState';
import { nodeInformationState } from './nodeInformationState';
import { authenticatedUserState } from './authenticatedUserState';
import { executionHistoryState } from './executionHistoryState';
import { isLoadingPage } from './loadingPage';

const persistConfig = {
    key: 'pipelinesState',
    storage,
    whitelist: [
        'storedPipeline', 
    ]
}

const persistedReducer = persistReducer(persistConfig, pipelinesState);

export default combineReducers({
    nodesState,
    pipelinesState: persistedReducer,
    functionListState,
    commandStackState,
    filesState,
    nodeInformationState,
    authenticatedUserState,
    executionHistoryState,
    isLoadingPage
});