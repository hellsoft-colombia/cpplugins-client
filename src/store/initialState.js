/**
 * File description: Contains the variable of the state when the application is init for the
 * first time.
 * Author: Hellsoft
 */
export const initialState = {
    isLoadingPage: false, 
    nodesState: {
        selectedNode: {
            nodeId: "Select one node",
            nodeInputs: [],
            nodeOutputs: [],
            fileParameters: []
        }
    },
    pipelinesState: {
        errors: "",
        isCustomFilter: false,
        activePipeline: {
            pipelineName: "",
            executionNodeId: -1,
            pipelineId: "",
            timeStamp: 0,
            nodes: [],
            connections: [],
            parameters: []
        },
        sentPipeline: {},
        storedPipeline: {
            pipelineName: "",
            executionNodeId: -1,
            pipelineId: "",
            timeStamp: 0,
            nodes: [],
            connections: [],
            parameters: []
        }
    },
    functionListState: {
        isFilled: false, 
        functionList : [
            {
                title: "Parameters",
                subtitle: "Paquete de parámetros",
                isDirectory: true,
                children: [
                    {
                        title: "Parameter multiplexor",
                        subtitle: "Parámetro",
                        inputs: [],
                        outputs: [],
                        nodeClass: "Multiplexor",
                        parameters: ["multiplexor"],
                        type: "Pm",
                    }
                ]
            }
        ]
    },
    commandStackState: {
        undoCommandStackState: {
            commands: [],
        },
        redoCommandStackState: {
            commands: [],
        },
    },    
    filesState: {
        fileList: [],
        notifications: [],
        selectedFile: {
            creationDate: "",
            executionState: "",
            fileId: "",
            fileName: "",
            description: "",    
            lastModificationDate: "",
            type: "",
            inputs: [],
            outputs: [],
        },
    },
    nodeInformationState:{
        nodeInformation: "",
    },
    executionHistoryState:{
        historyList: [],
        hasMore: true,
        currentPage: 0,
        error: "",
        isLoading: true,
    },
    authenticatedUserState: {
        user: {
            firstName: "",
            lastName: "",
            userName : "",
            email: "",
        },
        isAuthenticated : false,
    },
}