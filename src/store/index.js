/**
 * File description: Contains the configuration and the centralized state of the application.
 * Author: Hellsoft
 */
import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import promiseMiddleware from 'redux-promise';
import reducers from './../reducers';
import thunk from 'redux-thunk';
import { initialState } from './initialState';

const persistConfig = {
    key: 'root',
    storage,
    whitelist: [
        'authenticatedUserState',
    ]
}

const persistedReducers = persistReducer(persistConfig, reducers);

const composeEnhancers =  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    let store = createStore(
        persistedReducers, 
        initialState, 
        composeEnhancers(
            applyMiddleware(thunk, promiseMiddleware)
        )
    );
    let persistor = persistStore(store);
    return { store, persistor }
}
