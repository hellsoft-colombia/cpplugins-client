/**
 * File description: Contains the selectors of loading page to know how is the state.
 * Author: Hellsoft
 */
export const getIsLoadingStatePage = state => state.isLoadingPage;