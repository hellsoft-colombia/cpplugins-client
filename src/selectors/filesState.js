/**
 * File description: Contains the selectors of files to know how is the state.
 * Author: Hellsoft
 */
export const getSelectedFile = (state) => {
    return state.filesState.selectedFile;
};

export const getFileList = (state) => {
    return state.filesState.fileList;
};

export const getPipelineList = (state) => {
    return state.filesState.pipelineList;
};

export const getPipelineNotifications = (state) => {
    return state.filesState.notifications;
}