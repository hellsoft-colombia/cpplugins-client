/**
 * File description: Contains the selectors of execution history to know how is the state.
 * Author: Hellsoft
 */
export const getHistoryList = state => state.executionHistoryState.historyList;
export const getHasMoreFlag = state => state.executionHistoryState.hasMore;
export const getCurrentPage = state => state.executionHistoryState.currentPage;
export const getIsLoading = state => state.executionHistoryState.isLoading;