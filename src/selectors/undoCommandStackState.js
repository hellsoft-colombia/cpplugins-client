/**
 * File description: Contains the selectors of the undo command stack to know how is the state.
 * Author: Hellsoft
 */
export const getUndoCommandStack = (state) => {
    return state.commandStackState.undoCommandStackState.commands;
};