/**
 * File description: Contains the selectors of plugins to know how is the state.
 * Author: Hellsoft
 */
export const getFunctionList = (state) => {
    return state.functionListState.functionList;
};

export const getIsFunctionListFilled = (state) => {
    return state.functionListState.isFilled;
};