/**
 * File description: Contains the selectors of custom filter to know how is the state.
 * Author: Hellsoft
 */
export const getIsCustomFilter = (state) => {
    return state.pipelinesState.isCustomFilter;
}