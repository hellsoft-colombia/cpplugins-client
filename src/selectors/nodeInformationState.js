/**
 * File description: Contains the selectors of node information to know how is the state.
 * Author: Hellsoft
 */
export const getNodeInformation = (state) => {
    return state.nodeInformationState.nodeInformation;
};