/**
 * File description: Contains the selectors of node state to know how is the state.
 * Author: Hellsoft
 */
export const getSelectedNode = (state) => {
    return state.nodesState.selectedNode;
};