/**
 * File description: Contains the selectors of authentication to know how is the state.
 * Author: Hellsoft
 */
export const getUserInformation = (state) => {
    return state.authenticatedUserState.user;
};


export const getAuthenticationState = (state) => {
    return state.authenticatedUserState.isAuthenticated;
};
