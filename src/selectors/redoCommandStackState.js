/**
 * File description: Contains the selectors of the redo command stack to know how is the state.
 * Author: Hellsoft
 */
export const getRedoCommandStack = (state) => {
    return state.commandStackState.redoCommandStackState.commands;
};