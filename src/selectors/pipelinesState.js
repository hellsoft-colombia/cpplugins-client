/**
 * File description: Contains the selectors of pipelines to know how is the state.
 * Author: Hellsoft
 */
export const getActivePipeline = (state) => {
    return state.pipelinesState.activePipeline;
}

export const getStoredPipeline = (state) => {
    return state.pipelinesState.storedPipeline;
}

export const getLocalStoredPipeline = (state) => {
    return state.pipelinesState.storedPipeline.localStoragePipeLine;
}

export const getPrevPipeline = (state) => {
    return state.pipelinesState.prevPipeline;
}

export const getNodesPipeline = (state) => {
    return state.pipelinesState.activePipeline.nodes;
}

export const getReadersFromPipeline = (state) => {
    return state.pipelinesState.activePipeline.nodes.filter(n => n.title === "reader");
}

export const getParamsNode = (state) => {
    return state.pipelinesState.activePipeline.parameters;
}

export const getEditPipelineRequest = (state) => ({
    pipelineId: state.pipelinesState.activePipeline.pipelineId,
    pipelineName: state.pipelinesState.activePipeline.pipelineName,
    pipelineDesign: state.pipelinesState.activePipeline
});

export const getEditCustomPluginRequest = (state) => ({
    pluginId: state.filesState.selectedFile.fileId,
    pluginName: state.filesState.selectedFile.fileName,
    subtitle: state.filesState.selectedFile.description,
    inputs: state.filesState.selectedFile.inputs,
    outputs: state.filesState.selectedFile.outputs,
    descriptor: state.pipelinesState.activePipeline
})
