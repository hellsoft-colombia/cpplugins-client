//File order cryteria
export const SORT_CURRENT_LIST = 'SORT_CURRENT_LIST';
export const ORDER_CRYTERIA = ['fileName', 'lastModificationDate', 'executionState'];

//File functions
export const FETCH_PIPELINE_LIST = 'FETCH_PIPELINE_LIST';
export const FETCH_FILE_LIST = 'FETCH_FILE_LIST';
export const SET_FILE_LIST = 'SET_FILE_LIST';
export const SET_SELECTED_FILE = 'SET_SELECTED_FILE';
export const CLEAN_SELECTED_FILE  = 'CLEAN_SELECTED_FILE';

//Files parameters
export const SEND_FILE = 'SEND_FILE';
export const DELETE_FILE = 'DELETE_FILE';
export const ADD_FILE_NODE = 'ADD_FILE_NODE';
