import { URL } from './api';

const IMAGES_PATH = URL + 'images/';

const VIDEO_PATH = URL + 'video/';

export const CHECK_BOX1 = IMAGES_PATH + 'check_box1.svg';

export const CHECK_BOX2 = IMAGES_PATH + 'check_box2.svg';

export const VIDEO_THUMBNAIL = VIDEO_PATH + 'thumbnail.jpg';

export const VIDEO = VIDEO_PATH + 'video.mp4';