//export  const BASE_URL = 'http://localhost:8080/cpPlugins/';
//export const BASE_URL = 'https://hellsoft-colombia-cpplugins-3-0-server.backend.chaustre.pw/cpPlugins/';
const server_host = process.env.SERVER_HOST;

export const URL = server_host;

export const BASE_URL = URL + 'cpPlugins/';