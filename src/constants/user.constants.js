//User logout
export const USER_LOGOUT = 'USER_LOGOUT';

//User login state
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGIN_FAILURE = "USER_LOGIN_FAILURE";

//User register state
export const USER_REGISTER = 'USER_REGISTER';
export const USER_REGISTER_FAILURE = "USER_REGISTER_FAILURE";

//User information state
export const USER_INFO = 'USER_INFO';
export const USER_INFO_FAILURE = "USER_INFO_FAILURE";
