export const HOME_ROUTE = '/';
export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';
export const DESIGN_AREA_ROUTE = '/design-area';
export const FILE_AREA_ROUTE = '/file-area';
export const EXECUTION_HISTORY = '/execution-history';
export const ROUTE_LIST = [
    HOME_ROUTE,
    LOGIN_ROUTE,
    REGISTER_ROUTE,
    DESIGN_AREA_ROUTE,
    FILE_AREA_ROUTE,
    EXECUTION_HISTORY
];