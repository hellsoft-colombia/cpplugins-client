/**
 * File description: Calls a function to create the history of the browser history.
 * Author: Hellsoft
 */
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();