/**
 * File description: Helps to constraint the access to the different pages within the application.
 * Author: Hellsoft
 */
import React, {Component} from "react"
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {getUserInformation, getAuthenticationState} from '../selectors/authenticatedUserState';
import {fetchUserInfo} from '../actions';
import {LOGIN_ROUTE, FILE_AREA_ROUTE} from "../constants/routes";
import {Redirect} from 'react-router-dom';

export const AuthorizationComponent = (WrappedComponent) => {
    class AccessControl extends Component {

        componentDidMount() {
            let {user, fetchUserInfo, loggedInState} = this.props;
            if (loggedInState) {
                if (user.userName === "" || user === undefined) {
                    fetchUserInfo();
                }
            }
        }

        render() {
            let {loggedInState} = this.props;
            if (!loggedInState) {
                return (<Redirect to={LOGIN_ROUTE}/>)
            } else {
                return (<WrappedComponent {...this.props}/>)
            }
        }
    }

    AccessControl.propTypes = {
        fetchUserInfo: PropTypes.func.isRequired,
        loggedInState: PropTypes.bool.isRequired,
        user: PropTypes.object.isRequired
    }

    const mapStateToProps = state => ({user: getUserInformation(state), loggedInState: getAuthenticationState(state)});

    const mapDispatchToProps = dispatch => ({
        fetchUserInfo: _ => dispatch(fetchUserInfo())
    });

    return connect(mapStateToProps, mapDispatchToProps, null)(AccessControl);
}

export const NoAuthorizationComponent = (WrappedComponent) => {
    class AccessControl extends Component {

        componentDidMount() {
            let {user, fetchUserInfo, loggedInState} = this.props;
            if (loggedInState) {
                if (user.userName === "" || user === undefined) {
                    fetchUserInfo();
                }
            }
        }

        render() {
            let {loggedInState} = this.props;
            if (loggedInState) {
                return (<Redirect to={FILE_AREA_ROUTE}/>)
            } else {
                return (<WrappedComponent {...this.props}/>)
            }
        }
    }

    AccessControl.propTypes = {
        fetchUserInfo: PropTypes.func.isRequired,
        loggedInState: PropTypes.bool.isRequired,
        user: PropTypes.object.isRequired
    }

    const mapStateToProps = state => ({user: getUserInformation(state), loggedInState: getAuthenticationState(state)});

    const mapDispatchToProps = dispatch => ({
        fetchUserInfo: _ => dispatch(fetchUserInfo())
    });

    return connect(mapStateToProps, mapDispatchToProps, null)(AccessControl);
}