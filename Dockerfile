FROM node:current-alpine as build
ENV LANG C.UTF-8
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
COPY .env /app/.env
RUN yarn install --silent
COPY . /app
RUN yarn build

FROM nginx:mainline-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 5000
CMD ["nginx", "-g", "daemon off;"]